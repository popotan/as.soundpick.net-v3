import { Directive, ElementRef, ViewChild, ViewChildren, QueryList } from '@angular/core';

@Directive({
  selector: '[FC-carousel]'
})
export class CarouselDirective {
  @ViewChild('contentBox') contentBox: ElementRef;
  @ViewChildren('content') content_list: QueryList<any>;
  state = {
    presentPage: 1
  };
  constructor(
    public el: ElementRef
  ) { }
  onArrowClicked(actionType) {
    switch (actionType) {
      case 'prev':
        if (this.state.presentPage > 1) {
          this.slideTo(this.state.presentPage--);
        }
        break;
      case 'next':
        if (this.content_list.length > this.state.presentPage) {
          this.slideTo(this.state.presentPage++);
        } else {
          this.slideTo(this.state.presentPage = 1);
        }
        break;
    }
  }
  slideTo(page) {
    alert(page);
  }
}
