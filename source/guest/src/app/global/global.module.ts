import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FcCarouselComponent } from './fc-carousel/fc-carousel.component';

@NgModule({
  imports: [
    CommonModule
  ],
  exports : [FcCarouselComponent],
  declarations: [FcCarouselComponent]
})
export class GlobalModule { }
