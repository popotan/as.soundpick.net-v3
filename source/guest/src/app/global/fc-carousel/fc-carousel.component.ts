import { Component, OnInit, AfterViewInit, ViewChild, ViewChildren, ElementRef, QueryList, Renderer2, HostListener } from '@angular/core';

@Component({
  selector: 'app-fc-carousel',
  templateUrl: './fc-carousel.component.html',
  styleUrls: ['./fc-carousel.component.css']
})
export class FcCarouselComponent implements OnInit, AfterViewInit {
  @ViewChild('contentBox') contentBox: ElementRef;
  @ViewChildren('content') content_list: QueryList<any>;
  state = {
    presentPage: 1,
    offsetLeftList: []
  };
  constructor(
    public el: ElementRef,
    public renderer: Renderer2
  ) { }
  ngOnInit() {
  }
  ngAfterViewInit() {
    this.getChildrenOffsetLeft();
  }
  getChildrenOffsetLeft() {
    this.state.offsetLeftList = [];
    this.content_list.forEach(element => {
      this.state.offsetLeftList.push(element.nativeElement.offsetLeft);
    });
  }
  @HostListener('window:resize', ['$event'])
  onWindowResize($event) {
    this.slideTo(1);
    this.state.presentPage = 1;
    this.getChildrenOffsetLeft();
  }
  onArrowClicked(actionType) {
    switch (actionType) {
      case 'prev':
        if (this.state.presentPage > 1) {
          this.slideTo(--this.state.presentPage);
        }
        break;
      case 'next':
        if (this.content_list.length > this.state.presentPage) {
          this.slideTo(++this.state.presentPage);
        }
        break;
    }
  }
  slideTo(page) {
    this.renderer.setStyle(this.contentBox.nativeElement, 'left', '-' + this.state.offsetLeftList[page - 1] + 'px');
  }
}
