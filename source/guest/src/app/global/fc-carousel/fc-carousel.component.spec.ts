import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FcCarouselComponent } from './fc-carousel.component';

describe('FcCarouselComponent', () => {
  let component: FcCarouselComponent;
  let fixture: ComponentFixture<FcCarouselComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FcCarouselComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FcCarouselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
