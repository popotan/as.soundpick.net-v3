import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-search-total',
  templateUrl: './search-total.component.html',
  styleUrls: ['./search-total.component.css']
})
export class SearchTotalComponent implements OnInit {
  state = {
    isFocused: false
  };
  result_list = [];
  constructor() { }

  ngOnInit() {
  }
  onFocus($event) {
    this.state.isFocused = true;
  }
}
