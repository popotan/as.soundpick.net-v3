import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { IndexComponent } from './index/index.component';
import { SearchTotalComponent } from './search-total/search-total.component';
import { CarouselDirective } from './directives/carousel.directive';
import { GlobalModule } from './global/global.module';

@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    SearchTotalComponent,
    CarouselDirective
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    GlobalModule,
    RouterModule.forRoot([
      {
        path : '',
        component : IndexComponent
      }
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
