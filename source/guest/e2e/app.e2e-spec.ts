import { As.Soundpick.NetV3Page } from './app.po';

describe('as.soundpick.net-v3 App', () => {
  let page: As.Soundpick.NetV3Page;

  beforeEach(() => {
    page = new As.Soundpick.NetV3Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
