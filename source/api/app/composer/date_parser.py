import datetime, re

class DateParser(object):
    def __init__(self, date, str_format='yyyymmdd'):
        self.str_format = str_format
        if type(date) is str:
            self.str_type = date
            self.convert_to_date()
        elif type(date) is datetime:
            self.date_type = date
            self.convert_to_str()

    def convert_to_date(self):
        regex = re.compile('(?P<year>y*)([^ymd]*)(?P<month>m*)([^ymd]*)(?P<day>d*)([^ymd]*)')
        if regex.match(self.str_format):
            s = regex.search(self.str_format)
            year = int(self.str_type[:len(s.group("year"))])
            month = int(self.str_type[len(s.group("year"))+len(s.group(2)):len(s.group("year"))+len(s.group(2))+len(s.group("month"))])
            day = int(self.str_type[len(s.group("year"))+len(s.group(2))+len(s.group("month"))+len(s.group(4)):len(s.group("year"))+len(s.group(2))+len(s.group("month"))+len(s.group(4))+len(s.group("day"))])
            self.date_type = datetime.datetime(year, month, day)
            return self
        else:
            raise Exception("date regex is not matched.")
    
    def convert_to_str(self):
        self.date_type.strftime("%Y%m%d")
        return self