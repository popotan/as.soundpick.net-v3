from openpyxl import Workbook
from openpyxl import load_workbook

class SerialFileParser(object):

    def __init__(self, filename):
        self.FILENAME = filename
        self.workbook = load_workbook(filename=self.FILENAME)
        self.workbook.active
        pass

    def get_sheet_names(self):
        return self.workbook.get_sheet_names()

    def set_active_sheet(self, sheet_name):
        self.active_sheet = self.workbook[sheet_name]
        return self

    def get_column_names(self, sheet_name):
        temp_sheet = self.workbook[sheet_name]
        result = []
        for title in list(temp_sheet.rows)[0]:
            result.append(title.value)
        return result

    def get_selected_row(self, column_num):
        result = []
        for col in list(self.active_sheet.rows)[1:]:
            v = col[int(column_num)].value
            if v:
                result.append(v)
        return result