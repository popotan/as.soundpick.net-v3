import datetime, json
import urllib
import xmltodict
from flask import session
from sqlalchemy.exc import SQLAlchemyError

# from api_admin_soundpick_net.model.sms_hist import sms_hist_orm

class Sms():
	params = {
	'id': 'funclass',
	'pwd': 'O99RWO101070EWAT4450',
	'from': '',
	'to_country': '82',
	'to': '',
	'message': '',
	'report_req': '1'
	}
	result = False

	def __init__(self, title=None, to_number=None, from_number='', desc='', variable_pair={}):
		self.params['title'] = title
		self.params['to'] = to_number
		self.params['from'] = from_number
		self.params['message'] = desc
		self.variable_pair = variable_pair

	def convert_variables(self):
		for key, value in self.variable_pair.items():
			self.params['message'] = self.params['message'].replace('{' + key + '}', value)
		return self

	def send(self):
		url = 'http://rest.supersms.co:6200/sms/xml?' + urllib.parse.urlencode(self.params)
		response = urllib.request.urlopen(url).read().decode('utf8')
		tree = xmltodict.parse(response)

		self.save_result(err_code=tree['submit_response']['messages']['message']['err_code'])
		if tree['submit_response']['messages']['message']['err_code'] == 'R000':
			self.result = True
		else:
			self.result = False
		return self

	def save_result(self, err_code):
		try:
			# history = sms_hist_orm()
			# history.user_uid = session['user_uid']
			# history.targ_num = self.params['to']
			# history.msg = self.params['message']
			# history.result = err_code
			# history.add()
			return self

		except SQLAlchemyError as e:
			print(e)
		finally:
			return self