from soundpick_orms.admin.admin_user import admin_user_orm
from soundpick_orms.admin.admin_group import admin_group_orm
from .FCModel import FCModel
from datetime import timedelta
from app import db
from sqlalchemy import or_, and_, text, asc

class AdminUser(FCModel):
    def __init__(self):
        super(AdminUser, self).__init__()

    def get_admin_user_list(self, from_date=None, to_date=None, is_valid=None, keyword='', page=1, per_page=30):
        and_conditions = []

        # Table Join
        user_list = admin_user_orm().query

        # case 1: 처리상태
        if is_valid:
            is_valid = True if is_valid == 'true' else False
            and_conditions.append(admin_user_orm.is_valid == is_valid)
        # case 2: 접수기간
        if from_date and to_date:
            and_conditions.append(admin_user_orm.reg_date >= from_date)
            and_conditions.append(admin_user_orm.reg_date <=
                                  to_date + timedelta(days=1))  # 하루더하기
        
        # case 3: 문자열검색
        keyword_or_conditions = []
        if keyword:
            # case 5-1: 이름
            keyword_or_conditions.append(
                admin_user_orm.name.ilike(text('"%'+keyword+'%"')))

        user_list = user_list.filter(and_(*and_conditions))
        user_list = user_list.filter(or_(*keyword_or_conditions))

        self.count += user_list.count()

        if page and per_page:
            user_list = user_list.order_by(admin_user_orm.user_uid).offset(
                (page-1)*per_page).limit(per_page)
        user_list = user_list.all()
        if len(user_list) > 0:
            for li in user_list:
                self.orms.append(li)
        return self

    def set_validation(self, user_uid, is_valid):
        user = admin_user_orm().query.filter_by(user_uid=user_uid).first()
        if user:
            user.is_valid = True if is_valid == 'true' or is_valid == True else False
            db.session.add(user)
            db.session.flush()
            db.session.commit()
            self.orms.append(user)
            self.count += 1
        return self

    def get_group_list(self, from_date=None, to_date=None, is_valid=None, keyword='', page=1, per_page=30):
        and_conditions = []

        # Table Join
        group_list = admin_group_orm().query

        # case 1: 처리상태
        if is_valid:
            is_valid = True if is_valid == 'true' else False
            and_conditions.append(admin_group_orm.is_valid == is_valid)
        # case 2: 접수기간
        if from_date and to_date:
            and_conditions.append(admin_group_orm.reg_date >= from_date)
            and_conditions.append(admin_group_orm.reg_date <=
                                  to_date + timedelta(days=1))  # 하루더하기
        
        # case 3: 문자열검색
        keyword_or_conditions = []
        if keyword:
            # case 5-1: 이름
            keyword_or_conditions.append(
                admin_group_orm.name.ilike(text('"%'+keyword+'%"')))

        group_list = group_list.filter(and_(*and_conditions))
        group_list = group_list.filter(or_(*keyword_or_conditions))

        self.count += group_list.count()

        if page and per_page:
            group_list = group_list.order_by(admin_group_orm.id).offset(
                (page-1)*per_page).limit(per_page)
        group_list = group_list.all()
        if len(group_list) > 0:
            for li in group_list:
                self.orms.append(li)
        return self
    
    def add_group(self, info):
        with db.session.no_autoflush:
            if info['group'].get('id', None):
                group = admin_group_orm().query.filter_by(id=info['group']['id']).first()
            else:
                group = admin_group_orm()
            group.name = info['group']['name']
            group.is_valid = True if info['group']['is_valid'] == 'true' or info['group']['is_valid'] == True else False
            group.admin_group_code = info['group']['admin_group_code']
            db.session.add(group)
            db.session.flush()
            group.comment = info.get('comment', '')
            db.session.commit()

            self.orms.append(group)
            self.count += 1
            return self