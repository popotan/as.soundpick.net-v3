from app.model.FCModel import FCModel
from sqlalchemy import text, or_, and_

from soundpick_orms.custom.customer import customer_orm


class Customer(FCModel):
    def __init__(self):
        super(Customer, self).__init__()

    def get_info(self, id=None, keyword=None):
        if id:
            info = customer_orm().query.filter_by(id=id).first()
            if info:
                self.orms.append(info)

        if keyword:
            conditions = [
                customer_orm.name.ilike(text("'%" + keyword + "%'")),
                customer_orm.phone1.ilike(text("'%" + keyword + "%'")),
                customer_orm.phone2.ilike(text("'%" + keyword + "%'"))
            ]
            info = customer_orm().query.filter(or_(*conditions)).all()
            if info:
                self.orms = info
        return self

    def is_phone_exist(self, phone1=None):
        info = customer_orm().query.filter_by(phone1=phone1).first()
        self.count = self.count + 1
        if info:
            self.orms.append(info)
        return self