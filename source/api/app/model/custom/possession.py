from app.model.FCModel import FCModel
from soundpick_orms.custom.possession import possession_orm
from soundpick_orms.custom.possession_exchange_log import possession_exchange_log_orm

from sqlalchemy import and_, or_, asc, desc
from sqlalchemy.orm import joinedload
from datetime import datetime, timedelta


class Possession(FCModel):
    def __init__(self):
        super(Possession, self).__init__()

    def get_possession_list(self, from_date=None, to_date=None, is_expired=None, product_id=[], keyword='', page=1, per_page=30):
        and_conditions = []

        poss_list = possession_orm().query

        # case 1: 만료상태
        now = datetime.utcnow() + timedelta(hours=9)
        if is_expired == 'true':
            and_conditions.append(possession_orm.warranty_expired_date < now)
        elif is_expired == 'false':
            and_conditions.append(possession_orm.warranty_expired_date >= now)

        # case 2: 등록기간
        and_conditions.append(possession_orm.reg_date >= from_date)
        and_conditions.append(possession_orm.reg_date <=
                              to_date + timedelta(days=1))

        # case 3: 제품
        product_id_or_conditions = []
        if len(product_id) > 0:
            for pid in product_id:
                product_id_or_conditions.append(
                    possession_exchange_log_orm.product_id == pid)

        # case 4: 문자열 검색
        keyword_or_conditions = []
        if keyword:
            # case 5-1: 이름
            keyword_or_conditions.append(
                customer_orm.name.ilike(text('"%'+keyword+'%"')))
            # case 5-2: 연락처
            keyword_or_conditions.append(
                customer_orm.phone1.ilike(text('"%'+keyword+'%"')))
            keyword_or_conditions.append(
                customer_orm.phone2.ilike(text('"%'+keyword+'%"')))

        poss_list = poss_list.filter(or_(*product_id_or_conditions))
        poss_list = poss_list.filter(or_(*keyword_or_conditions))
        poss_list = poss_list.filter(and_(*and_conditions))

        self.count += poss_list.count()

        if page and per_page:
            poss_list = poss_list.order_by(possession_orm.id).offset(
                (page-1)*per_page).limit(per_page)
        
        poss_list = poss_list.all()
        if len(poss_list) > 0:
            for li in poss_list:
                self.orms.append(li)
        
        return self

    def get_possession(self, id=None, customer_id=None):
        poss_list = possession_orm().query

        if id:
            poss_list = poss_list.filter_by(id=id)
        if customer_id:
            poss_list = poss_list.filter(
                possession_exchange_log_orm.customer_id == customer_id)

        self.count += poss_list.count()
        poss_list = poss_list.all()
        if len(poss_list) > 0:
            for li in poss_list:
                self.orms.append(li)
        return self

    def get_possession_expired_of(self, days=-7):
        poss_list = possession_orm().query\
            .filter(possession_orm.warranty_expired_date >=
                    datetime.utcnow() + timedelta(hours=9) + timedelta(days=days))\
            .filter(possession_orm.warranty_expired_date <= datetime.utcnow() + timedelta(hours=9))

        self.count += poss_list.count()
        poss_list = poss_list.all()
        if len(poss_list) > 0:
            for li in poss_list:
                self.orms.append(li)
        return self