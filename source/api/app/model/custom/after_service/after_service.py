from flask import session
from app.model.FCModel import FCModel
from soundpick_orms.custom.after_service.as_case import as_case_orm
from soundpick_orms.custom.after_service.as_process import as_process_orm
from soundpick_orms.custom.after_service.as_config import as_config_orm
from soundpick_orms.custom.customer import customer_orm
from soundpick_orms.custom.possession import possession_orm
from soundpick_orms.custom.possession_exchange_log import possession_exchange_log_orm
from soundpick_orms.custom.possession_exchange_log_option import possession_exchange_log_option_orm
from soundpick_orms.product.serial import serial_orm
from soundpick_orms.comment import comment_orm
from soundpick_orms.custom.after_service.trouble_type_dictionary import trouble_type_dictionary_orm
from app import db
from app.composer.date_parser import DateParser
from sqlalchemy import or_, and_, text, asc
from flask_sqlalchemy import Pagination
from datetime import datetime, timedelta

from ..possession import Possession
from app.composer.sms import Sms


class AfterService(FCModel):
    def __init__(self):
        super(AfterService, self).__init__()

    def get_as_list(self, from_date=None, to_date=None, is_expired=None, symptom=[], product_id=[], keyword='', page=1, per_page=30):
        and_conditions = []

        # Table Join
        as_list = as_case_orm().query
        as_list = as_list.filter_by(admin_group_id=session['group_id'])

        # case 1: 처리상태
        if is_expired:
            and_conditions.append(as_case_orm.is_expired == is_expired)
        # case 2: 접수기간
        if from_date and to_date:
            and_conditions.append(as_case_orm.reg_date >= from_date)
            and_conditions.append(as_case_orm.reg_date <=
                                  to_date + timedelta(days=1))  # 하루더하기
        # case3 증상
        symptom_or_conditions = []
        if len(symptom) > 0:
            for s in symptom:
                symptom_or_conditions.append(as_case_orm.division == s)
        # case 4: 제품
        product_id_or_conditions = []
        if len(product_id) > 0:
            for pid in product_id:
                product_id_or_conditions.append(
                    possession_exchange_log_orm.product_id == pid)
        # case 5: 문자열검색
        keyword_or_conditions = []
        if keyword:
            # case 5-1: 이름
            keyword_or_conditions.append(
                customer_orm.name.ilike(text('"%'+keyword+'%"')))
            # case 5-2: 연락처
            keyword_or_conditions.append(
                customer_orm.phone1.ilike(text('"%'+keyword+'%"')))
            keyword_or_conditions.append(
                customer_orm.phone2.ilike(text('"%'+keyword+'%"')))
            # case 5-3: 제품번호
            # keyword_or_conditions.append(
            #     serial_orm.serial.ilike(text('"%'+keyword+'%"')))
            # case 5-4: 송장번호
            keyword_or_conditions.append(
                as_process_orm.value.ilike(text('"%'+keyword+'%"')))
            # case 5-5: 증상
            keyword_or_conditions.append(
                as_case_orm.description.ilike(text('"%'+keyword+'%"')))

        as_list = as_list.filter(and_(*and_conditions))
        as_list = as_list.filter(or_(*symptom_or_conditions))
        as_list = as_list.filter(or_(*product_id_or_conditions))
        as_list = as_list.filter(or_(*keyword_or_conditions))

        self.count += as_list.count()

        if page and per_page:
            as_list = as_list.order_by(as_case_orm.id).offset(
                (page-1)*per_page).limit(per_page)
        as_list = as_list.all()
        if len(as_list) > 0:
            for li in as_list:
                self.orms.append(li)
        return self

    def get_symptom(self):
        symptom_list = trouble_type_dictionary_orm().query.all()
        if symptom_list:
            for symptom in symptom_list:
                self.orms.append(symptom)
        return self

    def remove_symptom(self, value):
        symptom = trouble_type_dictionary_orm().query.filter_by(value=value).delete()
        if symptom:
            db.session.commit()
        return self

    def set_symptom(self, value):
        symptom = trouble_type_dictionary_orm()
        symptom.value = value
        db.session.add(symptom)
        db.session.commit()
        return self

    def set_invoice(self, as_case_id, invoice):
        with db.session.no_autoflush:
            address = as_process_orm().query.filter_by(as_case_id=as_case_id, attribute='address').first()
            postcode = as_process_orm().query.filter_by(as_case_id=as_case_id, attribute='postcode').first()
            if address and postcode:
                info = as_process_orm().query.filter_by(
                    as_case_id=as_case_id, attribute='invoice').first()
                if not info:
                    info = as_process_orm()
                    info.as_case_id = as_case_id
                    info.attribute = 'invoice'
                info.value = invoice
                db.session.add(info)
                db.session.flush()
                info.comment = '바코드 자동입력'
                db.session.commit()
                self.send_sms_on_set_invoice(info.as_case)
                self.orms.append(info.as_case)
            return self

    def send_sms_on_set_invoice(self, as_case):
        sms_desc = as_config_orm().query.filter_by(
            admin_group_id=session['group_id'], attribute='sms_on_invoice_added').first()
        v = {
            "받는분성명": as_case.customer.name,
            "받는분전화번호": as_case.customer.phone1,
            "우편번호": as_case.process['postcode'],
            "받는분주소": as_case.process['address'],
            "송장번호": as_case.process["invoice"]
        }
        Sms(title='', to_number=as_case.customer.phone1, from_number='07078140007',
            desc=sms_desc.value, variable_pair=v).convert_variables().send()

    def add_case(self, info):
        with db.session.no_autoflush:
            if info['possession'].get('id', None):
                poss = possession_orm().query.filter_by(
                    id=info['possession'].get('id')).first()
            else:
                poss = possession_orm()
            poss.purchase_date = datetime.strptime(info['possession'].get(
                'purchase_date', None), '%Y-%m-%dT%H:%M:%S.%fZ')
            poss.proof_image_filename = info['possession'].get(
                'proof_image_filename', None)
            poss.warranty_expired_date = datetime.strptime(info['possession'].get(
                'warranty_expired_date', None), '%Y-%m-%dT%H:%M:%S.%fZ')
            db.session.add(poss)
            db.session.flush()
            poss.comment = info.get('comment', '')

            for elog in info['possession']['exchange_log']:
                if elog['customer'].get('id', None):
                    customer = customer_orm().query.filter_by(
                        id=elog['customer'].get('id')).first()
                else:
                    customer = customer_orm()
                customer.name = elog['customer'].get('name', None)
                customer.phone1 = elog['customer'].get('phone1', None)
                customer.phone2 = elog['customer'].get('phone2', None)
                db.session.add(customer)
                db.session.flush()
                customer.comment = info.get('comment', '')

                if elog.get('id', None):
                    exchange_log = possession_exchange_log_orm(
                    ).query.filter_by(id=elog.get('id')).first()
                else:
                    exchange_log = possession_exchange_log_orm()
                exchange_log.possession_id = poss.id
                exchange_log.customer_id = customer.id
                exchange_log.product_id = elog['product'].get('id', None)
                exchange_log.serial_id = elog['serial'].get('id', None)
                if elog.get('as_case_id', None):
                    exchange_log.as_case_id = elog['as_case_id']
                db.session.add(exchange_log)
                db.session.flush()
                exchange_log.comment = info.get('comment', '')

                if elog['option']:
                    for key, val in elog.get('option', {}).items():
                        option = possession_exchange_log_option_orm().query.filter_by(
                            exchange_log_id=exchange_log.id, attribute=key).first()
                        if option and option.value != val:
                            option.value = val
                            db.session.add(option)
                            db.session.flush()
                            option.comment = info.get('comment', '')
                        elif not option and val:
                            option = possession_exchange_log_option_orm()
                            option.exchange_log_id = exchange_log.id
                            option.attribute = key
                            option.value = val
                            db.session.add(option)
                            db.session.flush()
                            option.comment = info.get('comment', '')

            for as_case in info['possession'].get('as_case', []):
                if as_case.get('id', None):
                    case = as_case_orm().query.filter_by(id=as_case.get('id')).first()
                else:
                    case = as_case_orm()

                if as_case['customer'].get('id', None):
                    customer = customer_orm().query.filter_by(
                        id=as_case['customer'].get('id')).first()
                else:
                    customer = customer_orm()
                cusomter.admin_group_id = session['group_id']
                customer.name = as_case['customer'].get('name', None)
                customer.phone1 = as_case['customer'].get('phone1', None)
                customer.phone2 = as_case['customer'].get('phone2', None)
                db.session.add(customer)
                db.session.flush()
                customer.comment = info.get('comment', '')

                case.division = as_case.get('division', None)
                case.description = as_case.get('description', None)
                case.is_expired = as_case.get('is_expired', None)
                if case.is_expired == '처리완료':
                    case.expired_date = datetime.utcnow() + timedelta(hours=9)
                case.customer_id = customer.id
                case.possession_id = poss.id
                db.session.add(case)
                db.session.flush()
                case.comment = info.get('comment', '')

                if as_case['process']:
                    for key, val in as_case.get('process', {}).items():
                        process = as_process_orm().query.filter_by(
                            as_case_id=case.id, attribute=key).first()
                        if process and process.value != val:
                            process.value = val
                            db.session.add(process)
                            db.session.flush()
                            process.comment = info.get('comment', '')
                            if process.attribute is 'invoice':
                                self.send_sms_on_set_invoice(as_case)
                        elif not process and val:
                            process = as_process_orm()
                            process.as_case_id = case.id
                            process.attribute = key
                            process.value = val
                            db.session.add(process)
                            db.session.flush()
                            process.comment = info.get('comment', '')
                            if process.attribute is 'invoice':
                                self.send_sms_on_set_invoice(as_case)

            db.session.commit()
            self.orms.append(poss)
            return self

    def add_exchange_log(self, possession_id, customer_id=None, product_id=None, serial_id=None, as_case_id=None, info):
        pass

    def add_possession(self, info):
        pass
