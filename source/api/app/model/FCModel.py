# _*_ coding: utf-8 _*_
import copy
class FCModel(object):
    def __init__(self):
        self.orms = []
        self.count = 0

    def get_json(self, data_structure={}):
        def get_col(orm, row):
            for _col in row.keys():
                if type(row[_col]) is list and len(row[_col]) > 0:
                    ds = copy.deepcopy(row[_col][0])
                    row[_col] = []
                    for o in getattr(orm, _col):
                        row[_col].append(get_col(o, ds))

                elif type(row[_col]) is dict and len(row[_col].keys()) > 0:
                    row[_col] = get_col(getattr(orm, _col), row[_col])
                
                else:
                    if hasattr(orm, _col):
                        row[_col] = getattr(orm, _col)
                    else:
                        row[_col] = None
            return row

        result = []
        if len(self.orms) > 0:
            for orm in self.orms:
                r = get_col(orm, copy.deepcopy(data_structure))
                result.append(r)
        return result