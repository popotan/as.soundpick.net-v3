# _*_ coding: utf-8 _*_

from datetime import datetime, timedelta
from sqlalchemy import or_, and_, text, asc
from app import db
from flask import session

from soundpick_orms.product.brand import brand_orm
from soundpick_orms.product.brand_option import brand_option_orm
from soundpick_orms.product.brand_image import brand_image_orm
from soundpick_orms.product.product import product_orm
from soundpick_orms.product.product_option import product_option_orm
from app.composer.Logger import Logger
from app.model.FCModel import FCModel


class Brand(FCModel):
    def __init__(self):
        super(Brand, self).__init__()

    def get_brand_list(self, from_date=None, to_date=None, keyword='', page=None, per_page=None):
        and_conditions = []

        brand_list = brand_orm().query.filter_by(admin_group_id=session['group_id'])

        if from_date and to_date:
            and_conditions.append(brand_orm.reg_date >= from_date)
            and_conditions.append(brand_orm.reg_date <=
                                  to_date + timedelta(days=1))

        keyword_or_conditions = []
        if keyword != '':
            keyword_or_conditions.append(
                brand_orm.name.ilike(text('"%'+keyword+'%"'))
            )

        brand_list = brand_list.filter(and_(*and_conditions))
        brand_list = brand_list.filter(or_(*keyword_or_conditions))
        brand_list = brand_list.order_by(brand_orm.id)

        if page and per_page:
            brand_list = brand_list.offset((page-1)*per_page).limit(per_page)

        self.count += brand_list.count()
        brand_list = brand_list.all()

        if len(brand_list) > 0:
            for li in brand_list:
                self.orms.append(li)
        return self

    def get_as_target_list(self):
        options = brand_option_orm.query.filter_by(
            attribute='is_as_target', value='true', value_type="bool").all()
        for o in options:
            if o.brand.admin_group_id == session['group_id']:
                self.orms.append(o.brand)
        return self

    def add_brand(self, info):
        with db.session.no_autoflush:
            if 'id' in info['brand']:
                brand = brand_orm().query.filter_by(
                    id=info['brand']['id']).first()
            else:
                brand = brand_orm()
            brand.name = info['brand']['name']
            brand.admin_group_id = session['group_id']
            db.session.add(brand)
            db.session.flush()
            brand.comment = info.get('comment', '')

            for image in info['brand']['image']:
                if 'id' in image:
                    img = brand_image_orm().query.filter_by(id=image['id']).first()
                else:
                    img = brand_image_orm()
                img.brand_id = brand.id
                img.image_filename = image['image_filename']
                img.image_size_width = image['image_size_width']
                img.image_size_height = image['image_size_height']
                db.session.add(img)
                db.session.flush()

            for key, val in info['brand'].get('option', {}).items():
                option = brand_option_orm().query.filter_by(brand_id=brand.id, attribute=key).first()
                if not option:
                    option = brand_option_orm()
                    option.brand_id = brand.id
                    option.attribute = key
                option.value = val
                db.session.add(option)
                db.session.flush()

            db.session.commit()
            self.orms.append(brand)
            return self
