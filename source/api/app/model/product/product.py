# _*_ coding: utf-8 _*_
from flask import session
from sqlalchemy import text, and_, or_
from datetime import datetime, timedelta
from soundpick_orms.product.brand import brand_orm
from soundpick_orms.product.brand_option import brand_option_orm
from soundpick_orms.product.product import product_orm
from soundpick_orms.product.product_option import product_option_orm
from soundpick_orms.product.product_image import product_image_orm
from soundpick_orms.product.serial import serial_orm
from app.model.FCModel import FCModel
from app import db

class Product(FCModel):
    def __init__(self):
        super(Product, self).__init__()

    def get_as_target_list(self, brand_id):
        options = product_option_orm.query.filter_by(attribute='is_as_target', value='true', value_type="bool")\
            .join(product_option_orm.product).filter_by(brand_id=brand_id).all()
        for o in options:
            if o.product.brand.admin_group_id == session['group_id']:
                self.orms.append(o.product)
        return self

    def get_validation_target_list(self, brand_id):
        product_list = product_orm.query.filter_by(validation=True, brand_id=brand_id).all()
        for product in product_list:
            if product.brand.admin_group_id == session['group_id']:
                self.orms.append(product)
        return self

    def get_product_list(self, from_date=None, to_date=None, brand=[], keyword='', page=None, per_page=None):
        and_conditions = []

        product_list = product_orm().query
        and_conditions.append(brand_orm.admin_group_id == session['group_id'])

        if from_date and to_date:
            and_conditions.append(product_orm.reg_date >= from_date)
            and_conditions.append(product_orm.reg_date <=
                                  to_date + timedelta(days=1))

        keyword_or_conditions = []
        if keyword != '':
            keyword_or_conditions.append(
                product_orm.name.ilike(text('"%'+keyword+'%"'))
            )

        brand_or_conditions = []
        if len(brand) > 0:
            for brand_id in brand:
                brand_or_conditions.append(
                    product_orm.brand_id == brand_id
                )

        product_list = product_list.filter(and_(*and_conditions))
        product_list = product_list.filter(or_(*keyword_or_conditions))
        product_list = product_list.filter(or_(*brand_or_conditions))
        product_list = product_list.order_by(product_orm.id)
        if page and per_page:
            product_list = product_list.offset((page-1)*per_page).limit(per_page)

        self.count += product_list.count()
        product_list = product_list.all()
        
        if len(product_list) > 0:
            for li in product_list:
                self.orms.append(li)
        return self

    def get_info(self, serial=None, id=None, keyword=None):
        if serial:
            info = serial_orm().query.filter_by(serial=serial).first()
            if info:
                self.orms.append(info.product)

        if id:
            info = product_orm().query.filter_by(id=id).first()
            if info:
                self.orms.append(info)

        if keyword:
            info = product_orm().query.filter(
                product_orm.name.ilike(text("'%" + keyword + "%'"))).all()
            if info:
                self.orms = info

        return self

    def add_product(self, info):
        with db.session.no_autoflush:
            if 'id' in info['product']:
                product = product_orm().query.filter_by(id=info['product']['id']).first()
            else:
                product = product_orm()
            product.brand_id = info['product']['brand']['id']
            product.name = info['product']['name']
            product.lineup = info['product']['lineup']
            product.warranty_day_length = info['product']['warranty_day_length']
            product.validation = info['product']['validation']
            db.session.add(product)
            db.session.flush()
            product.comment = info.get('comment', '')

            for image in info['product']['image']:
                if 'id' in image:
                    img = product_image_orm().query.filter_by(id=image['id']).first()
                else:
                    img = product_image_orm()
                img.product_id = product.id
                img.image_filename = image['image_filename']
                img.image_size_width = image['image_size_width']
                img.image_size_height = image['image_size_height']
                db.session.add(img)
                db.session.flush()

            for key, val in info['product'].get('option', {}).items():
                option = product_option_orm().query.filter_by(product_id=product.id, attribute=key).first()
                if not option:
                    option = product_option_orm()
                    option.product_id = product.id
                    option.attribute = key
                option.value = val
                db.session.add(option)
                db.session.flush()

            db.session.commit()
            self.orms.append(product)
            return self