# _*_ coding: utf-8 _*_

from soundpick_orms.product.brand import brand_orm
from soundpick_orms.product.brand_option import brand_option_orm
from soundpick_orms.product.product import product_orm
from soundpick_orms.product.product_option import product_option_orm
from soundpick_orms.product.serial import serial_orm
# from soundpick_orms.product.serial_option import serial_option_orm

from app import db
from sqlalchemy import or_, and_, text, asc
from datetime import datetime, timedelta

from app.model.FCModel import FCModel
class Serial(FCModel):
    def __init__(self):
        super(Serial, self).__init__()

    def get_serial_list(self, from_date=None, to_date=None, is_expired=None, product_id=[], keyword='', page=1, per_page=30):
        and_conditions = []

        #Table Join
        serial_list = serial_orm().query
        serial_list = serial_list.join(serial_orm.product)

        if is_expired is not None:
            and_conditions.append(serial_orm.is_expired == is_expired)

        if from_date and to_date:
            and_conditions.append(serial_orm.reg_date >= from_date)
            and_conditions.append(serial_orm.reg_date <= to_date + timedelta(days=1))

        product_id_or_conditions = []
        if len(product_id) > 0:
            for pid in product_id:
                product_id_or_conditions.append(product_orm.id == pid)

        keyword_or_conditions = []
        if keyword:
            keyword_or_conditions.append(
                serial_orm.serial.ilike(text('"%' + keyword + '%"'))
            )

        serial_list = serial_list.filter(and_(*and_conditions))
        serial_list = serial_list.filter(or_(*product_id_or_conditions))
        serial_list = serial_list.filter(or_(*keyword_or_conditions))

        self.count += serial_list.count()

        if page and per_page:
            serial_list = serial_list.order_by(serial_orm.id).offset((page-1)*per_page).limit(per_page)
        serial_list = serial_list.all()

        if len(serial_list) > 0:
            for li in serial_list:
                self.orms.append(li)

        return self

    def get_info(self, serial):
        info = serial_orm().query.filter_by(serial=serial).first()
        if info:
            self.orms.append(info)
        return self