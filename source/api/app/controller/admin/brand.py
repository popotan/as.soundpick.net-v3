# _*_ coding: utf-8 _*_
from flask import Blueprint, render_template, jsonify, request, session, redirect, g

from sqlalchemy import asc

from app import app
import os
import json
from datetime import datetime, timedelta

from app.composer.decorator import login_required, valid_required, login_required_admin, group_valid_required
from app.model.product.brand import Brand

from app.composer.date_parser import DateParser

brand = Blueprint('brand', __name__)


@brand.route('/<int:id>', methods=['GET'])
def get_brand_info(id):
    pass


@brand.route('', methods=['GET', 'POST'])
def get_brand_list():
    json_struct = {
        "id": None,
        "name": "",
        "reg_date": None,
        "image": [
            {
                "id": None,
                "image_filename": "",
                "image_size_width": 0,
                "image_size_height": 0,
                "reg_date": None
            }
        ],
        "option": None
    }
    if request.method == 'GET':
        result = Brand().get_brand_list(
            from_date=DateParser(request.args.get('from_date')).date_type,
            to_date=DateParser(request.args.get('to_date')).date_type,
            keyword=request.args.get('keyword'),
            page=request.args.get('page', type=int),
            per_page=30
        )
        return jsonify({
            'message': 'SUCCESS',
            'response': {
                'meta': {
                    'total_count': result.count
                },
                'document': result.get_json(json_struct)
            }
        })
    elif request.method == 'POST':
        result = Brand().add_brand(request.get_json())
        return jsonify({
            'message': 'SUCCESS',
            'response': {
                'meta': '',
                'document': result.get_json(json_struct)[0]
            }
        })


@brand.route('/search', methods=['GET'])
def brand_search_by_keyword():
    json_struct = {
        'id': 0,
        'name': '',
        'reg_date': None
    }
    result = Brand().get_brand_list(keyword=request.args.get('keyword')).get_json(json_struct)
    if len(result) > 0:
        return jsonify({
            'message': 'SUCCESS',
            'response': {
                'meta': {},
                'document': result
            }
        })
    else:
        return jsonify({
            'message': 'MATCHED_BRAND_IS_NOT_EXIST'
        })


@brand.route('/image', methods=['POST'])
def upload_images():
    def allowed_file(filename):
        ALLOWED_EXTENSIONS = set(['jpeg', 'png', 'jpg', 'gif'])
        return '.' in filename and \
            filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

    def get_image_size(filepath):
        from PIL import Image
        im = Image.open(filepath)
        return im.size

    filename_arr = []
    files = request.files.getlist('upload_files[]')
    for file in files:
        if file and allowed_file(file.filename.lower()):
            filename = str(round(datetime.now().timestamp())) + \
                '_' + file.filename.replace(" ", "_")
            file.save(os.getcwd() + '/app/static/brand/' + filename)
            iwidth, iheight = get_image_size(
                os.getcwd() + '/app/static/brand/' + filename)
            filename_arr.append({'filename': filename, 'size': {
                                'width': iwidth, 'height': iheight}})
    return json.dumps(filename_arr)
