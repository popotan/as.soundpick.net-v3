# _*_ coding: utf-8 _*_
from flask import Blueprint, render_template, jsonify, request, session, redirect, send_file, g

from sqlalchemy import asc

from app import app
import os
import json
from datetime import datetime, timedelta

from app.composer.decorator import login_required, valid_required, login_required_admin, group_valid_required
from app.model.product.brand import Brand
from app.model.product.product import Product
from app.model.custom.after_service.after_service import AfterService

from app.composer.date_parser import DateParser

after_service = Blueprint('after_service', __name__)

@valid_required
@after_service.route('/target/brand', methods=['GET'])
def get_as_target_brand():
    json_struct = {
        'id': 0,
        'name': ''
    }
    result = Brand().get_as_target_list().get_json(json_struct)
    return jsonify({
        'message': 'SUCCESS',
        'response': {
            'meta': {},
            'document': result
        }
    }), 200

@valid_required
@after_service.route('/target/brand/<int:brand_id>/product', methods=['GET'])
def get_as_target_product_of_brand(brand_id):
    json_struct = {
        'id': 0,
        'name': ''
    }
    result = Product().get_as_target_list(brand_id).get_json(json_struct)
    return jsonify({
        'message': 'SUCCESS',
        'response': {
            'meta': {},
            'document': result
        }
    }), 200

@valid_required
@after_service.route('', methods=['GET', 'POST'])
def as_target_list():
    if request.method == 'GET':
        result = AfterService().get_as_list(
            is_expired=request.args.get('is_expired'),
            from_date=DateParser(request.args.get('from_date')).date_type,
            to_date=DateParser(request.args.get('to_date')).date_type,
            symptom=request.args.getlist('symptom'),
            product_id=request.args.getlist('product'),
            keyword=request.args.get('keyword'),
            page=request.args.get('page', type=int),
            per_page=30
        )
        with open(os.path.dirname(os.path.dirname(__file__)) + '/../json/as_case.json', 'r') as f:
            return jsonify({
                'message': 'SUCCESS',
                'response': {
                    'meta': {
                        'total_count': result.count
                    },
                    'document': result.get_json(json.loads(f.read()))
                }
            })
    elif request.method == 'POST':
        with open(os.path.dirname(os.path.dirname(__file__)) + '/../json/possession.json', 'r') as f:
            result = AfterService().add_case(request.get_json())
            return jsonify({
                'message': 'SUCCESS',
                'response': {
                    'meta': {},
                    'document': result.get_json(json.loads(f.read()))[0]
                }
            })

@valid_required
@after_service.route('/symptom', methods=['GET', 'POST', 'DELETE'])
def symptom():
    if request.method == 'GET':
        json_struct = {
            'value': ''
        }
        result = AfterService().get_symptom().get_json(json_struct)
        return jsonify({
            'message': 'SUCCESS',
            'response': {
                'meta': {},
                'document': result
            }
        })
    elif request.method == 'POST':
        AfterService().set_symptom(request.get_json()['value'])
        return jsonify({
            'message': 'SUCCESS'
        })
    elif request.method == 'DELETE':
        AfterService().remove_symptom(request.args.get('value'))
        return jsonify({
            'message': 'SUCCESS'
        })

@valid_required
@after_service.route('/invoice/<int:id>', methods=['POST'])
def set_invoice(id):
    result = AfterService().set_invoice(
        id, request.get_json()['invoice']).orms
    if result:
        return jsonify({
            'message': 'SUCCESS'
        })
    else:
        return jsonify({
            'message' : 'ADDRESS_OR_POSTCODE_IS_NOT_EXIST'
        })

@valid_required
@after_service.route('/invoice/export', methods=['POST'])
def export_invoice():
    import io
    from openpyxl import Workbook
    from openpyxl.writer.excel import save_virtual_workbook
    wb = Workbook()
    ws = wb.active
    for row in request.get_json()['invoice_list']:
        ws.append(row)
    return save_virtual_workbook(wb)
