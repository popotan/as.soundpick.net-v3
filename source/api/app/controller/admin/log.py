# _*_ coding: utf-8 _*_
from flask import Blueprint, render_template, jsonify, request, session, redirect, g

from sqlalchemy import asc, and_, or_

from app import app
import os
from datetime import datetime, timedelta
from urllib.parse import unquote

from app.composer.decorator import login_required, valid_required, login_required_admin, group_valid_required
from soundpick_orms.comment import comment_orm

log = Blueprint('log', __name__)

@log.route('', methods=['GET'])
def get_comments():
    target = request.args.get('target')
    temp1 = target.split('|')
    query = comment_orm().query
    or_conditions = []
    for tg in temp1:
        keyval = tg.split(':')
        for id in keyval[1].split(','):
            or_conditions.append(and_(comment_orm.table_name==keyval[0], comment_orm.pid==id))
    query = query.filter(or_(*or_conditions))
    query = query.all()

    repr_cols = ['id', 'table_name', 'column_name', 'admin_name', 'prev_value', 'next_value', 'description', 'reg_date']
    return jsonify({
        'message' : 'SUCCESS',
        'response' : {
            'meta' : {},
            'document' : [{_col : getattr(_row, _col) for _col in repr_cols} for _row in query]
        }
    })
