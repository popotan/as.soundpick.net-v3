from flask import Blueprint, render_template, jsonify, request, session, redirect
from werkzeug.security import generate_password_hash, check_password_hash
from app.composer.decorator import login_required, valid_required, login_required_admin, group_valid_required

from soundpick_orms.admin.admin_user import admin_user_orm
from soundpick_orms.admin.admin_group import admin_group_orm
from soundpick_orms.admin.admin_group_option import admin_group_option_orm
from app.model.admin_user import AdminUser
from app.composer.date_parser import DateParser

from app import app, db
import os
from datetime import datetime, timedelta

user = Blueprint('user', __name__)

SESSION_KEY_LIST = ['is_logged_in', 'name', 'email', 'phone', 'birth', 'state']

@group_valid_required
@user.route('', methods=['GET'])
def user_list():
    json_struct = {
        'user_uid' : None,
        'name' : '',
        'email' : '',
        'reg_date' : None,
        'last_logged_in' : None,
        'admin_group' : {
            'id' : None,
            'name' : '',
            'is_valid' : None
        },
        'is_valid' : None
    }
    result = AdminUser().get_admin_user_list(
        from_date=DateParser(request.args.get('from_date')).date_type,
        to_date=DateParser(request.args.get('to_date')).date_type,
        is_valid=request.args.get('is_valid'),
        keyword=request.args.get('keyword'))
    return jsonify({
        'message': 'SUCCESS',
        'response': {
            'meta' : {
                'total_count' : result.count
            },
            'document' : result.get_json(json_struct)
        }
    })

@group_valid_required
@login_required_admin
@user.route('/auth/<int:user_uid>', methods=['POST'])
def set_auth(user_uid):
    if 'SUPER' in session['state']:
        user = admin_user_orm().query.filter_by(user_uid=user_uid).first()
        if user:
            user.is_valid = request.get_json()['is_valid']
            user.state = request.get_json()['state']
            db.session.add(user)
            db.session.commit()

            user_repr = ['user_uid', 'email', 'name',
                         'state', 'is_valid', 'reg_date', 'last_logged_in']
            return jsonify({
                'status': 201,
                'message': 'SUCCESS',
                'response': {_col: getattr(user, _col) for _col in user_repr}
            })
        else:
            return jsonify({
                'status': 204,
                'message': 'USER_IS_NOT_EXIST'
            })
    else:
        return jsonify({
            'status': 204,
            'message': 'YOU_HAVE_NOT_PRIVILIGE'
        })


@user.route('/session', methods=['GET'])
def get_session():
    info = {
        'is_logged_in': session.get('is_logged_in', False),
        'name': session.get('name', None),
        'state': session.get('state', None),
        'email': session.get('email', None)
    }
    return jsonify(info)


@user.route('/join', methods=['POST'])
def join():
    admin_group = admin_group_orm().query.filter_by(admin_group_code=request.get_json()['groupCode']).first()
    if admin_group:
        old_user = admin_user_orm().query.filter_by(email=request.get_json()['email']).all()
        if old_user:
            return jsonify({
                'status': 204,
                'message': 'EMAIL_ADDRESS_WAS_ALREADY_EXIST'
            })
        else:
            new_user = admin_user_orm()
            new_user.email = request.get_json()['email']
            new_user.password = generate_password_hash(request.get_json()['password'])
            new_user.name = request.get_json()['name']
            new_user.admin_group_id = admin_group.id
            new_user.is_valid = False
            new_user.state = ['ATNDN']
            db.session.add(new_user)
            db.session.commit()
            return jsonify({
                'status': 201,
                'message': 'SUCCESS'
            })

@group_valid_required
@login_required_admin
@user.route('/valid/<int:user_uid>', methods=['POST'])
def valid(user_uid):
    result = AdminUser().set_validation(user_uid=user_uid, is_valid=request.get_json()['is_valid'])
    if result.count:
        return jsonify({
            'message' : 'SUCCESS'
        })
    else:
        return jsonify({
            'message' : 'FAIL'
        })


@user.route('/login', methods=['POST'])
def login():
    email = request.get_json()['email']
    password = request.get_json()['password']

    user = admin_user_orm().query.filter_by(email=email).first()
    if user:
        group = admin_group_orm().query.filter_by(id=user.admin_group_id).first()
        if group:
            if group.is_valid:
                if user.is_valid:
                    if check_password_hash(user.password, password):
                        user.last_logged_in = datetime.utcnow() + timedelta(hours=9)
                        db.session.add(user)
                        db.session.commit()
                        set_session(user)
                        return jsonify({
                            'status': 200,
                            'message': 'SUCCESS'
                        }), 200
                    else:
                        return jsonify({
                            'status' : 204,
                            'message' : 'PASSWORD_IS_NOT_MATCHED'
                        })
                else:
                    session.clear()
                    return jsonify({
                        'status': 204,
                        'message': 'USER_IS_NOT_VALID'
                    })
            else:
                session.clear()
                return jsonify({
                    'status' : 204,
                    'message' : 'GROUP_IS_NOT_VALID'
                })
        else:
            session.clear()
            return jsonify({
                'status' : 204,
                'message' : 'GROUP_IS_NOT_EXIST'
            })
    else:
        session.clear()
        return jsonify({
            'status': 204,
            'message': 'USER_IS_NOT_EXIST'
        }), 200


def set_session(user_info):
    session['user_uid'] = user_info.user_uid
    session['is_logged_in'] = True
    session['state'] = user_info.state
    session['email'] = user_info.email
    session['name'] = user_info.name
    session['group_name'] = user_info.admin_group.name
    session['group_id'] = user_info.admin_group.id
    session['group_is_valid'] = user_info.admin_group.is_valid
    session['is_valid'] = user_info.is_valid


@user.route('/session/destroy', methods=['GET'])
def logout():
    session.clear()
    return redirect('/', code=302)


@user.route('/password', methods=['POST'])
def password_change():
    user = admin_user_orm().query.filter_by(
        user_uid=session['user_uid']).first()
    if check_password_hash(user.password, request.get_json()['present_password']):
        user.password = generate_password_hash(request.get_json()['next_password'])
        db.session.add(user)
        db.session.commit()

        return jsonify({
            'status': 201,
            'message': 'SUCCESS'
        })
    else:
        return jsonify({
            'status': 204,
            'message': 'PASSWORD_IS_NOT_VALID'
        })

@user.route('/group', methods=['GET', 'POST'])
def admin_group():
    json_struct = {
        'id' : None,
        'name' : '',
        'is_valid' : None,
        'reg_date' : None,
        'admin_group_code' : None
    }
    if request.method == 'GET':
        result = AdminUser().get_group_list(
            from_date=DateParser(request.args.get('from_date')).date_type,
            to_date=DateParser(request.args.get('to_date')).date_type,
            is_valid=request.args.get('is_valid'),
            keyword=request.args.get('keyword'))
        return jsonify({
            'message' : 'SUCCESS',
            'response' : {
                'meta' : {
                    'total_count' : result.count
                },
                'document' : result.get_json(json_struct)
            }
        })
    elif request.method == 'POST':
        result = AdminUser().add_group(request.get_json())
        if result.count:
            return jsonify({
                'message' : 'SUCCESS',
                'response' : {
                    'document' : result.get_json(json_struct)[0]
                }
            })