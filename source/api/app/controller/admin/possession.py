# _*_ coding: utf-8 _*_
from flask import Blueprint, render_template, jsonify, request, session, redirect

from sqlalchemy import asc

from app import app
from app.config import static as STATIC
import os
import json
from datetime import datetime, timedelta

from app.composer.decorator import login_required, valid_required, login_required_admin, group_valid_required
from app.model.custom.possession import Possession

from app.composer.date_parser import DateParser

possession = Blueprint('possession', __name__)

@group_valid_required
@valid_required
@possession.route('/<int:id>', methods=['GET'])
def get_possession(id):
    with open(os.path.dirname(os.path.dirname(__file__)) + '/../json/possession.json', 'r') as f:
        result = Possession().get_possession(id=id)
        return jsonify({
            'message': 'SUCCESS',
            'response': {
                'meta': {},
                'document': result.get_json(json.loads(f.read()))[0]
            }
        })

@group_valid_required
@valid_required
@possession.route('/customer/<int:id>', methods=['GET'])
def get_possession_by_customer(id):
    with open(os.path.dirname(os.path.dirname(__file__)) + '/../json/possession.json', 'r') as f:
        result = Possession().get_possession(customer_id=id)
        return jsonify({
            'message': 'SUCCESS',
            'response': {
                'meta': {
                    'total_count': result.count
                },
                'document': result.get_json(json.loads(f.read()))
            }
        })

@group_valid_required
@valid_required
@possession.route('', methods=['GET'])
def possession_list():
    if request.method == 'GET':
        result = Possession().get_possession_list(
            is_expired=request.args.get('is_expired'),
            from_date=DateParser(request.args.get('from_date')).date_type,
            to_date=DateParser(request.args.get('to_date')).date_type,
            product_id=request.args.getlist('product'),
            keyword=request.args.get('keyword'),
            page=request.args.get('page', type=int),
            per_page=30
        )
        with open(os.path.dirname(os.path.dirname(__file__)) + '/../json/possession.json', 'r') as f:
            return jsonify({
                'message': 'SUCCESS',
                'response': {
                    'meta': {
                        'total_count': result.count
                    },
                    'document': result.get_json(json.loads(f.read()))
                }
            })

@group_valid_required
@valid_required
@possession.route('/expired', methods=['GET'])
def possession_expired_of():
    if request.method == 'GET':
        result = Possession().get_possession_expired_of(days = request.args.get('days', type=int))
        with open(os.path.dirname(os.path.dirname(__file__)) + '/../json/possession.json', 'r') as f:
            return jsonify({
                'message': 'SUCCESS',
                'response': {
                    'meta': {
                        'total_count': result.count
                    },
                    'document': result.get_json(json.loads(f.read()))
                }
            })

@group_valid_required
@valid_required
@possession.route('/receipt/upload', methods=['POST'])
def receipt_imagefile_upload():
    def upload():
        def allowed_file(filename):
            ALLOWED_EXTENSIONS = set(
                ['png', 'jpg', 'jpeg', 'gif', 'PNG', 'JPG', 'JPEG', 'GIF'])
            return '.' in filename and \
                filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

        file = request.files.getlist('upload_files[]')[0]
        if file and allowed_file(file.filename):
            filename = file.filename.replace(" ", "_")
            file.save(STATIC.FILE_PATH.format('receipt', filename))
            return filename
        else:
            return ""

    if request.method == 'POST':
        filename = upload()
        return jsonify({"filename": filename})
