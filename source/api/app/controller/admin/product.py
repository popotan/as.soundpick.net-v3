# _*_ coding: utf-8 _*_
from flask import Blueprint, render_template, jsonify, request, session, redirect, g

from sqlalchemy import asc

from app import app
import os
import json
from datetime import datetime, timedelta

from app.composer.decorator import login_required, valid_required, login_required_admin, group_valid_required
from app.model.product.brand import Brand
from app.model.product.product import Product
from app.model.product.serial import Serial

from app.composer.date_parser import DateParser

product = Blueprint('product', __name__)

@valid_required
@group_valid_required
@product.route('', methods=['GET', 'POST'])
def get_product_list():
    json_struct = {
        "id": None,
        "name": "",
        "lineup": "",
        "validation": None,
        "warranty_day_length": 0,
        "reg_date": None,
        "brand": {
            "id": None,
            "name": "",
            "reg_date": None
        },
        "image": [
            {
                "id": None,
                "image_filename": "",
                "image_size_width": 0,
                "image_size_height": 0,
                "reg_date": None
            }
        ],
        "option": None
    }
    if request.method == 'GET':
        result = Product().get_product_list(
            from_date=DateParser(request.args.get('from_date')).date_type,
            to_date=DateParser(request.args.get('to_date')).date_type,
            brand=request.args.getlist('brand', type=int),
            keyword=request.args.get('keyword'),
            page=request.args.get('page', type=int),
            per_page=30
        )
        return jsonify({
            'message': 'SUCCESS',
            'response': {
                'meta': {
                    'total_count': result.count
                },
                'document': result.get_json(json_struct)
            }
        })
    elif request.method == 'POST':
        result = Product().add_product(request.get_json())
        return jsonify({
            'message': 'SUCCESS',
            'response': {
                'meta': {},
                'document': result.get_json(json_struct)[0]
            }
        })

@valid_required
@group_valid_required
@product.route('/serial/<serial>', methods=['GET'])
def get_product_info_by_serial(serial):
    json_struct = {
        'id': 0,
        'serial': '',
        'is_expired': True,
        'expired_date': None,
        'product': {
            'id': 0,
            'brand_id': 0,
            'name': '',
            'warranty_day_length': None,
            'lineup': '',
            'validation': False
        }
    }
    result = Serial().get_info(serial=serial).get_json(json_struct)
    if len(result) > 0:
        return jsonify({
            'message': 'SUCCESS',
            'response': {
                'meta': {},
                'document': result[0]
            }
        })
    else:
        return jsonify({
            'message': 'SERIAL_IS_NOT_EXIST'
        })

@valid_required
@group_valid_required
@product.route('/search', methods=['GET'])
def product_search_by_keyword():
    json_struct = {
        'id': 0,
        'brand_id': 0,
        'name': '',
        'warranty_day_length': None,
        'lineup': '',
        'validation': False,
        'brand': {
            'id': '',
            'name': '',
            'reg_date': None
        }
    }
    result = Product().get_product_list(
        keyword=request.args.get('keyword')).get_json(json_struct)
    if len(result) > 0:
        return jsonify({
            'message': 'SUCCESS',
            'response': {
                'meta': {},
                'document': result
            }
        })
    else:
        return jsonify({
            'message': 'MATCHED_PRODUCT_IS_NOT_EXIST'
        })


@valid_required
@group_valid_required
@product.route('/image', methods=['POST'])
def upload_images():
    def allowed_file(filename):
        ALLOWED_EXTENSIONS = set(['jpeg', 'png', 'jpg', 'gif'])
        return '.' in filename and \
            filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

    def get_image_size(filepath):
        from PIL import Image
        im = Image.open(filepath)
        return im.size

    filename_arr = []
    files = request.files.getlist('upload_files[]')
    for file in files:
        if file and allowed_file(file.filename.lower()):
            filename = str(round(datetime.now().timestamp())) + \
                '_' + file.filename.replace(" ", "_")
            file.save(os.getcwd() + '/app/static/product/' + filename)
            iwidth, iheight = get_image_size(
                os.getcwd() + '/app/static/product/' + filename)
            filename_arr.append({'filename': filename, 'size': {
                                'width': iwidth, 'height': iheight}})
    return json.dumps(filename_arr)
