# _*_ coding: utf-8 _*_
from flask import Blueprint, render_template, jsonify, request, session, redirect

from sqlalchemy import asc

from app import app
import os
from datetime import datetime, timedelta
from urllib.parse import unquote

from app.composer.decorator import login_required, valid_required, login_required_admin, group_valid_required
from app.model.custom.customer import Customer

customer = Blueprint('customer', __name__)


@customer.route('/<int:id>', methods=['GET'])
def get_customer_info(id):
    pass


@customer.route('', methods=['GET'])
def get_customer_list():
    pass


@customer.route('/search', methods=['GET'])
def get_customer_by_keyword():
    json_struct = {
        'id': 0,
        'name': '',
        'phone1': '',
        'phone2': '',
        'reg_date': None
    }
    result = Customer().get_info(keyword=request.args.get('keyword'))
    if result:
        return jsonify({
            'message': 'SUCCESS',
            'response': {
                'meta': {
                    'total_count': result.count
                },
                'document': result.get_json(json_struct)
            }
        })
    else:
        return jsonify({
            'message': 'MATCHED_CUSTOMER_IS_NOT_EXIST'
        })


@customer.route('/phone', methods=['GET'])
def get_customer_phone_is_exist():
    if request.method == 'GET':
        result = Customer().is_phone_exist(request.args.get('phone'))
        return jsonify({
            'message': 'SUCCESS',
            'response': {
                'meta': {},
                'document': {'is_exist' : True if result.count > 0 else False}
            }
        })
