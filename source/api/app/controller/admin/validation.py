# _*_ coding: utf-8 _*_
from flask import Blueprint, render_template, jsonify, request, session, redirect, g

from sqlalchemy import asc

from app import app
import os
import json
from datetime import datetime, timedelta

from app.composer.decorator import login_required, valid_required, login_required_admin, group_valid_required
from app.model.product.brand import Brand
from app.model.product.product import Product
from app.model.product.serial import Serial

from app.composer.date_parser import DateParser
from app.composer.SerialFileParser import SerialFileParser
import openpyxl
import os
import sys
import math
from urllib.parse import unquote
from datetime import datetime, timedelta

validation = Blueprint('validation', __name__)


@validation.route('/serial', methods=['GET', 'POST'])
def get_validation_list():
    json_struct = {
        'id': None,
        'serial': '',
        'is_expired': None,
        'expired_date': None,
        'reg_date': None,
        'product': {
            'id': None,
            'name': '',
            'warranty_day_length': 0,
            'brand': {
                'id': None,
                'name': '',
                'reg_date': None
            }
        }
    }
    if request.method == 'GET':
        result = Serial().get_serial_list(
            from_date=DateParser(request.args.get('from_date')).date_type,
            to_date=DateParser(request.args.get('to_date')).date_type,
            product_id=request.args.getlist('product'),
            keyword=request.args.get('keyword'),
            page=request.args.get('page', type=int),
            per_page=30
        )
        return jsonify({
            'message': 'SUCCESS',
            'response': {
                'meta': {
                    'total_count': result.count
                },
                'document': result.get_json(json_struct)
            }
        })
    elif request.method == 'POST':
        return input_serials()


@validation.route('/target/brand', methods=['GET'])
def get_validation_target_brand():
    json_struct = {
        'id': 0,
        'name': ''
    }
    # result = Brand().get_validation_target_list().get_json(json_struct)
    result = Brand().get_brand_list().get_json(json_struct)
    return jsonify({
        'message': 'SUCCESS',
        'response': {
            'meta': {},
            'document': result
        }
    })


@validation.route('/target/brand/<int:brand_id>/product', methods=['GET'])
def get_validation_target_product_of_brand(brand_id):
    json_struct = {
        'id': 0,
        'name': ''
    }
    result = Product().get_validation_target_list(brand_id).get_json(json_struct)
    return jsonify({
        'message': 'SUCCESS',
        'response': {
            'meta': {},
            'document': result
        }
    })

from soundpick_orms.product.serial import serial_orm
from app import db
@validation.route('/serial/file', methods=['GET', 'POST'])
def post_serial_file():
    def upload():
        def allowed_file(filename):
            ALLOWED_EXTENSIONS = set(
                ['csv', 'xlsx', 'xls', 'CSV', 'XLSX', 'XLS'])
            return '.' in filename and \
                filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

        file = request.files.getlist('upload_files[]')[0]
        if file and allowed_file(file.filename):
            filename = file.filename.replace(" ", "_")
            file.save(os.getcwd() + '/app/static/serial/' + filename)
            return filename
        else:
            return ""

    if request.method == 'POST':
        filename = upload()
        return jsonify({
            'message': 'SUCCESS',
            'response': {
                'meta': {
                    'filename': filename
                },
                'document': {
                    'sheet': get_sheet_names(filename)
                }
            }
        })


def get_sheet_names(filename):
    filepath = os.getcwd() + '/app/static/serial/' + unquote(filename)
    sfp = SerialFileParser(filename=filepath)
    return sfp.get_sheet_names()


@validation.route('/serial/file/<filename>/sheet/<sheetname>/columns', methods=['GET'])
def get_column_name(filename, sheetname):
    filepath = os.getcwd() + '/app/static/serial/' + unquote(filename)
    sfp = SerialFileParser(filename=filepath)
    sfp = sfp.set_active_sheet(sheet_name=unquote(sheetname))
    return jsonify({
        'message': 'SUCCESS',
        'response': {
            'meta': {},
            'document': sfp.get_column_names(unquote(sheetname))
        }
    })


def input_serials():
    if request.method == 'POST':
        count = 0
        for req in request.get_json()['works']:
            filepath = os.getcwd() + '/app/static/serial/' + unquote(req['filename'])
            serial_list = SerialFileParser(filename=filepath).set_active_sheet(
                sheet_name=req['sheet']).get_selected_row(column_num=req['columnIndex'])

            for key in serial_list:
                k = serial_orm().query.filter_by(serial=key).first()

                if not k:
                    k = serial_orm()
                    k.serial = key
                    k.product_id = req['product']['id']

                if req['is_expired'] and not k.is_expired:
                    k.expired_date = dateime.utcnow() + timedelta(hours=9)
                elif not req['is_expired'] and k.is_expired:
                    k.expired_date = None

                k.is_expired = req['is_expired']

                db.session.add(k)
                count += 1
                db.session.flush()

                k.admin = {'name': session['name']}
                k.comment = request.get_json()['comment']
        db.session.commit()

        return jsonify({
            'message': 'SUCCESS',
            'response': {
                'meta': {
                    'total_count': count
                }
            }
        })
