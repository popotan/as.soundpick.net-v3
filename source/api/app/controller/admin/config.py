# _*_ coding: utf-8 _*_
from flask import Blueprint, render_template, jsonify, request, session, redirect, g

from sqlalchemy import asc

from app import app, db
import os
import json
from datetime import datetime, timedelta

from app.composer.decorator import login_required, valid_required, login_required_admin, group_valid_required
from soundpick_orms.custom.after_service.as_config import as_config_orm

config = Blueprint('config', __name__)

@valid_required
@config.route('/invoice/columns', methods=['GET', 'PUT'])
def get_invoice_output_info():
    invoice_columns = as_config_orm().query.filter_by(user_uid=session['user_uid'], attribute='invoice_columns').first()
    if request.method == 'GET':
        return jsonify({
            'message' : 'SUCCESS',
            'response' : {
                'meta' : {},
                'document' : invoice_columns.value if invoice_columns else ''
            }
        })
    elif request.method == 'PUT':
        if not invoice_columns:
            invoice_columns = as_config_orm()
            invoice_columns.attribute = 'invoice_columns'
            invoice_columns.user_uid = session['user_uid']
        invoice_columns.value = request.get_json()['columns']
        db.session.add(invoice_columns)
        db.session.commit()
        return jsonify({
            'message' : 'SUCCESS',
            'response' : {
                'meta' : {},
                'document' : invoice_columns.value
            }
        })

@valid_required
@config.route('/sms/template', methods=['GET', 'POST'])
def get_sms_as_template():
    if request.method == 'GET':
        templates = [
            as_config_orm().query.filter_by(admin_group_id=session['group_id'], attribute='sms_on_invoice_added').first(),
            as_config_orm().query.filter_by(admin_group_id=session['group_id'], attribute='sms_on_as_case_added').first()
        ]
        with open(os.path.dirname(os.path.dirname(__file__)) + '/../json/sms_variable_list.json', 'r') as f:
            return jsonify({
                'message':'SUCCESS',
                'response' : {
                    'meta' : {},
                    'document' : {
                        'variable_list' : json.loads(f.read()),
                        'template' : {
                            'on_invoice_added' : templates[0].value if templates[0] else '',
                            'on_as_case_added' : templates[1].value if templates[1] else ''
                        }
                    }
                }
            })
    elif request.method == 'POST':
        attr = request.get_json()['sms']['attribute']
        template = as_config_orm().query.filter_by(admin_group_id=session['group_id'], attribute='sms_' + attr).first()
        if not template:
            template = as_config_orm()
            template.admin_group_id = session['group_id']
            template.attribute = 'sms_' + attr
        template.value = request.get_json()['sms']['value']
        db.session.add(template)
        db.session.commit()
        return jsonify({
            'message' : 'SUCCESS'
        })
