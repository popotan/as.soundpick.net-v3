# _*_ coding: utf-8 _*_
from flask import Flask, send_from_directory, session, request, render_template, g
from flask_session import Session
from flask_bcrypt import Bcrypt

##########
# 공동 FUNCLASS Object Relationship Model 위치 정의
##########
import os
import sys
sys.path.append(os.path.dirname(os.path.abspath('/mnt/www/soundpick_orms')))
from soundpick_orms import database
##### End #####

from datetime import timedelta

app = Flask(__name__, template_folder='template', static_url_path='/static')
app.secret_key = 'as_soundpick_net'

app.config.from_object(database)
from flask_sqlalchemy import SQLAlchemy
db = SQLAlchemy()
db.init_app(app)

# 쿠키관련 설정
app.config.update(
    SESSION_TYPE='sqlalchemy',
    SESSION_COOKIE_NAME='spcsas',
    SESSION_COOKIE_HTTPONLY=False,
    SESSION_COOKIE_SECURE=False,
    PERMANENT_SESSION_LIFETIME=timedelta(hours=2),
    SESSION_REFRESH_EACH_REQUEST=True
)
# app.config['SQLALCHEMY_ECHO'] = True
# Session().init_app(app)

bcrypt = Bcrypt(app)


@app.route('/static/<path:path>')
def send_js(path):
    """
            static 파일들을 템플릿상에서 url_for 없이 사용하기 위해 라우팅 제공
    """
    return send_from_directory('static/', path)


@app.before_request
def _before_request_():
    pass

@app.after_request
def add_header(response):
    """
    Add headers to both force latest IE rendering engine or Chrome Frame,
    and also to cache the rendered page for 10 minutes.
    """
    response.headers['X-UA-Compatible'] = 'IE=Edge,chrome=1'
    response.headers['Cache-Control'] = 'public, max-age=0'
    response.headers['Access-Control-Allow-Origin'] = '*'
    return response


@app.route('/api/ping', methods=['GET'])
def ping_test():
    print('ping')
    return 'ping', 200


@app.route('/api/test', methods=['GET'])
def orm_test():
    from orms.fidue.buy_user import buy_user_orm
    print(buy_user_orm().query.count())
    return 'test', 200

def formalize(params):
    def formal(params):
        for key in params:
            if type(params[key]) is dict:
                params[key] = formal(params[key])
            elif type(params[key]) is list:
                for idx, obj in enumerate(params[key]):
                    params[key][idx] = formal(params[key][idx])
            if key.startswith('__'):
                key = key.replace('__', '')
        return params
    return formal(params)


from app.controller.admin.user import user as admin_user
from app.controller.admin.after_service import after_service as admin_after_service
from app.controller.admin.product import product as admin_product
from app.controller.admin.brand import brand as admin_brand
from app.controller.admin.possession import possession as admin_possession
from app.controller.admin.customer import customer as admin_customer
from app.controller.admin.validation import validation as admin_validation
from app.controller.admin.config import config as admin_config
from app.controller.admin.log import log as admin_log

app.register_blueprint(admin_user, url_prefix='/api/admin/user')
app.register_blueprint(admin_after_service, url_prefix='/api/admin/as')
app.register_blueprint(admin_product, url_prefix='/api/admin/product')
app.register_blueprint(admin_brand, url_prefix='/api/admin/brand')
app.register_blueprint(admin_possession, url_prefix='/api/admin/possession')
app.register_blueprint(admin_validation, url_prefix='/api/admin/validation')
app.register_blueprint(admin_customer, url_prefix='/api/admin/customer')
app.register_blueprint(admin_config, url_prefix='/api/admin/config')
app.register_blueprint(admin_log, url_prefix='/api/admin/log')