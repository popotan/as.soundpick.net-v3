import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { GlobalModule } from './global/global.module';

import { AppComponent } from './app.component';
import { IndexComponent } from './route/index/index.component';
import { AsComponent } from './route/as/as.component';
import { ValidationComponent } from './route/validation/validation.component';
import { CsComponent } from './route/cs/cs.component';
import { JoinComponent } from './route/join/join.component';
import { LoginComponent } from './route/login/login.component';
import { PossessionComponent } from './route/possession/possession.component';
import { CoreModule } from './core/core.module';
import { AsInfoComponent } from './route/as/as-info/as-info.component';
import { ConfigComponent } from './route/config/config.component';
import { ExchangeProductComponent } from './route/as/as-info/exchange-product/exchange-product.component';
import { AsCaseComponent } from './route/as/as-info/as-case/as-case.component';
import { GoodsComponent } from './route/goods/goods.component';
import { BrandComponent } from './route/goods/brand/brand.component';
import { BrandInfoComponent } from './route/goods/brand/brand-info/brand-info.component';
import { ProductInfoComponent } from './route/goods/product/product-info/product-info.component';
import { ProductComponent } from './route/goods/product/product.component';
import { UploadSerialComponent } from './route/validation/upload-serial/upload-serial.component';
import { SerialInfoComponent } from './route/validation/serial-info/serial-info.component';
import { SessionService } from './core/session.service';
import { SessionGuard } from './core/session.guard';
import { SuperAdminComponent } from './route/super-admin/super-admin.component';
import { UserInfoComponent } from './route/super-admin/user-info/user-info.component';
import { AdminGroupComponent } from './route/admin-group/admin-group.component';
import { GroupInfoComponent } from './route/admin-group/group-info/group-info.component';

@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    AsComponent,
    ValidationComponent,
    CsComponent,
    JoinComponent,
    LoginComponent,
    PossessionComponent,
    AsInfoComponent,
    ConfigComponent,
    ExchangeProductComponent,
    AsCaseComponent,
    GoodsComponent,
    BrandComponent,
    ProductComponent,
    BrandInfoComponent,
    ProductInfoComponent,
    UploadSerialComponent,
    SerialInfoComponent,
    SuperAdminComponent,
    UserInfoComponent,
    AdminGroupComponent,
    GroupInfoComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    GlobalModule,
    CoreModule,
    RouterModule.forRoot([
      {
        path: '',
        canActivateChild: [SessionGuard],
        children: [
          {
            path: '',
            component: IndexComponent
          }, {
            path: 'as',
            component: AsComponent
          }, {
            path: 'goods',
            component: GoodsComponent
          }, {
            path: 'validation',
            component: ValidationComponent
          }, {
            path: 'possession',
            component: PossessionComponent
          }, {
            path: 'cs',
            component: CsComponent
          }, {
            path: 'config',
            component: ConfigComponent
          }, {
            path: 'super_admin',
            component : SuperAdminComponent
          }, {
            path : 'admin_group',
            component : AdminGroupComponent
          }
        ]
      }, {
        path: 'join',
        component: JoinComponent
      }, {
        path: 'login',
        component: LoginComponent
      }
    ])
  ],
  providers: [SessionService, SessionGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
