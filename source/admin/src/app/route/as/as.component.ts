import { Component, OnInit, ViewChild, ComponentRef, ElementRef, ViewContainerRef, ComponentFactoryResolver } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators, FormArray } from '@angular/forms';

import { AsService } from './as.service';
import { FormAssistantService } from '../../global/service/form-assistant.service';
import { AsInfoComponent } from './as-info/as-info.component';
import { Possession } from '../../class/possession';
import { SplitedViewTable } from '../../global/extend/splited-view-table';
import { ASCase } from '../../class/ascase';
import { SplitedViewTableComponent } from '../../global/component/splited-view-table/splited-view-table.component';

@Component({
  selector: 'app-as',
  templateUrl: './as.component.html',
  styleUrls: ['./as.component.css'],
  providers: [AsService, FormAssistantService],
  entryComponents: [AsInfoComponent]
})
export class AsComponent extends SplitedViewTable implements OnInit {

  /**
   * @memberof AsComponent
   */
  @ViewChild(SplitedViewTableComponent) svtComponent;

  /**
   * @memberof AsComponent
   */
  @ViewChild('InfoContainer', { read: ViewContainerRef }) infoContainer;

  /**
   * @description 검색조건
   * @memberof AsComponent
   */
  searchCondition = {
    is_expired: '처리중',
    from_date: this.formAssistantService.calculateDateTerm(-7, new Date()),
    to_date: new Date(),
    brand: '',
    product: [],
    symptom: [],
    keyword: '',
    page: 1
  };

  /**
   * @description 검색 가능한 브랜드 리스트. 선택을 통해 searchCondition.brand에 입력.
   * @memberof AsComponent
   */
  brandListOnSearchCondition = [];

  /**
   * @memberof AsComponent
   */
  searchResultCount = 0;

  /**
   * info component 삽입을 위한 property
   * @memberof AsComponent
   */
  createdComponent = null;

  /**
   * @description Creates an instance of AsComponent.
   * @param {AsService} asService
   * @param {FormAssistantService} formAssistantService
   * @param {ViewContainerRef} vcr
   * @param {ComponentFactoryResolver} cfr
   * @memberof AsComponent
   */
  constructor(
    private asService: AsService,
    private formAssistantService: FormAssistantService,
    private vcr: ViewContainerRef,
    private cfr: ComponentFactoryResolver
  ) {
    super(
      [['케이스ID', '처리상태', '이름', '신청시각', '연락처'], ['제품번호', '브랜드', '제품명', '구매일', '보증종료일', '송장번호', '처리타입', '보기']],
      [80, 100, 80, 150, 150, 150, 150, 150, 150, 150, 150, 80, 100]
    );
  }

  ngOnInit() {
    this.asService.getASTargetBrand();
    this.asService.getAsSymptomList();
  }

  /**
   * @description 검색 기간 설정
   * @param {number} dateLength
   * @memberof AsComponent
   */
  setReceptionPeriod(dateLength: number) {
    this.searchCondition.from_date = this.formAssistantService.calculateDateTerm(dateLength, new Date());
    this.searchCondition.to_date = this.formAssistantService.calculateDateTerm(0, new Date());
  }

  /**
   * @description searchCondition 값을 바탕으로 검색 수행. 검색결과를 splited view table 포멧에 맞게 변형.
   * @memberof AsComponent
   */
  search() {
    this.asService.getASList(this.searchCondition)
      .subscribe(result => {
        if (result['message'] === 'SUCCESS') {
          this.asService.asList = result['response']['document'].map(v => Object.assign(new ASCase(), v));
          const arr = [[], []];
          this.searchResultCount = result['response']['meta']['total_count'];
          this.asService.asList.forEach(res => {
            const staticArea = [
              { text: res.id },
              { text: res.is_expired },
              { text: res.possession.exchange_log[0].customer.name },
              { text: this.formAssistantService.convertPyDatetime(res.reg_date, 'string') },
              { text: res.possession.exchange_log[0].customer.phone1 }
            ];
            const scrollableArea = [
              {
                text: (res.possession.exchange_log[0].serial.serial)
                  ? res.possession.exchange_log[0].serial.serial : '-'
              },
              { text: res.possession.exchange_log[0].product.brand.name },
              { text: res.possession.exchange_log[0].product.name },
              { text: this.formAssistantService.convertPyDatetime(res.possession.purchase_date, 'string') },
              { text: this.formAssistantService.convertPyDatetime(res.possession.warranty_expired_date, 'string') },
              { text: res.process.invoice ? res.process.invoice : '', actionType: 'input', methodName: 'inputInvoice' },
              { text: (res.process.invoice_fee_type === 'paid') ? '유상배송' : '무상배송' },
              { text: '정보수정/보기', actionType: 'button', methodName: 'showInfo' }
            ];
            arr[0].push(staticArea);
            arr[1].push(scrollableArea);
          });
          this.SVTRows = arr;
        }
      });
  }

  /**
   * @description splited view table에서 선택된 as신청정보의 인덱스를 바탕으로 구매정보 획득 후, AsInfoComponent 생성
   * @param {number} [index=-1]
   * @memberof AsComponent
   */
  showInfo(index: number = -1) {
    this.infoContainer.clear();
    if (index > -1) {
      this.asService.getPossession(this.asService.asList[index].possession.id)
        .subscribe(result => {
          if (result['message'] === 'SUCCESS') {
            const factory = this.cfr.resolveComponentFactory(AsInfoComponent);
            this.createdComponent = this.infoContainer.createComponent(factory);
            this.createdComponent.instance.establishedInfo = Object.assign(new Possession(), result['response']['document']);
            this.createdComponent.instance.initialAsCaseId = this.asService.asList[index].id;
            this.createdComponent.instance.ref = this.createdComponent;
          }
        });
    } else {
      const factory = this.cfr.resolveComponentFactory(AsInfoComponent);
      this.createdComponent = this.infoContainer.createComponent(factory);
      this.createdComponent.instance.establishedInfo = new Possession();
      this.createdComponent.instance.ref = this.createdComponent;
    }
  }

  /**
   * @description splited view table에서 바로 출고용 invoice number(송장번호) 입력
   * @param {*} index
   * @memberof AsComponent
   */
  inputInvoice(index) {
    let invoice = this.SVTRows[1][index][5].text;
    this.asService.updateInvoice(this.asService.asList[index]['id'], invoice)
      .subscribe(result => {
        if (result['message'] === 'SUCCESS') {
          if (index - 1 < this.asService.asList.length) {
            // let inputs = this.svtComponent.el.nativeElement.querySelector('input[type=text]');
            // inputs[index + 1].focus();
          };
        } else if (result['message'] === 'ADDRESS_OR_POSTCODE_IS_NOT_EXIST') {
          alert('주소지 또는 우편번호가 입력되지 않았습니다.\r\n입력 후 송장번호 기입이 가능합니다.');
        } else {
          alert('오류가 발생하였습니다.');
        }
      });
  }

  /**
   * @description 송장정보를 엑셀파일형식으로 출력
   * @memberof AsComponent
   */
  exportInvoice() {
    let temp = [];
    temp.push(['받는분성명', '받는분전화번호', '받는분기타연락처', '우편번호', '받는분주소(전체,분할)', '품목명/개수', '배송메세지', '박스수량']);
    if (this.SVTSelectedIndexes.length) {
      this.SVTSelectedIndexes.forEach(index => {
        const info = this.asService.asList[index];
        const row = [
          info.customer.name,
          info.customer.phone1,
          info.customer.phone2,
          info.process.postcode,
          info.process.address,
          info.possession.exchange_log[info.possession.exchange_log.length - 1].product.brand.name +
          ' ' +
          info.possession.exchange_log[info.possession.exchange_log.length - 1].product.name +
          ' AS',
          '',
          '1'
        ];
        temp.push(row);
      });
      this.asService.exportInvoice(temp)
        .subscribe(result => {
          if (result) {
            let url = window.URL.createObjectURL(result.data);
            let a = document.createElement('a');
            document.body.appendChild(a);
            a.setAttribute('style', 'display: none');
            a.href = url;
            a.download = result.filename;
            a.click();
            window.URL.revokeObjectURL(url);
            a.remove(); // remove the element
          }
        });
    } else {
      alert('선택된 열이 없습니다.');
    }
  }
}
