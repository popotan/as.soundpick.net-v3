import { Injectable } from '@angular/core';

import { Http, URLSearchParams, Response, ResponseContentType } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { apiURL, httpOptions } from '../../../environments/environment.prod';
import { FormAssistantService } from '../../global/service/form-assistant.service';

@Injectable()
export class AsService {
  /**
   * @description AS접수내역 검색조건, 증상
   * @memberof AsService
   */
  symptomList = [];

  /**
   * @description AS접수내역 검색조건, 브랜드
   * @memberof AsService
   */
  brandList = [];

  /**
   * @description AS접수내역 검색조건, 제품
   * @memberof AsService
   */
  productList = [];

  /**
   * @description AS접수내역 검색결과
   * @memberof AsService
   */
  asList = [];

  constructor(
    private $http: Http,
    private formAssistantService: FormAssistantService
  ) { }
  getASTargetBrand() {
    return this.$http.get(apiURL + '/as/target/brand', httpOptions)
      .map((res: Response) => res.json()).subscribe(result => {
        if (result['message'] === 'SUCCESS') {
          this.brandList = result['response']['document'];
        }
      });
  }
  getASTargetProduct(brandId) {
    return this.$http.get(apiURL + '/as/target/brand/' + brandId + '/product', httpOptions)
      .map((res: Response) => res.json()).subscribe(result => {
        if (result['message'] === 'SUCCESS') {
          this.productList = result['response']['document'];
        }
      });
  }
  getAsSymptomList() {
    return this.$http.get(apiURL + '/as/symptom')
      .map((res: Response) => res.json()).subscribe(result => {
        if (result['message'] === 'SUCCESS') {
          this.symptomList = result['response']['document'];
        }
      });
  }
  getASList(options) {
    const cOptions = Object.assign({}, options);
    cOptions['from_date'] = this.formAssistantService.convertToStringTypeOfDate(options['from_date']);
    cOptions['to_date'] = this.formAssistantService.convertToStringTypeOfDate(options['to_date']);
    httpOptions['params'] = cOptions;
    return this.$http.get(apiURL + '/as', httpOptions)
      .map((res: Response) => res.json());
  }
  getPossession(possessionId) {
    return this.$http.get(apiURL + '/possession/' + possessionId)
      .map((res: Response) => res.json());
  }
  getPostcode(address) {
    const headers = new Headers({ 'Authorization': 'KakaoAK cda27b0cbbce7296d2f50a8ede70005c' });
    return this.$http.get('https://dapi.kakao.com/v2/local/search/address.json', {
      params: { query: address }, headers: headers
    }).map((res: Response) => res.json());
  }
  addASCase(info, comment) {
    return this.$http.post(apiURL + '/as', { possession: this.formAssistantService.formalize(info), comment: comment })
      .map((res: Response) => res.json());
  }
  updateInvoice(asCaseId, invoice) {
    return this.$http.post(apiURL + '/as/invoice/' + asCaseId, { invoice: invoice })
      .map((res: Response) => res.json());
  }
  exportInvoice(invoice_list) {
    return this.$http.post(apiURL + '/as/invoice/export', { invoice_list: invoice_list }, {responseType : ResponseContentType.Blob})
      .map((res: Response) => {
        return {
          filename: this.formAssistantService.convertToStringTypeOfDate(new Date()) +
            '_' + 'AS_택배파일접수.xls',
          data: res.blob()
        };
      });
  }
}
