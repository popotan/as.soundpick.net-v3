import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { apiURL, staticURL, clone } from '../../../../../environments/environment.prod';
import { ExchangeLog } from '../../../../class/exchange-log';
import { ExchangeLogOption } from '../../../../class/exchange-log-option';
import { AsService } from '../../as.service';

@Component({
  selector: 'app-exchange-product',
  templateUrl: './exchange-product.component.html',
  styleUrls: ['./exchange-product.component.css']
})
export class ExchangeProductComponent implements OnInit {
  /**
   * @description 교환내역 수정여부
   * @memberof ExchangeProductComponent
   */
  isEditing = false;

  /**
   * @memberof ExchangeProductComponent
   */
  staticURL = staticURL;

  /**
   * @deprecated
   * @memberof ExchangeProductComponent
   */
  isExpand = false;

  /**
   * @description 전파받은 교환내역정보 원형
   * @type {ExchangeLog}
   * @memberof ExchangeProductComponent
   */
  __oExchange: ExchangeLog;

  /**
   * @description 교환내역정보 변경사항
   * @type {ExchangeLog}
   * @memberof ExchangeProductComponent
   */
  __uExchange: ExchangeLog;

  /**
   * @description 전파받은 교환내역정보를 저장하고 복사
   * @memberof ExchangeProductComponent
   */
  @Input('exchange') set exchange(v: ExchangeLog) {
    this.__oExchange = Object.assign(new ExchangeLog(), v);
    this.__uExchange = clone(this.__oExchange);
    if (!this.__uExchange.option) {
      this.__uExchange.option = new ExchangeLogOption();
    }
  };

  /**
   * @description 교환내역정보 변경사항을 전달할 EventEmitter
   * @type {EventEmitter<any>}
   * @memberof ExchangeProductComponent
   */
  @Output('onExchangeLogUpdated') onExchangeLogUpdated: EventEmitter<any> = new EventEmitter();

  /**
   * @description 교환내역정보 삭제를 위한 EventEmitter
   * @type {EventEmitter<any>}
   * @memberof ExchangeProductComponent
   */
  @Output('onExchangeLogRemoved') onExchangeLogRemoved: EventEmitter<any> = new EventEmitter();
  constructor(
    private asService: AsService
  ) { }

  ngOnInit() {
  }

  /**
   * @description 변경감지
   * @returns
   * @memberof ExchangeProductComponent
   */
  isDiff() {
    return JSON.stringify(this.__oExchange) !== JSON.stringify(this.__uExchange);
  }

  /**
   * @description 제품정보 수정 시, 수정된 제품정보 입력
   * @param {*} $event
   * @memberof ExchangeProductComponent
   */
  onProductSelected($event) {
    this.__uExchange.product = $event.product;
    if ($event.serial) {
      this.__uExchange.serial = $event.serial;
    } else {
      this.__uExchange.serial = null;
    }
  }

  /**
   * @description DaumMapApi 를 통해 입력된 주소의 우편번호를 파싱
   * @memberof ExchangeProductComponent
   */
  getPostcode() {
    if (this.__uExchange.option.address && this.__uExchange.option.address.length > 5) {
      this.asService.getPostcode(this.__uExchange.option.address)
        .debounceTime(500)
        .subscribe(result => {
          if (result.hasOwnProperty('documents') && result['documents'].length > 0) {
            if (result.documents[0].address_type === 'REGION_ADDR') {
              this.__uExchange.option.postcode = result.documents[0].address.zip_code;
            } else {
              this.__uExchange.option.postcode = result.documents[0].road_address.zone_no;
            }
          } else {
            alert('주소형식이 잘못되었습니다. 다시 한 번 확인해주시기 바랍니다.');
          }
        });
    } else {
      alert('주소를 6자 이상 입력해주세요!');
    }
  }

  /**
   * @description 교환내역 업데이트
   * @memberof ExchangeProductComponent
   */
  update() {
    this.onExchangeLogUpdated.emit(this.__uExchange);
    this.isEditing = false;
  }
}
