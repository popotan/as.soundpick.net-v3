import { Injectable } from '@angular/core';
import { Possession } from '../../../class/possession';
import { ExchangeLog } from '../../../class/exchange-log';
import { Subject, BehaviorSubject } from 'rxjs';
import { ASCase } from '../../../class/ascase';
import { Customer } from '../../../class/customer';
import { Product } from '../../../class/product';
import { Serial } from '../../../class/serial';

@Injectable()
export class AsInfoService {

  /**
   * @deprecated
   * @description possession 오브젝트의 수정여부
   * @memberof AsInfoService
   */
  public isDirty = false;

  /**
   * @private
   * @type {Possession}
   * @memberof AsInfoService
   */
  private __possession: Possession;

  /**
   *
   * @description 기존 구매내역 할당 시, 즉시 확인하고자 하는 AS접수내역의 id. 진입 시 스크롤하기 위한 변수
   * @type {number}
   * @memberof AsInfoService
   */
  public initialAsCaseId: number;

  /**
   * @description AS접수내역과 교환내역을 쌍으로 표현하기 위한 임시 배열. {'as_case', 'exchange_log'} 오브젝트로 구성됨.
   * @memberof AsInfoService
   */
  public exchangeLogAsCaseIndexPair = [];

  /**
   * @description 제품구매정보. 최초생성 시, 교환내역(ExchangeLog[])/최초구매자(Customer)/제품(Product)/시리얼(Serial) 오브젝트가 초기화됨.
   * @memberof AsInfoService
   */
  set possession(v: Possession) {
    this.__possession = Object.assign(new Possession(), v);
    console.log(this.__possession);
    if (!this.__possession.hasOwnProperty('__exchange_log')) {
      this.firstCustomer = new Customer();
      this.exchangeLog[0].serial = new Serial();
      this.exchangeLog[0].product = new Product();
    }
    this.tieExchangeLogWithAsCase();
  }

/**
 * @type {Possession}
 * @memberof AsInfoService
 */
get possession(): Possession {
    return this.__possession;
  }

  /**
   * @description 제품최초구매자 정보, possession.exchange_log의 0번째 인덱스
   * @type {(Customer | null)}
   * @memberof AsInfoService
   */
  get firstCustomer(): Customer | null {
    if (this.__possession &&
      this.__possession['exchange_log'] &&
      this.__possession['exchange_log'][0].customer) {
      return this.__possession['exchange_log'][0].customer;
    } else {
      return null;
    }
  }

  /**
   * @description 제품최초구매자 정보, possession.exchange_log의 0번째 인덱스
   * @memberof AsInfoService
   */
  set firstCustomer(v: Customer) {
    if (this.__possession.id) {
      throw new Error('CANNOT_MODIFY_FIRST_CUSTOMER');
    } else {
      if (!this.__possession['exchange_log']) {
        this.__possession.exchange_log = [new ExchangeLog()];
      }
      this.__possession.exchange_log[0] = Object.assign(new ExchangeLog(), {
        customer: v
      });
    }
  }

  /**
   * @readonly
   * @memberof AsInfoService
   */
  get asCase() {
    if (this.firstCustomer) {
      return this.__possession.as_case;
    } else {
      throw new Error('INVALID_POSSESSION');
    }
  }

  /**
   * @description AS접수내역 추가
   * @param {ASCase} v 빈 AS접수내역 오브젝트
   * @memberof AsInfoService
   */
  addAsCase(v: ASCase) {
    if (!this.__possession.as_case) {
      this.__possession.as_case = [];
    }
    this.__possession.as_case.push(v);
  }

  /**
   * @description AS접수내역 수정
   * @param {number} index possession.as_case 인덱스
   * @param {ASCase} v 수정할 AS접수내역 오브젝트
   * @memberof AsInfoService
   */
  updateAsCase(index: number, v: ASCase) {
    this.__possession.as_case[index] = v;
  }

  /**
   * @description 신규 생성된 AS접수내역 삭제, 기존 접수내역은 삭제되지 않음
   * @param {number} index 삭제할 AS접수내역 인덱스
   * @memberof AsInfoService
   */
  removeAsCase(index: number) {
    if (this.__possession.as_case[index].id) {
      alert('기입력된 정보는 삭제할 수 없습니다. 처리상태를 종료로 처리하여 주시기 바랍니다.');
    } else {
      this.__possession.as_case.splice(index, 1);
      alert('삭제되었습니다.');
    }
  }

  /**
   * @readonly
   * @type {ExchangeLog[]}
   * @memberof AsInfoService
   */
  get exchangeLog(): ExchangeLog[] {
    return this.__possession.exchange_log;
  }

  /**
   * @description 교환내역 추가
   * @param {ExchangeLog} v 빈 교환내역 오브젝트
   * @memberof AsInfoService
   */
  addExchangeLog(v: ExchangeLog) {
    if (!this.__possession.exchange_log) {
      this.__possession.exchange_log = [];
    }
    v.customer = this.firstCustomer;
    this.__possession.exchange_log.push(v);
  }

  /**
   * @description 교환내역 수정
   * @param {number} index possession.exchange_log의 인덱스
   * @param {ExchangeLog} v 수정된 교환내역 오브젝트
   * @memberof AsInfoService
   */
  updateExchangeLog(index: number, v: ExchangeLog) {
    this.__possession.exchange_log[index] = v;
  }

  /**
   *
   * @description 교환내역 삭제
   * @param {number} index possession.exchange_log의 인덱스
   * @memberof AsInfoService
   */
  removeExchangeLog(index: number) {
    if (this.__possession.exchange_log[index]['id']) {
      alert('기입력된 정보는 삭제할 수 없습니다.');
    } else {
      this.__possession.exchange_log.splice(index, 1);
      alert('삭제되었습니다.');
    }
  }

  /**
   * @description AS케이스와 교환내역을 쌍으로 표현하기위한 오브젝트 배열을 생성
   * @memberof AsInfoService
   */
  tieExchangeLogWithAsCase() {
    if (this.__possession.exchange_log && this.__possession.as_case) {
      this.exchangeLogAsCaseIndexPair = [];
      for (let i = 0; i < this.__possession.exchange_log.length; i++) {
        let kloop = 0;
        if (i > 0) {
          searchAsCase:
          for (let k = kloop; k < this.__possession.as_case.length; k++) {
            if (this.__possession.exchange_log[i].as_case_id === this.__possession.as_case[k].id) {
              this.exchangeLogAsCaseIndexPair.push({ 'as_case': k, 'exchange_log': i });
              kloop = k;
              break searchAsCase;
            }
          }
          if (kloop === this.__possession.as_case.length) {
            this.exchangeLogAsCaseIndexPair.push({ 'as_case': null, 'exchange_log': i });
          }
        }
      }
      let point = 0;
      for (let i = 0; i < this.__possession.as_case.length; i++) {
        indexPair:
        for (let k = point; k < this.exchangeLogAsCaseIndexPair.length; k++) {
          if (this.exchangeLogAsCaseIndexPair[k]['as_case'] === i) {
            point = k;
            break indexPair;
          }
        }
        if (point === this.exchangeLogAsCaseIndexPair.length) {
          this.exchangeLogAsCaseIndexPair.splice(point, 0, { 'as_case': i, 'exchange_log': null });
        }
      }
    }
  }
  getLogTarget() {

  }
}
