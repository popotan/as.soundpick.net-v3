import { Component, OnInit, Input, AfterViewInit, EventEmitter, Output, OnChanges, ElementRef } from '@angular/core';
import { AsService } from '../../as.service';
import { ASProcess } from '../../../../class/asprocess';
import { ASCase } from '../../../../class/ascase';
import { clone } from '../../../../../environments/environment.prod';
import { AsInfoService } from '../as-info.service';
import { FormAssistantService } from '../../../../global/service/form-assistant.service';

@Component({
  selector: 'app-as-case',
  templateUrl: './as-case.component.html',
  styleUrls: ['./as-case.component.css']
})
export class AsCaseComponent implements OnInit, AfterViewInit {

  /**
   * @description AS내역 수정여부
   * @memberof AsCaseComponent
   */
  isEditing = false;

  /**
   * @deprecated
   * @memberof AsCaseComponent
   */
  isExpand = false;

  /**
   * @description 전파받은 AS내역 원형
   * @type {ASCase}
   * @memberof AsCaseComponent
   */
  __oAsCase: ASCase;

  /**
   * @description AS내역 변경사항
   * @type {ASCase}
   * @memberof AsCaseComponent
   */
  __uAsCase: ASCase;

  /**
   * @description 전파받은 AS내역 저장하고 복사
   * @memberof AsCaseComponent
   */
  @Input('asCase') set asCase(v: ASCase) {
    this.__oAsCase = Object.assign(new ASCase(), v);
    this.__uAsCase = clone(this.__oAsCase);
    if (!this.__uAsCase.process) {
      this.__uAsCase.process = new ASProcess();
    }
  };

  /**
   * @description AS내역 변경사항을 전달할 EventEmitter
   * @type {EventEmitter<any>}
   * @memberof AsCaseComponent
   */
  @Output('onAsCaseUpdated') onAsCaseUpdated: EventEmitter<any> = new EventEmitter();

  /**
   * @description AS내역 삭제를 위한 EventEmitter
   * @type {EventEmitter<any>}
   * @memberof AsCaseComponent
   */
  @Output('onAsCaseRemoved') onAsCaseRemoved: EventEmitter<any> = new EventEmitter();
  constructor(
    private asInfo: AsInfoService,
    private el: ElementRef,
    private asService: AsService,
    private formAssistantService: FormAssistantService
  ) { }

  ngOnInit() {
  }

  /**
   * @description AS내역을 통해 진입 시, 해당 AS내역으로 스크롤
   * @memberof AsCaseComponent
   */
  ngAfterViewInit() {
    if (this.__oAsCase.id === this.asInfo.initialAsCaseId) {
      this.isExpand = true;
      window.scrollTo(0, this.el.nativeElement.offsetTop - 100);
    }
  }

  /**
   * @description 변경감지
   * @returns
   * @memberof AsCaseComponent
   */
  isDiff() {
    return JSON.stringify(this.__oAsCase) !== JSON.stringify(this.__uAsCase);
  }

  /**
   * @description 구매내역 내 구매자와 AS내역 내 접수자 일치화
   * @memberof AsCaseComponent
   */
  setRegisterToPossessionCustomer() {
    if (this.asInfo.firstCustomer === this.__uAsCase.customer) {
      this.__uAsCase.customer = null;
    } else {
      this.__uAsCase.customer = this.asInfo.firstCustomer;
    }
  }

  /**
   * @description AS접수자 업데이트
   * @param {*} $event
   * @memberof AsCaseComponent
   */
  onASRegisterSelected($event) {
    this.__uAsCase.customer = $event;
  }

  /**
   * @description AS내역 업데이트
   * @memberof AsCaseComponent
   */
  update() {
    this.onAsCaseUpdated.emit(this.__uAsCase);
    this.isEditing = false;
  }
}
