import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsCaseComponent } from './as-case.component';

describe('AsCaseComponent', () => {
  let component: AsCaseComponent;
  let fixture: ComponentFixture<AsCaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsCaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsCaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
