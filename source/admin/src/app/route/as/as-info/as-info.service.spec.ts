import { TestBed, inject } from '@angular/core/testing';

import { AsInfoService } from './as-info.service';

describe('AsInfoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AsInfoService]
    });
  });

  it('should ...', inject([AsInfoService], (service: AsInfoService) => {
    expect(service).toBeTruthy();
  }));
});
