import { Component, OnInit, ViewChild, ElementRef, ViewContainerRef, TemplateRef, EmbeddedViewRef, ComponentRef, AfterViewInit, ViewChildren, QueryList, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators, FormArray } from '@angular/forms';
import { Subject, BehaviorSubject } from 'rxjs';

import { AsService } from '../as.service';
import { FormAssistantService } from '../../../global/service/form-assistant.service';
import { FileUploadService } from '../../../core/file-upload.service';

import { Possession } from '../../../class/possession';
import { ExchangeLog } from '../../../class/exchange-log';
import { ASCase } from '../../../class/ascase';
import { AsInfoService } from './as-info.service';
import { AsCaseComponent } from './as-case/as-case.component';
import { ExchangeProductComponent } from './exchange-product/exchange-product.component';
import { SessionService } from '../../../core/session.service';
import { staticURL } from '../../../../environments/environment.prod';

@Component({
  selector: 'app-as-info',
  templateUrl: './as-info.component.html',
  styleUrls: ['./as-info.component.css'],
  providers: [FileUploadService, AsInfoService]
})
export class AsInfoComponent implements OnInit, AfterViewInit {
  /**
   * @memberof AsInfoComponent
   */
  staticURL = staticURL;

  /**
   * @description component 제어를 위한 ref
   * @type {*}
   * @memberof AsInfoComponent
   */
  ref: any;

  /**
   * @description comment 입력 폼
   * @type {TemplateRef<any>}
   * @memberof AsInfoComponent
   */
  @ViewChild('askComment') askComment: TemplateRef<any>;

  /**
   * @description comment textarea element
   * @type {ElementRef}
   * @memberof AsInfoComponent
   */
  @ViewChild('submitComment') submitComment: ElementRef;

  /**
   * @description 각 AsCaseComponent 순환을 위한 QueryList
   * @type {QueryList<AsCaseComponent>}
   * @memberof AsInfoComponent
   */
  @ViewChildren(AsCaseComponent) asCase: QueryList<AsCaseComponent>;

  /**
   * @description 각 AsCaseComponent 순환을 위한 QueryList
   * @type {QueryList<ExchangeProductComponent>}
   * @memberof AsInfoComponent
   */
  @ViewChildren(ExchangeProductComponent) exchangeProduct: QueryList<ExchangeProductComponent>;

  /**
   * @description comment 입력 폼을 제어하기 위한 view ref
   * @private
   * @type {EmbeddedViewRef<any>}
   * @memberof AsInfoComponent
   */
  private createdView: EmbeddedViewRef<any> = null;

  /**
   * @description AsInfoService에 구매정보 할당
   * @memberof AsInfoComponent
   */
  set establishedInfo(v) {
    this.asInfo.possession = v;
  }

  /**
   * @description AS내역을 통해 구매정보 접근 시, AsInfoService에 진입점 할당
   * @memberof AsInfoComponent
   */
  set initialAsCaseId(v) {
    this.asInfo.initialAsCaseId = v;
  }

  /**
   *Creates an instance of AsInfoComponent.
   * @param {AsService} asService
   * @param {AsInfoService} asInfo
   * @param {FormAssistantService} formAssistantService
   * @param {ViewContainerRef} vcr
   * @param {ElementRef} el
   * @param {FileUploadService} fileUploadService
   * @param {SessionService} sessionService
   * @memberof AsInfoComponent
   */
  constructor(
    private asService: AsService,
    private asInfo: AsInfoService,
    private formAssistantService: FormAssistantService,
    private vcr: ViewContainerRef,
    private el: ElementRef,
    private fileUploadService: FileUploadService,
    private sessionService: SessionService
  ) { }

  /**
   * @memberof AsInfoComponent
   */
  ngOnInit() {
    this.asService.getAsSymptomList();
  }

  /**
   * @memberof AsInfoComponent
   */
  ngAfterViewInit() {
    if (!this.asInfo.initialAsCaseId) {
      window.scrollTo(0, this.el.nativeElement.offsetTop - 100);
    }
  }

  /**
   *
   * @description AS내역 추가, as_case배열 내 AsCase 객체 중, is_expired가 false인 객체가 있다면 생성되지 않음
   * @param {*} $event
   * @returns
   * @memberof AsInfoComponent
   */
  addASCase($event) {
    if (this.asInfo.possession['as_case']) {
      for (const as_case of this.asInfo.possession.as_case) {
        if (as_case.is_expired === '처리중') {
          alert('현재 진행중인 AS가 있습니다.\r\n진행중인 AS를 종료처리 후, 생성이 가능합니다.');
          return;
        }
      }
    }
    $event.target.disabled = true;
    this.asInfo.addAsCase(new ASCase());
    this.asInfo.exchangeLogAsCaseIndexPair.push({ 'as_case': this.asInfo.asCase.length, 'exchange_log': null });
  }

  /**
   * @description AS내역 수정
   * @param {number} idx possession.as_case 내 리스트 중 수정할 객체의 인덱스
   * @param {*} $event
   * @memberof AsInfoComponent
   */
  onAsCaseUpdated(idx: number, $event) {
    this.asInfo.updateAsCase(idx, $event);
    alert('입력상태가 저장되었습니다.');
  }

  /**
   * @description AS내역 삭제. id가 부여되지 않은 AsCase 객체만 삭제가능
   * @param {number} idx possession.as_case 내 리스트 중 삭제할 객체의 인덱스
   * @param {*} $event
   * @memberof AsInfoComponent
   */
  onAsCaseRemoved(idx: number, $event) {
    this.asInfo.removeAsCase(idx);
  }

  /**
   * @description 교환내역 추가. AsCase.id가 있으면 AS내역과 쌍으로 연결된 관계이며, 없으면 기타사유로 교환
   * @param {*} $event
   * @param {number} [asCaseId]
   * @memberof AsInfoComponent
   */
  addExchangeLog($event, asCaseId?: number) {
    $event.target.disabled = true;
    const e = new ExchangeLog();
    console.log('asCaseId', asCaseId);
    if (asCaseId) {
      e.as_case_id = asCaseId;
    }
    this.asInfo.addExchangeLog(e);
    if (asCaseId) {
      for (let i = 0; i < this.asInfo.exchangeLogAsCaseIndexPair.length; i++) {
        if (this.asInfo.asCase[this.asInfo.exchangeLogAsCaseIndexPair[i]['as_case']].id === asCaseId) {
          this.asInfo.exchangeLogAsCaseIndexPair[i]['exchange_log'] = this.asInfo.exchangeLog.length;
          break;
        }
      }
    } else {
      this.asInfo.exchangeLogAsCaseIndexPair.push({ 'as_case': null, 'exchange_log': this.asInfo.exchangeLog.length });
    }
  }

  /**
   * @description 교환내역 수정
   * @param {number} idx possession.exchange_log 내 리스트 중 수정할 객체의 인덱스
   * @param {*} $event
   * @memberof AsInfoComponent
   */
  onExchangeLogUpdated(idx: number, $event) {
    this.asInfo.updateExchangeLog(idx, $event);
    alert('입력상태가 저장되었습니다.');
  }

  /**
   * @description 교환내역 삭제. id가 부여되지 않은 ExchangeLog객체만 삭제 가능
   * @param {number} idx possession.exchange_log 내 리스트 중 삭제할 객체의 인덱스
   * @param {*} $event
   * @memberof AsInfoComponent
   */
  onExchangeLogRemoved(idx: number, $event) {
    this.asInfo.removeExchangeLog(idx);
  }

  /**
   * @description comment 입력창 생성
   * @returns
   * @memberof AsInfoComponent
   */
  drawCommentInputForm() {
    // tslint:disable-next-line:curly
    if (this.createdView) return;
    this.createdView = this.vcr.createEmbeddedView(this.askComment);
  }

  /**
   * @description possession 객체 신규입력/업데이트
   * @memberof AsInfoComponent
   */
  submit() {
    this.asService.addASCase(this.asInfo.possession, this.submitComment.nativeElement.value)
      .debounceTime(3000)
      .subscribe(result => {
        if (result['message'] === 'SUCCESS') {
          this.removeCommentInputForm();
          this.asInfo.possession = Object.assign(new Possession(), result['response']['document']);
          alert('반영되었습니다. 목록에서는 변경된 정보가 반영되지 않으므로 반드시 새로고침 하시기 바랍니다.');
        } else {
          alert('정보등록에 실패하였습니다.');
        }
      });
  }

  /**
   * @description comment 입력창 제거
   * @memberof AsInfoComponent
   */
  removeCommentInputForm() {
    this.createdView.destroy();
    this.createdView = null;
  }

  /**
   * @description 컴포넌트 destroy
   * @memberof AsInfoComponent
   */
  closeInfoView() {
    let canDeactive = true;
    this.asCase.forEach(component => {
      if (component.isDiff()) {
        canDeactive = false;
      }
    });
    this.exchangeProduct.forEach(component => {
      if (component.isDiff()) {
        canDeactive = false;
      }
    });
    if (canDeactive) {
      this.ref.destroy();
    } else {
      alert('저장하지 않은 변경사항이 있습니다.');
    }
  };
}
