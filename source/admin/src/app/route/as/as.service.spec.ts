import { TestBed, inject } from '@angular/core/testing';

import { AsService } from './as.service';

describe('AsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AsService]
    });
  });

  it('should ...', inject([AsService], (service: AsService) => {
    expect(service).toBeTruthy();
  }));
});
