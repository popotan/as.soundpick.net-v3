import { Component, OnInit, ViewChild, TemplateRef, ElementRef, EmbeddedViewRef, ViewContainerRef } from '@angular/core';
import { AdminGroupService } from '../admin-group.service';

@Component({
  selector: 'app-group-info',
  templateUrl: './group-info.component.html',
  styleUrls: ['./group-info.component.css']
})
export class GroupInfoComponent implements OnInit {
  ref: any;
  __dispatchedInfo;
  set establishedInfo(v) {
    this.__dispatchedInfo = v;
  }

  @ViewChild('askComment') askComment: TemplateRef<any>;
  @ViewChild('submitComment') submitComment: ElementRef;
  private createdView: EmbeddedViewRef<any> = null;

  constructor(
    private adminGroupService: AdminGroupService,
    private vcr: ViewContainerRef
  ) { }

  ngOnInit() {
  }

  addGroup() {
    this.adminGroupService.addGroup(this.__dispatchedInfo, this.submitComment.nativeElement.value)
      .debounceTime(3000)
      .subscribe(result => {
        if (result['message'] === 'SUCCESS') {
          this.removeCommentInputForm();
          this.__dispatchedInfo = result['response']['document'];
          alert('반영되었습니다.');
        }
      });
  }
  /**
   * @description comment 입력창 생성
   * @returns
   * @memberof ProductInfoComponent
   */
  drawCommentInputForm() {
    // tslint:disable-next-line:curly
    if (this.createdView) return;
    this.createdView = this.vcr.createEmbeddedView(this.askComment);
    console.log(this.__dispatchedInfo);
  }
  /**
 * @description comment 입력창 제거
 * @memberof ProductInfoComponent
 */
  removeCommentInputForm() {
    this.createdView.destroy();
    this.createdView = null;
  }

  closeInfoView() {
    this.ref.destroy();
  }
}
