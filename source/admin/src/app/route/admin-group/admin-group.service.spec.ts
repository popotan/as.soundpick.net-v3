import { TestBed, inject } from '@angular/core/testing';

import { AdminGroupService } from './admin-group.service';

describe('AdminGroupService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminGroupService]
    });
  });

  it('should ...', inject([AdminGroupService], (service: AdminGroupService) => {
    expect(service).toBeTruthy();
  }));
});
