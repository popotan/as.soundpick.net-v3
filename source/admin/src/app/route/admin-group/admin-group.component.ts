import { Component, OnInit, ViewContainerRef, ViewChild, ComponentFactoryResolver } from '@angular/core';
import { FormAssistantService } from '../../global/service/form-assistant.service';
import { SplitedViewTable } from '../../global/extend/splited-view-table';
import { GroupInfoComponent } from './group-info/group-info.component';
import { AdminGroupService } from './admin-group.service';

@Component({
  selector: 'app-admin-group',
  templateUrl: './admin-group.component.html',
  styleUrls: ['./admin-group.component.css'],
  providers: [AdminGroupService, FormAssistantService],
  entryComponents: [GroupInfoComponent]
})
export class AdminGroupComponent extends SplitedViewTable implements OnInit {
  @ViewChild('groupInfoContainer', { read: ViewContainerRef }) groupInfoContainer;
  searchCondition = {
    is_valid: true,
    from_date: this.formAssistantService.calculateDateTerm(-7, new Date()),
    to_date: new Date(),
    keyword: '',
    page: 1
  };
  searchResultCount = 0;
  createdComponent = null;

  constructor(
    private adminGroupService: AdminGroupService,
    private cfr: ComponentFactoryResolver,
    private formAssistantService: FormAssistantService
  ) {
    super(
      [['그룹번호', '그룹명'], ['그룹코드', '생성일자', '보기']],
      [80, 200, 150, 200, 100]
    );
  }

  ngOnInit() {
  }

  setReceptionPeriod(dateLength: number) {
    this.searchCondition.from_date = this.formAssistantService.calculateDateTerm(dateLength, new Date());
    this.searchCondition.to_date = this.formAssistantService.calculateDateTerm(0, new Date());
  }

  search() {
    this.adminGroupService.getGroupList(this.searchCondition)
      .subscribe(result => {
        if (result['message'] === 'SUCCESS') {
          this.adminGroupService.groupList = result['response']['document'];
          this.searchResultCount = result['response']['meta']['total_count'];

          const arr = [[], []];
          this.adminGroupService.groupList.forEach(group => {
            const staticArea = [
              { text: group.id },
              { text: group.name }
            ];
            const scrollableArea = [
              { text: group.admin_group_code },
              { text: this.formAssistantService.convertPyDatetime(group.reg_date, 'string') },
              { text: '정보수정/보기', actionType: 'button', methodName: 'showGroupInfo' }
            ];
            arr[0].push(staticArea);
            arr[1].push(scrollableArea);
          });
          this.SVTRows = arr;
        }
      });
  }

  showGroupInfo(index = -1) {
    this.groupInfoContainer.clear();
    const factory = this.cfr.resolveComponentFactory(GroupInfoComponent);
    this.createdComponent = this.groupInfoContainer.createComponent(factory);
    this.createdComponent.instance.ref = this.createdComponent;
    if (index > -1) {
      this.createdComponent.instance.establishedInfo = this.adminGroupService.groupList[index];
    } else {
      this.createdComponent.instance.establishedInfo = {};
    }
  }
}
