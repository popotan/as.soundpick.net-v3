import { Injectable } from '@angular/core';

import { Http, URLSearchParams, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { apiURL } from '../../../environments/environment.prod';
import { FormAssistantService } from '../../global/service/form-assistant.service';

@Injectable()
export class AdminGroupService {
  groupList = [];
  constructor(
    private $http: Http,
    private formAssistantService: FormAssistantService
  ) { }

  getGroupList(params) {
    const cOptions = Object.assign({}, params);
    cOptions['from_date'] = this.formAssistantService.convertToStringTypeOfDate(params['from_date']);
    cOptions['to_date'] = this.formAssistantService.convertToStringTypeOfDate(params['to_date']);
    return this.$http.get(apiURL + '/user/group', { params: cOptions })
      .map((res: Response) => res.json());
  }
  addGroup(params, comment) {
    return this.$http.post(apiURL + '/user/group', {group : params, comment : comment})
      .map((res: Response) => res.json());
  }
}
