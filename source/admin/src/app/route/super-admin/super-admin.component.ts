import { Component, OnInit, ViewContainerRef, ViewChild, ComponentFactoryResolver } from '@angular/core';
import { FormAssistantService } from '../../global/service/form-assistant.service';
import { SplitedViewTable } from '../../global/extend/splited-view-table';
import { SuperAdminService } from './super-admin.service';
import { UserInfoComponent } from './user-info/user-info.component';

@Component({
  selector: 'app-super-admin',
  templateUrl: './super-admin.component.html',
  styleUrls: ['./super-admin.component.css'],
  providers: [SuperAdminService, FormAssistantService],
  entryComponents: [UserInfoComponent]
})
export class SuperAdminComponent extends SplitedViewTable implements OnInit {

  /**
   *
   *
   * @memberof SuperAdminComponent
   */
  @ViewChild('userInfoContainer', { read: ViewContainerRef }) userInfoContainer;

  /**
   * @description 검색조건
   * @memberof AsComponent
   */
  searchCondition = {
    is_valid: true,
    from_date: this.formAssistantService.calculateDateTerm(-7, new Date()),
    to_date: new Date(),
    keyword: '',
    page: 1
  };

  /**
   * @memberof SuperAdminComponent
   */
  searchResultCount = 0;
  createdComponent = null;

  constructor(
    private superAdminService: SuperAdminService,
    private formAssistantService: FormAssistantService,
    private cfr: ComponentFactoryResolver
  ) {
    super(
      [['회원번호', '이름', '이메일(ID)'], ['관리그룹', '가입일', '마지막접속', '인증여부', '보기']],
      [80, 100, 300, 200, 150, 150, 80, 100]
    );
  }

  ngOnInit() {
  }

  setReceptionPeriod(dateLength: number) {
    this.searchCondition.from_date = this.formAssistantService.calculateDateTerm(dateLength, new Date());
    this.searchCondition.to_date = this.formAssistantService.calculateDateTerm(0, new Date());
  }

  search() {
    this.superAdminService.getAdminUserList(this.searchCondition)
      .subscribe(result => {
        if (result['message'] === 'SUCCESS') {
          this.superAdminService.adminUserList = result['response']['document'];
          this.searchResultCount = result['response']['meta']['total_count'];
          const arr = [[], []];
          this.superAdminService.adminUserList.forEach(user => {
            const staticArea = [
              { text: user.user_uid },
              { text: user.name },
              { text: user.email }];
            const scrollableArea = [
              { text: user.admin_group.name },
              { text: user.reg_date },
              { text: user.last_logged_in },
              { text: user.is_valid ? '인증됨' : '인증안됨' },
              { text: '정보수정/보기', actionType: 'button', methodName: 'showUserInfo' }
            ];
            arr[0].push(staticArea);
            arr[1].push(scrollableArea);
          });
          this.SVTRows = arr;
        }
      });
  }
  showUserInfo(index = -1) {
    this.userInfoContainer.clear();
    const factory = this.cfr.resolveComponentFactory(UserInfoComponent);
    this.createdComponent = this.userInfoContainer.createComponent(factory);
    this.createdComponent.instance.ref = this.createdComponent;
    if (index > -1) {
      this.createdComponent.instance.establishedInfo = this.superAdminService.adminUserList[index];
    } else {
      this.createdComponent.instance.establishedInfo = {};
    }
  }
}
