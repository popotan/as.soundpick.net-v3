import { Injectable } from '@angular/core';

import { Http, URLSearchParams, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { apiURL } from '../../../environments/environment.prod';
import { FormAssistantService } from '../../global/service/form-assistant.service';

@Injectable()
export class SuperAdminService {

  /**
   * @description 관리자 검색결과
   * @memberof SuperAdminService
   */
  adminUserList = [];

  constructor(
    private $http: Http,
    private formAssistantService: FormAssistantService
  ) { }

  getAdminUserList(params): Observable<object> {
    const cOptions = Object.assign({}, params);
    cOptions['from_date'] = this.formAssistantService.convertToStringTypeOfDate(params['from_date']);
    cOptions['to_date'] = this.formAssistantService.convertToStringTypeOfDate(params['to_date']);
    return this.$http.get(apiURL + '/user', { params: cOptions })
      .map((res: Response) => res.json());
  }
  setValidation(user_uid, is_valid): Observable<object> {
    return this.$http.post(apiURL + '/user/valid/' + user_uid, { is_valid: is_valid })
      .map((res: Response) => res.json());
  }
}
