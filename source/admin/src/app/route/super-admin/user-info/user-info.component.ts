import { Component, OnInit } from '@angular/core';
import { SuperAdminService } from '../super-admin.service';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.css']
})
export class UserInfoComponent implements OnInit {

  __dispatchedInfo;
  ref: any;
  set establishedInfo(v) {
    this.__dispatchedInfo = v;
  }
  constructor(
    private superAdminService: SuperAdminService
  ) { }

  ngOnInit() {
  }
  setValidation(is_valid: boolean) {
    this.superAdminService.setValidation(this.__dispatchedInfo.user_uid, is_valid)
      .subscribe(result => {
        if (result['message'] === 'SUCCESS') {
          this.__dispatchedInfo.is_valid = is_valid;
          alert('권한이 변경되었습니다.');
        } else {
          this.__dispatchedInfo.is_valid = !is_valid;
          alert('권한 변경에 실패하였습니다.');
        }
      });
  }
  closeInfoView() {
    this.ref.destroy();
  }
}
