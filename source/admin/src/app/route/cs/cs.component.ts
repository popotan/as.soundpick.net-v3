import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators, FormArray } from '@angular/forms';

import { CsService } from './cs.service';
import { FormAssistantService } from '../../global/service/form-assistant.service';

@Component({
  selector: 'app-cs',
  templateUrl: './cs.component.html',
  styleUrls: ['./cs.component.css'],
  providers: [CsService, FormAssistantService]
})
export class CsComponent implements OnInit {
  searchCondition = new FormGroup({
    treatmentState: new FormControl('', []),
    receptionPeriod: new FormGroup({
      from: new FormControl(this.formAssistantService.calculateDateTerm(-7), []),
      to: new FormControl(this.formAssistantService.calculateDateTerm(0), [])
    }),
    symptom: new FormArray([]),
    keyword: new FormControl('', [])
  });
  constructor(
    private csService: CsService,
    private formAssistantService: FormAssistantService
  ) { }

  ngOnInit() {
  }
  setReceptionPeriod(dateLength) {
    this.searchCondition.patchValue({
      receptionPeriod: {
        from: this.formAssistantService.calculateDateTerm(dateLength),
        to: this.formAssistantService.calculateDateTerm(0)
      }
    });
  }
}
