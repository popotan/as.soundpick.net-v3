import { TestBed, inject } from '@angular/core/testing';

import { CsService } from './cs.service';

describe('CsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CsService]
    });
  });

  it('should ...', inject([CsService], (service: CsService) => {
    expect(service).toBeTruthy();
  }));
});
