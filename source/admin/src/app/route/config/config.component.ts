import { Component, OnInit, ViewChild } from '@angular/core';
import { ConfigService } from './config.service';

@Component({
  selector: 'app-config',
  templateUrl: './config.component.html',
  styleUrls: ['./config.component.css'],
  providers: [ConfigService]
})
export class ConfigComponent implements OnInit {

  /**
   * @description AS관련 값 설정
   * @memberof ConfigComponent
   */
  after_service = {
    symptom_list: [],
    new_symptom: ''
  };

  /**
   * @description 문자발송 관련 값 설정
   * @memberof ConfigComponent
   */
  sms = {
    variableList: [],
    template: {
      on_invoice_added: '',
      on_as_case_added: ''
    }
  };

  /**
   * @deprecated
   * @memberof ConfigComponent
   */
  invoiceColumns = '';
  
  /**
   * @description 증상 select element
   * @memberof ConfigComponent
   */
  @ViewChild('symptomList') symptomList;

  /**
   *Creates an instance of ConfigComponent.
   * @param {ConfigService} configService
   * @memberof ConfigComponent
   */
  constructor(
    private configService: ConfigService
  ) { }

  ngOnInit() {
    this.getAsSymptomList();
    this.getAsCaseSmsTemplate();
    // this.getInvoiceColumns();
  }

  /**
   * @description 증상 값 불러오기
   * @memberof ConfigComponent
   */
  getAsSymptomList() {
    this.configService.getAsSymptomList()
      .subscribe(result => {
        if (result['message'] === 'SUCCESS') {
          this.after_service.symptom_list = result['response']['document'];
        }
      });
  }

  /**
   * @description 증상 값 추가
   * @memberof ConfigComponent
   */
  addSymptom() {
    if (this.after_service.new_symptom) {
      this.configService.postAsSymptom(this.after_service.new_symptom)
        .subscribe(result => {
          if (result['message'] === 'SUCCESS') {
            this.after_service.symptom_list.push({ 'value': this.after_service.new_symptom });
            this.after_service.new_symptom = '';
          }
        });
    }
  }

  /**
   * @description 증상 값 제거
   * @memberof ConfigComponent
   */
  removeSymptom() {
    if (confirm('이 항목을 삭제하더라도 기존에 접수된 증상은 변경/삭제되지 않습니다. 삭제하시겠습니까?')) {
      this.configService.removeAsSymptom(this.symptomList.nativeElement.value)
        .subscribe(result => {
          if (result['message'] === 'SUCCESS') {
            for (let i = 0; i < this.after_service.symptom_list.length; i++) {
              if (this.after_service.symptom_list[i].value === this.symptomList.nativeElement.value) {
                this.after_service.symptom_list.splice(i, 1);
                break;
              }
            }
          }
        });
    }
  }

  /**
   * @description 문자발송 관련 값 불러오기
   * @memberof ConfigComponent
   */
  getAsCaseSmsTemplate() {
    this.configService.getAsCaseSmsTemplate()
      .subscribe(result => {
        if (result['message'] === 'SUCCESS') {
          this.sms.variableList = result['response']['document']['variable_list'];
          this.sms.template = result['response']['document']['template'];
        }
      });
  }

  /**
   * @description 문자발송 관련 값 수정
   * @param {*} attribute
   * @returns
   * @memberof ConfigComponent
   */
  updateAsCaseSmsTemplate(attribute) {
    for (const keyword of this.sms.variableList[attribute]['allow_variable']) {
      if (this.sms.template[attribute].indexOf('{' + keyword + '}') === -1) {
        if (confirm('포함되지 않은 필수 키워드가 있습니다.\r\n입력한 내용을 저장하시겠습니까?')) {
          break;
        } else {
          return;
        }
      }
    }
    const obj = { 'sms': { 'attribute': attribute, value: this.sms.template[attribute] } };
    this.configService.updateAsCaseSmsTemplate(obj)
      .subscribe(result => {
        if (result['message'] === 'SUCCESS') {
          alert('반영되었습니다.');
        }
      });
  }
  // getInvoiceColumns() {
  //   this.configService.getInvoiceColumns()
  //     .subscribe(result => {
  //       if (result['message'] === 'SUCCESS') {
  //         this.invoiceColumns = result['response']['document'];
  //       }
  //     });
  // }
  // putInvoiceColumns() {
  //   let temp_str = this.invoiceColumns.replace(/\s/g, '');
  //   let temp = temp_str.split(',');
  //   for (let i = 0; i < temp.length; i++) {
  //     if (this.allowInvoiceColumns.indexOf(temp[i]) === -1) {
  //       alert('허용하지 않는 문자열이 있습니다.');
  //       return false;
  //     }
  //   }
  //   this.configService.putInvoiceColumns(temp.join(','))
  //     .subscribe(result => {
  //       if (result['message'] === 'SUCCESS') {
  //         this.invoiceColumns = result['response']['document'];
  //         alert('수정되었습니다.');
  //       }
  //     });
  // }
}
