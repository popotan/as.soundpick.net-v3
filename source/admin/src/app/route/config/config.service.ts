import { Injectable } from '@angular/core';

import { Http, URLSearchParams, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { apiURL } from '../../../environments/environment.prod';

@Injectable()
export class ConfigService {

  constructor(
    private $http: Http
  ) { }
  getAsSymptomList() {
    return this.$http.get(apiURL + '/as/symptom')
      .map((res: Response) => res.json());
  }
  postAsSymptom(value) {
    return this.$http.post(apiURL + '/as/symptom', { value: value })
      .map((res: Response) => res.json());
  }
  removeAsSymptom(value) {
    return this.$http.delete(apiURL + '/as/symptom', { params: { value: value } })
      .map((res: Response) => res.json());
  }
  getInvoiceColumns() {
    return this.$http.get(apiURL + '/config/invoice/columns')
      .map((res: Response) => res.json());
  }
  putInvoiceColumns(columns) {
    return this.$http.put(apiURL + '/config/invoice/columns', { columns: columns })
      .map((res: Response) => res.json());
  }
  getAsCaseSmsTemplate() {
    return this.$http.get(apiURL + '/config/sms/template')
      .map((res: Response) => res.json());
  }
  updateAsCaseSmsTemplate(info) {
    return this.$http.post(apiURL + '/config/sms/template', info)
      .map((res: Response) => res.json());
  }
}
