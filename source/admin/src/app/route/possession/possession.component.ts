import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators, FormArray } from '@angular/forms';

import { PossessionService } from './possession.service';
import { FormAssistantService } from '../../global/service/form-assistant.service';
import { SplitedViewTable } from '../../global/extend/splited-view-table';
import { Possession } from '../../class/possession';

@Component({
  selector: 'app-possession',
  templateUrl: './possession.component.html',
  styleUrls: ['./possession.component.css'],
  providers: [PossessionService, FormAssistantService]
})
export class PossessionComponent extends SplitedViewTable implements OnInit {
  /**
   *
   *
   * @memberof PossessionComponent
   */
  searchCondition = {
    is_expired: false,
    from_date: this.formAssistantService.calculateDateTerm(-7, new Date()),
    to_date: new Date(),
    brand: '',
    product: [],
    keyword: '',
    page: 1
  };

  /**
   *
   *
   * @memberof PossessionComponent
   */
  searchResultCount = 0;

  /**
   *Creates an instance of PossessionComponent.
   * @param {PossessionService} possessionService
   * @param {FormAssistantService} formAssistantService
   * @memberof PossessionComponent
   */
  constructor(
    private possessionService: PossessionService,
    private formAssistantService: FormAssistantService
  ) {
    super(
      [['브랜드', '제품명', '제품번호', '이름'], ['파기여부', '등록시각', '구매일', '보증종료일']],
      [200, 150, 200, 100, 80, 150, 200, 200]
    );
  }

  /**
   *
   *
   * @memberof PossessionComponent
   */
  ngOnInit() {
    this.possessionService.getBrand()
      .subscribe(result => {
        if (result['message'] === 'SUCCESS') {
          this.possessionService.brandList = result['response']['document'];
        }
      });
  }

  /**
   *
   *
   * @param {*} dateLength
   * @memberof PossessionComponent
   */
  setReceptionPeriod(dateLength) {
    this.searchCondition.from_date = this.formAssistantService.calculateDateTerm(dateLength);
    this.searchCondition.to_date = this.formAssistantService.calculateDateTerm(0);
  }

  /**
   *
   *
   * @memberof PossessionComponent
   */
  search() {
    this.possessionService.getPossessionInfo(this.searchCondition)
      .subscribe(result => {
        if (result['message'] === 'SUCCESS') {
          this.possessionService.possessionList = result['response']['document'].map(v => Object.assign(new Possession(), v));
          const arr = [[], []];
          this.searchResultCount = result['response']['meta']['total_count'];
          this.possessionService.possessionList.forEach(res => {
            const staticArea = [
              { text: res.exchange_log[0].product.brand.name },
              { text: res.exchange_log[0].product.name },
              { text: res.exchange_log[0].serial ? res.exchange_log[0].serial.serial : '-' },
              { text: res.exchange_log[0].customer.name }
            ];
            const scrollableArea = [
              { text: res.is_expired ? '파기' : '유효' },
              { text: this.formAssistantService.convertPyDatetime(res.reg_date, 'string') },
              { text: this.formAssistantService.convertPyDatetime(res.purchase_date, 'string') },
              { text: this.formAssistantService.convertPyDatetime(res.warranty_expired_date, 'string') },
              // { text: '정보수정/보기', actionType: 'button', methodName: 'showInfo' }
            ];
            arr[0].push(staticArea);
            arr[1].push(scrollableArea);
          });
          this.SVTRows = arr;
        }
      });
  }
}
