import { Injectable } from '@angular/core';

import { Http, URLSearchParams, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { apiURL } from '../../../environments/environment.prod';
import { FormAssistantService } from '../../global/service/form-assistant.service';

@Injectable()
export class PossessionService {
  brandList = [];
  productList = [];
  possessionList = [];
  constructor(
    private $http: Http,
    private formAssistantService: FormAssistantService
  ) { }
  getBrand() {
    return this.$http.get(apiURL + '/as/target/brand')
      .map((res: Response) => res.json());
  }
  getProduct(brandId) {
    return this.$http.get(apiURL + '/as/target/brand/' + brandId + '/product')
      .map((res: Response) => res.json()).subscribe(result => {
        if (result['message'] === 'SUCCESS') {
          this.productList = result['response']['document'];
        }
      });
  }
  getPossessionInfo(params) {
    const cOptions = Object.assign({}, params);
    cOptions['from_date'] = this.formAssistantService.convertToStringTypeOfDate(params['from_date']);
    cOptions['to_date'] = this.formAssistantService.convertToStringTypeOfDate(params['to_date']);
    return this.$http.get(apiURL + '/possession', { params: cOptions })
      .map((res: Response) => res.json());
  }
}
