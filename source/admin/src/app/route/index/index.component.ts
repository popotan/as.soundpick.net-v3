import { Component, OnInit } from '@angular/core';
import { IndexService } from './index.service';
import { FormAssistantService } from '../../global/service/form-assistant.service';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [IndexService, FormAssistantService]
})
export class IndexComponent implements OnInit {
  /**
   *
   *
   * @memberof IndexComponent
   */
  counts = {
    newPossession: 0,
    newAsCase: 0,
    byAsCaseDivision: {},
    unexpiredAsCase: 0,
    expiredAsCase: 0,
    expiredPossessionOf: 0
  };

  /**
   *
   *
   * @memberof IndexComponent
   */
  asCaseDivisions = [];

  /**
   *Creates an instance of IndexComponent.
   * @param {IndexService} indexService
   * @param {FormAssistantService} formAssistantService
   * @memberof IndexComponent
   */
  constructor(
    private indexService: IndexService,
    private formAssistantService: FormAssistantService
  ) { }

  ngOnInit() {
    this.getNewPossessionCount();
    this.getToExpiredPossession();
    this.getAsCaseCount();
  }

  /**
   *
   *
   * @memberof IndexComponent
   */
  getNewPossessionCount() {
    const options = {
      from_date: this.formAssistantService.convertToStringTypeOfDate(this.formAssistantService.calculateDateTerm(-7, new Date())),
      to_date: this.formAssistantService.convertToStringTypeOfDate(new Date())
    };
    this.indexService.getNewPossessionList(options)
      .subscribe(result => {
        if (result['message'] === 'SUCCESS') {
          this.counts.newPossession = result['response']['meta']['total_count'];
        }
      });
  }

  /**
   *
   *
   * @memberof IndexComponent
   */
  getToExpiredPossession() {
    this.indexService.getExpiredPossessionOf(-7)
      .subscribe(result => {
        if (result['message'] === 'SUCCESS') {
          this.counts.expiredPossessionOf = result['response']['meta']['total_count'];
        }
      });
  }

  /**
   *
   *
   * @memberof IndexComponent
   */
  getAsCaseCount() {
    const options = {
      from_date: this.formAssistantService.convertToStringTypeOfDate(this.formAssistantService.calculateDateTerm(-7, new Date())),
      to_date: this.formAssistantService.convertToStringTypeOfDate(new Date())
    };
    this.indexService.getNewAsCaseList(options)
      .subscribe(result => {
        if (result['message'] === 'SUCCESS') {
          this.counts.newAsCase = result['response']['meta']['total_count'];
          this.counts.expiredAsCase = 0;
          this.counts.unexpiredAsCase = 0;
          result['response']['document'].forEach(ascase => {
            if (ascase['is_expired']) {
              this.counts.expiredAsCase++;
            } else {
              this.counts.unexpiredAsCase++;
            }
            if (!this.counts.byAsCaseDivision.hasOwnProperty(ascase['division'])) {
              this.asCaseDivisions.push(ascase['division']);
              this.counts.byAsCaseDivision[ascase['division']] = 0;
            }
            this.counts.byAsCaseDivision[ascase['division']]++;
          });
        }
      });
  }
}
