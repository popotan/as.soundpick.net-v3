import { Injectable } from '@angular/core';

import { Http, URLSearchParams, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { apiURL } from '../../../environments/environment.prod';

@Injectable()
export class LoginService {
  constructor(
    private $http: Http
  ) { }

  doLogin(userInfo) {
    return this.$http.post(apiURL + '/user/login', userInfo)
      .map((res: Response) => res.json());
  }

  doPasswordChange(userInfo) {
    return this.$http.post(apiURL + '/user/password', userInfo)
      .map((res: Response) => res.json());
  }

  doInfoChange(userInfo) {
    return this.$http.post(apiURL + '/user/info', userInfo)
      .map((res: Response) => res.json());
  }
}
