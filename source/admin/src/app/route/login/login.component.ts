import { Component, OnInit } from '@angular/core';
import { LoginService } from './login.service';
import { SessionService } from '../../core/session.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [LoginService]
})
export class LoginComponent implements OnInit {
  /**
   * @description 로그인 요청정보
   * @memberof LoginComponent
   */
  __info = {
    email: '',
    password: ''
  };

  /**
   *Creates an instance of LoginComponent.
   * @param {LoginService} loginService
   * @param {SessionService} sessionService
   * @param {Router} router
   * @memberof LoginComponent
   */
  constructor(
    private loginService: LoginService,
    private sessionService: SessionService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  /**
   * @description 로그인 요청 후, 성공시 세션 요청
   * @memberof LoginComponent
   */
  doLogin() {
    this.loginService.doLogin(this.__info)
      .subscribe(result => {
        if (result['message'] === 'SUCCESS') {
          this.sessionService.getSession().then(result => {
            if (result['is_logged_in']) {
              this.router.navigateByUrl('/');
            } else {
              alert('로그인에는 성공했으나, 세션을 불러오는데 실패하였습니다.');
            }
          });
        } else if (result['message'] === 'USER_IS_NOT_EXIST') {
          alert('존재하지 않는 계정입니다.');
        } else if (result['message'] === 'USER_IS_NOT_VALID') {
          alert('접근권한이 없는 계정입니다.');
        } else if (result['message'] === 'GROUP_IS_NOT_VALID') {
          alert('사용승인되지 않은 사용자그룹입니다.');
        } else if (result['message'] === 'GROUP_IS_NOT_EXIST') {
          alert('존재하지 않는 그룹에 속해있는 그룹입니다.');
        } else if (result['message'] === 'PASSOWRD_IS_NOT_MATCHED') {
          alert('비밀번호가 일치하지 않습니다.\r\n다시 시도해 주세요.');
        } else {
          alert('알 수 없는 오류이거나 현재 서버가 구동중이 아닙니다.');
        }
      }, error => { console.log(error); });
  }
}
