import { Injectable } from '@angular/core';

import { Http, URLSearchParams, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { apiURL } from '../../../environments/environment.prod';

@Injectable()
export class JoinService {

  constructor(
    private $http: Http
  ) { }

  postUserInfo(userInfo) {
    return this.$http.post(apiURL + '/user/join', userInfo)
      .map((res: Response) => res.json());
  }

}
