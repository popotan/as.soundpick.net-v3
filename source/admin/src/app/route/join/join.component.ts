import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { JoinService } from './join.service';
import { SessionService } from '../../core/session.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-join',
  templateUrl: './join.component.html',
  styleUrls: ['./join.component.css'],
  providers: [JoinService]
})
export class JoinComponent implements OnInit {
  /**
   * @description 가입정보
   * @memberof JoinComponent
   */
  __info = {
    email: '',
    password: '',
    groupCode: '',
    name: ''
  };

  /**
   * @description 패스워드 확인용 input element
   * @type {ElementRef}
   * @memberof JoinComponent
   */
  @ViewChild('passwordCheck') passwordCheck: ElementRef;

  /**
   *Creates an instance of JoinComponent.
   * @param {JoinService} joinService
   * @param {SessionService} sessionService
   * @param {Router} router
   * @memberof JoinComponent
   */
  constructor(
    private joinService: JoinService,
    private sessionService: SessionService,
    private router: Router
  ) { }
  ngOnInit() { }

  /**
   * @description 가입요청
   * @memberof JoinComponent
   */
  doJoin() {
    if (this.__info.password === this.passwordCheck.nativeElement.value) {
      this.joinService.postUserInfo(this.__info)
        .subscribe(result => {
          if (result['message'] === 'SUCCESS') {
            alert('가입신청이 완료되었습니다.\r\n관리자 승인 후, 사용이 가능합니다.');
          } else if (result['message'] === 'EMAIL_ADDRESS_WAS_ALREADY_EXIST') {
            alert('이미 존재하는 이메일입니다. 다른 이메일을 사용해주세요.');
          }
        });
    } else {
      alert('비밀번호가 일치하지 않습니다.\r\n다시 한 번 확인해주세요.');
    }
  }
}
