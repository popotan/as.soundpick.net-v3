import { Component, OnInit, ViewChild, ViewContainerRef, ComponentFactoryResolver, EventEmitter, OnChanges, Output } from '@angular/core';
import { BrandInfoComponent } from './brand-info/brand-info.component';
import { GoodsService } from '../goods.service';
import { FormAssistantService } from '../../../global/service/form-assistant.service';
import { SplitedViewTable } from '../../../global/extend/splited-view-table';
import { Brand } from '../../../class/brand';

@Component({
  selector: 'app-brand',
  templateUrl: './brand.component.html',
  styleUrls: ['./brand.component.css'],
  providers: [FormAssistantService],
  entryComponents: [BrandInfoComponent]
})
export class BrandComponent extends SplitedViewTable implements OnInit {
  /**
   * @description 브랜드 정보를 담기위한 view container
   * @memberof BrandComponent
   */
  @ViewChild('brandInfoContainer', { read: ViewContainerRef }) brandInfoContainer;

  /**
   * @description 선택된 브랜드 정보 전파를 위한 EventEmitter
   * @type {EventEmitter<any>}
   * @memberof BrandComponent
   */
  @Output('onBrandSelected') onBrandSelected: EventEmitter<any> = new EventEmitter();

  /**
   * @description info component 삽입을 위한 property
   * @memberof BrandComponent
   */
  createdComponent = null;

  /**
   * @description 검색조건
   * @memberof BrandComponent
   */
  searchCondition = {
    from_date: this.formAssistantService.calculateDateTerm(-180),
    to_date: this.formAssistantService.calculateDateTerm(0),
    keyword: '',
    page: 1
  };

  /**
   * @memberof BrandComponent
   */
  searchResultCount = 0;

  /**
   *Creates an instance of BrandComponent.
   * @param {GoodsService} goodsService
   * @param {FormAssistantService} formAssistantService
   * @param {ViewContainerRef} vcr
   * @param {ComponentFactoryResolver} cfr
   * @memberof BrandComponent
   */
  constructor(
    private goodsService: GoodsService,
    private formAssistantService: FormAssistantService,
    private vcr: ViewContainerRef,
    private cfr: ComponentFactoryResolver
  ) {
    super(
      [['브랜드명', '등록일시'], ['보기']],
      [200, 150, 100]
    );
  }
  ngOnInit() {
  }

  /**
   * @param {number} dateLength
   * @memberof BrandComponent
   */
  setReceptionPeriod(dateLength: number) {
    this.searchCondition.from_date = this.formAssistantService.calculateDateTerm(dateLength);
    this.searchCondition.to_date = this.formAssistantService.calculateDateTerm(0);
  }

  /**
   * @description 브랜드 검색
   * @memberof BrandComponent
   */
  searchBrandInfo() {
    this.goodsService.getBrandInfo(this.searchCondition)
      .subscribe(result => {
        if (result['message'] === 'SUCCESS') {
          this.onBrandSelected.emit([]);
          this.goodsService.brandList = result['response']['document'].map(v => Object.assign(new Brand(), v));
          this.searchResultCount = result['response']['meta']['total_count'];

          const arr = [[], []];
          this.goodsService.brandList.forEach(brand => {
            const staticArea = [
              { text: brand.name },
              { text: this.formAssistantService.convertPyDatetime(brand.reg_date, 'string') }
            ];
            const scrollableArea = [
              { text: '정보수정/보기', actionType: 'button', methodName: 'showBrandInfo' }
            ];
            arr[0].push(staticArea);
            arr[1].push(scrollableArea);
          });
          this.SVTRows = arr;
        }
      });
  }

  /**
   * @description 선택된 브랜드를 상위 컴포넌트로 전파
   * @param {*} $event
   * @memberof BrandComponent
   */
  onVTCBrandSelected($event) {
    const selected_brand = [];
    this.SVTSelectedIndexes.forEach(index => {
      selected_brand.push(this.goodsService.brandList[index]);
    });
    this.onBrandSelected.emit(selected_brand);
  }

  /**
   * @param {number} [index=-1]
   * @memberof BrandComponent
   */
  showBrandInfo(index: number = -1) {
    this.brandInfoContainer.clear();
    const factory = this.cfr.resolveComponentFactory(BrandInfoComponent);
    this.createdComponent = this.brandInfoContainer.createComponent(factory);
    this.createdComponent.instance.ref = this.createdComponent;
    if (index > -1) {
      this.createdComponent.instance.establishedInfo = this.goodsService.brandList[index];
    } else {
      this.createdComponent.instance.establishedInfo = new Brand();
    }
  }
}
