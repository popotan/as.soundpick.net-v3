import { Component, OnInit, ViewChild, ElementRef, ViewContainerRef, AfterViewInit, EmbeddedViewRef, TemplateRef } from '@angular/core';
import { FormAssistantService } from '../../../../global/service/form-assistant.service';
import { Subject } from 'rxjs';
import { Brand } from '../../../../class/brand';
import { FileUploadService } from '../../../../core/file-upload.service';
import { apiURL, staticURL } from '../../../../../environments/environment.prod';
import { GoodsService } from '../../goods.service';

@Component({
  selector: 'app-brand-info',
  templateUrl: './brand-info.component.html',
  styleUrls: ['./brand-info.component.css'],
  providers: [FileUploadService]
})
export class BrandInfoComponent implements OnInit, AfterViewInit {
  /**
   * @description static file path
   * @memberof BrandInfoComponent
   */
  staticURL = staticURL;

  /**
   * @description 컴포넌트 클래스 스스로 컴포넌트를 destroy 할 수 있도록 부모 컴포넌트로부터 상속
   * @type {*}
   * @memberof BrandInfoComponent
   */
  ref: any;

  /**
   * @memberof BrandInfoComponent
   */
  __viewmode = 1;

  /**
   * @description comment 입력 폼
   * @type {TemplateRef<any>}
   * @memberof BrandInfoComponent
   */
  @ViewChild('askComment') askComment: TemplateRef<any>;

  /**
   * @description comment textarea element
   * @type {ElementRef}
   * @memberof BrandInfoComponent
   */
  @ViewChild('submitComment') submitComment: ElementRef;

  /**
   * @description comment 입력 폼을 제어하기 위한 view ref
   * @private
   * @type {EmbeddedViewRef<any>}
   * @memberof BrandInfoComponent
   */
  private createdView: EmbeddedViewRef<any> = null;

  /**
   *
   *
   * @memberof BrandInfoComponent
   */
  set establishedInfo(v) {
    const subj = new Subject<Brand>();
    this.__info = subj;
    this.__info.subscribe(brand => {
      this.__dispatchedInfo = brand;
    });
    this.__info.next(v);
  }

  /**
   *
   *
   * @memberof BrandInfoComponent
   */
  __info;

  /**
   *
   *
   * @memberof BrandInfoComponent
   */
  __dispatchedInfo;

  /**
   * @description 브랜드 이미지 업로드용 file input element
   * @type {ElementRef}
   * @memberof BrandInfoComponent
   */
  @ViewChild('brandImageFileInput') brandImageFileInput: ElementRef;

  /**
   *Creates an instance of BrandInfoComponent.
   * @param {GoodsService} goodsService
   * @param {FormAssistantService} formAssistantService
   * @param {FileUploadService} fileUploadService
   * @param {ViewContainerRef} vcr
   * @param {ElementRef} el
   * @memberof BrandInfoComponent
   */
  constructor(
    private goodsService: GoodsService,
    private formAssistantService: FormAssistantService,
    private fileUploadService: FileUploadService,
    private vcr: ViewContainerRef,
    private el: ElementRef
  ) { }
  ngOnInit() {
  }

  /**
   * @description 브랜드 이미지 업로드
   * @param {*} $event
   * @memberof BrandInfoComponent
   */
  uploadImage($event) {
    this.fileUploadService.uploadFile(apiURL + '/brand/image', $event.target.files)
      .then(result => {
        // tslint:disable-next-line:forin
        for (const i in result) {
          if (this.__dispatchedInfo.image === undefined) {
            this.__dispatchedInfo.image = [];
          }
          this.__dispatchedInfo.image.push({
            image_filename: result[i].filename,
            image_size_width: result[i].size.width,
            image_size_height: result[i].size.height
          });
        }
      });
  }

  /**
   * @description comment 입력창 생성
   * @returns
   * @memberof BrandInfoComponent
   */
  drawCommentInputForm() {
    // tslint:disable-next-line:curly
    if (this.createdView) return;
    this.createdView = this.vcr.createEmbeddedView(this.askComment);
  }

  /**
   * @description 정보수정 수행
   * @memberof BrandInfoComponent
   */
  submit() {
    this.goodsService.addBrand(this.__dispatchedInfo, this.submitComment.nativeElement.value)
      .debounceTime(3000)
      .subscribe(result => {
        this.removeCommentInputForm();
        this.__dispatchedInfo = Object.assign(new Brand(), result['response']['document']);
        this.__info.next(this.__dispatchedInfo);
      });
  }

  /**
   * @description comment 입력창 제거
   * @memberof BrandInfoComponent
   */
  removeCommentInputForm() {
    this.createdView.destroy();
    this.createdView = null;
  }

  /**
   * @description 작업창으로 스크롤
   * @memberof BrandInfoComponent
   */
  ngAfterViewInit() {
    window.scrollTo(0, this.el.nativeElement.offsetTop);
  }

  /**
   * @description 컴포넌트 destroy
   * @memberof BrandInfoComponent
   */
  closeInfoView() {
    this.ref.destroy();
  }
}
