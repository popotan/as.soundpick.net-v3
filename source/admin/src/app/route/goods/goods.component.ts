import { Component, OnInit } from '@angular/core';
import { GoodsService } from './goods.service';
import { FormAssistantService } from '../../global/service/form-assistant.service';

@Component({
  selector: 'app-goods',
  templateUrl: './goods.component.html',
  styleUrls: ['./goods.component.css'],
  providers: [GoodsService, FormAssistantService]
})
export class GoodsComponent implements OnInit {
  /**
   * @description brandComponent에서 전파된 선택된 brand 정보
   * @type {any[]}
   * @memberof GoodsComponent
   */
  selectedBrands: any[] = [];
  constructor() { }

  ngOnInit() {
  }

}
