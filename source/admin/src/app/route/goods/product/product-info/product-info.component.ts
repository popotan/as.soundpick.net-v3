import { Component, OnInit, AfterViewInit, ViewChild, ElementRef, ViewContainerRef, TemplateRef, EmbeddedViewRef } from '@angular/core';
import { FormAssistantService } from '../../../../global/service/form-assistant.service';
import { Subject } from 'rxjs';
import { Product } from '../../../../class/product';
import { FileUploadService } from '../../../../core/file-upload.service';
import { apiURL, staticURL } from '../../../../../environments/environment.prod';
import { GoodsService } from '../../goods.service';

@Component({
  selector: 'app-product-info',
  templateUrl: './product-info.component.html',
  styleUrls: ['./product-info.component.css'],
  providers: [FileUploadService]
})
export class ProductInfoComponent implements OnInit, AfterViewInit {
  /**
   * @description static file path
   * @memberof ProductInfoComponent
   */
  staticURL = staticURL;

  /**
   * @description 컴포넌트 클래스 스스로 컴포넌트를 destroy 할 수 있도록 부모 컴포넌트로부터 상속
   * @type {*}
   * @memberof ProductInfoComponent
   */
  ref: any;

  /**
   * @memberof ProductInfoComponent
   */
  __viewmode = 1;

  /**
   * @description comment 입력 폼
   * @type {TemplateRef<any>}
   * @memberof ProductInfoComponent
   */
  @ViewChild('askComment') askComment: TemplateRef<any>;

  /**
   * @description comment textarea element
   * @type {ElementRef}
   * @memberof ProductInfoComponent
   */
  @ViewChild('submitComment') submitComment: ElementRef;

  /**
   * @description comment 입력 폼을 제어하기 위한 view ref
   * @private
   * @type {EmbeddedViewRef<any>}
   * @memberof ProductInfoComponent
   */
  private createdView: EmbeddedViewRef<any> = null;

  /**
   *
   *
   * @memberof ProductInfoComponent
   */
  set establishedInfo(v) {
    const subj = new Subject<Product>();
    this.__info = subj;
    this.__info.subscribe(product => {
      this.__dispatchedInfo = product;
    });
    this.__info.next(v);
  }

  /**
   *
   *
   * @memberof ProductInfoComponent
   */
  __info;

  /**
   *
   *
   * @memberof ProductInfoComponent
   */
  __dispatchedInfo;

  /**
   *
   *
   * @memberof ProductInfoComponent
   */
  searchedBrandList = [];

  /**
   * @description 제품 이미지 업로드용 file input element
   * @type {ElementRef}
   * @memberof ProductInfoComponent
   */
  @ViewChild('productImageFileInput') productImageFileInput: ElementRef;

  /**
   *Creates an instance of ProductInfoComponent.
   * @param {GoodsService} goodsService
   * @param {FormAssistantService} formAssistantService
   * @param {FileUploadService} fileUploadService
   * @param {ViewContainerRef} vcr
   * @param {ElementRef} el
   * @memberof ProductInfoComponent
   */
  constructor(
    private goodsService: GoodsService,
    private formAssistantService: FormAssistantService,
    private fileUploadService: FileUploadService,
    private vcr: ViewContainerRef,
    private el: ElementRef
  ) { }
  ngOnInit() {
  }

  /**
   * @description 전파받은 브랜드 정보 입력
   * @param {*} $event
   * @memberof ProductInfoComponent
   */
  onBrandSelected($event) {
    this.__dispatchedInfo.brand = $event;
    this.__info.next(this.__dispatchedInfo);
  }

  /**
   * @description 제품 이미지 업로드
   * @param {*} $event
   * @memberof ProductInfoComponent
   */
  uploadImage($event) {
    this.fileUploadService.uploadFile(apiURL + '/product/image', $event.target.files)
      .then(result => {
        // tslint:disable-next-line:forin
        for (const i in result) {
          if (this.__dispatchedInfo.image === undefined) {
            this.__dispatchedInfo.image = [];
          }
          this.__dispatchedInfo.image.push({
            image_filename: result[i].filename,
            image_size_width: result[i].size.width,
            image_size_height: result[i].size.height
          });
        }
      });
  }

  /**
   * @description AS가능 여부 설정
   * @memberof ProductInfoComponent
   */
  setIsASTarget() {
    this.__dispatchedInfo.option['is_as_target'] = !this.__dispatchedInfo.option['is_as_target'];
    this.__info.next(this.__dispatchedInfo);
  }

  /**
   * @description comment 입력창 생성
   * @returns
   * @memberof ProductInfoComponent
   */
  drawCommentInputForm() {
    // tslint:disable-next-line:curly
    if (this.createdView) return;
    this.createdView = this.vcr.createEmbeddedView(this.askComment);
    console.log(this.__dispatchedInfo);
  }

  /**
   * @description 정보수정 수행
   * @memberof ProductInfoComponent
   */
  submit() {
    this.goodsService.addProduct(this.__dispatchedInfo, this.submitComment.nativeElement.value)
      .debounceTime(3000)
      .subscribe(result => {
        this.removeCommentInputForm();
        this.__dispatchedInfo = Object.assign(new Product(), result['response']['document']);
        this.__info.next(this.__dispatchedInfo);
      });
  }

  /**
   * @description comment 입력창 제거
   * @memberof ProductInfoComponent
   */
  removeCommentInputForm() {
    this.createdView.destroy();
    this.createdView = null;
  }

  /**
   * @description 작업창으로 스크롤
   * @memberof ProductInfoComponent
   */
  ngAfterViewInit() {
    window.scrollTo(0, this.el.nativeElement.offsetTop);
  }

  /**
   * @description 컴포넌트 destroy
   * @memberof ProductInfoComponent
   */
  closeInfoView() {
    this.ref.destroy();
  };
}
