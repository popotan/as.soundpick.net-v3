import { Component, OnInit, ViewChild, ViewContainerRef, ComponentFactoryResolver, Input } from '@angular/core';
import { ProductInfoComponent } from './product-info/product-info.component';
import { GoodsService } from '../goods.service';
import { FormAssistantService } from '../../../global/service/form-assistant.service';
import { SplitedViewTable } from '../../../global/extend/splited-view-table';
import { Product } from '../../../class/product';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
  providers: [FormAssistantService],
  entryComponents: [ProductInfoComponent]
})
export class ProductComponent extends SplitedViewTable implements OnInit {
  /**
   *
   *
   * @memberof ProductComponent
   */
  @ViewChild('productInfoContainer', { read: ViewContainerRef }) productInfoContainer;

  /**
   *
   *
   * @memberof ProductComponent
   */
  @Input('selectedBrands') selectedBrands;

  /**
   *
   *
   * @memberof ProductComponent
   */
  createdComponent = null;

  /**
   *
   *
   * @memberof ProductComponent
   */
  searchCondition = {
    brand: [],
    searchOnSelectedBrandList: false,
    from_date: this.formAssistantService.calculateDateTerm(-180),
    to_date: this.formAssistantService.calculateDateTerm(0),
    keyword: '',
    page: 1
  };

  /**
   *
   *
   * @memberof ProductComponent
   */
  searchResultCount = 0;

  /**
   *Creates an instance of ProductComponent.
   * @param {GoodsService} goodsService
   * @param {FormAssistantService} formAssistantService
   * @param {ViewContainerRef} vcr
   * @param {ComponentFactoryResolver} cfr
   * @memberof ProductComponent
   */
  constructor(
    private goodsService: GoodsService,
    private formAssistantService: FormAssistantService,
    private vcr: ViewContainerRef,
    private cfr: ComponentFactoryResolver
  ) {
    super(
      [['제품명', '등록일시'], ['제품종류', '정품관리여부', '보기']],
      [200, 150, 150, 120, 100]
    );
  }

  ngOnInit() {
  }

  /**
   *
   *
   * @param {*} dateLength
   * @memberof ProductComponent
   */
  setReceptionPeriod(dateLength) {
    this.searchCondition.from_date = this.formAssistantService.calculateDateTerm(dateLength);
    this.searchCondition.to_date = this.formAssistantService.calculateDateTerm(0);
  }

  /**
   *
   *
   * @memberof ProductComponent
   */
  searchProductInfo() {
    if (this.searchCondition.searchOnSelectedBrandList && this.selectedBrands.length > 0) {
      this.searchCondition.brand = [];
      this.selectedBrands.forEach(brand => {
        this.searchCondition.brand.push(brand.id);
      });
    } else {
      this.searchCondition.brand = [];
    }
    this.goodsService.getProductInfo(this.searchCondition)
      .subscribe(result => {
        if (result['message'] === 'SUCCESS') {
          this.goodsService.productList = result['response']['document'].map(v => Object.assign(new Product(), v));
          this.searchResultCount = result['response']['meta']['total_count'];

          const arr = [[], []];
          this.goodsService.productList.forEach(product => {
            const staticArea = [
              { text: product.name },
              { text: this.formAssistantService.convertPyDatetime(product.reg_date, 'string') }
            ];
            const scrollableArea = [
              { text: product.lineup },
              { text: product.validation ? '관리' : '-' },
              { text: '정보수정/보기', actionType: 'button', methodName: 'showProductInfo' }
            ];
            arr[0].push(staticArea);
            arr[1].push(scrollableArea);
          });
          this.SVTRows = arr;
        }
      });
  }

  /**
   *
   *
   * @param {*} [index=-1]
   * @memberof ProductComponent
   */
  showProductInfo(index = -1) {
    this.productInfoContainer.clear();
    const factory = this.cfr.resolveComponentFactory(ProductInfoComponent);
    this.createdComponent = this.productInfoContainer.createComponent(factory);
    this.createdComponent.instance.ref = this.createdComponent;
    if (index > -1) {
      this.createdComponent.instance.establishedInfo = this.goodsService.productList[index];
    } else {
      this.createdComponent.instance.establishedInfo = new Product();
    }
  }
}
