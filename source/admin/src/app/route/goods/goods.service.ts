import { Injectable } from '@angular/core';

import { Http, URLSearchParams, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { apiURL } from '../../../environments/environment.prod';
import { FormAssistantService } from '../../global/service/form-assistant.service';

@Injectable()
export class GoodsService {
  brandList = [];
  productList = [];
  constructor(
    private $http: Http,
    private formAssistantService: FormAssistantService
  ) { }

  getBrandInfo(params) {
    const cOptions = Object.assign({}, params);
    cOptions['from_date'] = this.formAssistantService.convertToStringTypeOfDate(params['from_date']);
    cOptions['to_date'] = this.formAssistantService.convertToStringTypeOfDate(params['to_date']);
    return this.$http.get(apiURL + '/brand', { params: cOptions })
      .map((res: Response) => res.json());
  }
  getProductInfo(params) {
    const cOptions = Object.assign({}, params);
    cOptions['from_date'] = this.formAssistantService.convertToStringTypeOfDate(params['from_date']);
    cOptions['to_date'] = this.formAssistantService.convertToStringTypeOfDate(params['to_date']);
    return this.$http.get(apiURL + '/product', { params: cOptions })
      .map((res: Response) => res.json());
  }
  addBrand(params, comment) {
    return this.$http.post(apiURL + '/brand', { brand: this.formAssistantService.formalize(params), comment: comment })
      .map((res: Response) => res.json());
  }
  addProduct(params, comment) {
    return this.$http.post(apiURL + '/product', { product: this.formAssistantService.formalize(params), comment: comment })
      .map((res: Response) => res.json());
  }
}
