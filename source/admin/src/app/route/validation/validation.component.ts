import { Component, OnInit, ViewChild, ElementRef, ComponentFactoryResolver, ViewContainerRef } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators, FormArray } from '@angular/forms';

import { ValidationService } from './validation.service';
import { FormAssistantService } from '../../global/service/form-assistant.service';
import { SplitedViewTable } from '../../global/extend/splited-view-table';
import { UploadSerialComponent } from './upload-serial/upload-serial.component';
import { SerialInfoComponent } from './serial-info/serial-info.component';
import { Serial } from '../../class/serial';

@Component({
  selector: 'app-validation',
  templateUrl: './validation.component.html',
  styleUrls: ['./validation.component.css'],
  providers: [ValidationService, FormAssistantService],
  entryComponents: [UploadSerialComponent, SerialInfoComponent]
})
export class ValidationComponent extends SplitedViewTable implements OnInit {
  /**
   * @description UploadSerialComenent 생성을 위한 property
   * @memberof ValidationComponent
   */
  createdComponent = null;

  /**
   * @memberof ValidationComponent
   */
  @ViewChild('uploadSerialAndSerialInfoContainer', { read: ViewContainerRef }) uploadSerialAndSerialInfoContainer;

  /**
   * @description 검색조건
   * @memberof ValidationComponent
   */
  searchCondition = {
    from_date: this.formAssistantService.calculateDateTerm(-180),
    to_date: this.formAssistantService.calculateDateTerm(0),
    brand: 0,
    product: [],
    keyword: '',
    page: 1
  };

  /**
   *
   * @memberof ValidationComponent
   */
  searchResultCount = 0;

  /**
   *Creates an instance of ValidationComponent.
   * @param {ValidationService} validationService
   * @param {FormAssistantService} formAssistantService
   * @param {ViewContainerRef} vcr
   * @param {ComponentFactoryResolver} cfr
   * @memberof ValidationComponent
   */
  constructor(
    private validationService: ValidationService,
    private formAssistantService: FormAssistantService,
    private vcr: ViewContainerRef,
    private cfr: ComponentFactoryResolver
  ) {
    super(
      [['브랜드', '제품명', '제품번호'], ['파기여부', '등록시각', '조회/변경']],
      [200, 200, 200, 150, 250, 100]
    );
  }

  /**
   * @memberof ValidationComponent
   */
  ngOnInit() {
    this.validationService.getValidationTargetBrand();
  }

  /**
   * @param {number} dateLength
   * @memberof ValidationComponent
   */
  setReceptionPeriod(dateLength: number) {
    this.searchCondition.from_date = this.formAssistantService.calculateDateTerm(dateLength);
    this.searchCondition.to_date = this.formAssistantService.calculateDateTerm(0);
  }

  /**
   * @memberof ValidationComponent
   */
  getSerialList() {
    this.validationService.getSerialList(this.searchCondition)
      .subscribe(result => {
        if (result['message'] === 'SUCCESS') {
          this.validationService.serialList = result['response']['document'].map(v => Object.assign(new Serial(), v));
          this.searchResultCount = result['response']['meta']['total_count'];

          const arr = [[], []];
          this.validationService.serialList.forEach(serial => {
            const staticArea = [
              { text: serial.product.brand.name },
              { text: serial.product.name },
              { text: serial.serial }
            ];
            const scrollableArea = [
              { text: serial.is_expired ? '파기됨' : '유효' },
              { text: this.formAssistantService.convertPyDatetime(serial.reg_date, 'string') },
              { text: '정보수정/보기', actionType: 'button', methodName: 'showSerialInfo' }
            ];
            arr[0].push(staticArea);
            arr[1].push(scrollableArea);
          });
          this.SVTRows = arr;
        }
      });
  }

  /**
   * @memberof ValidationComponent
   */
  showUploadSerialForm() {
    this.uploadSerialAndSerialInfoContainer.clear();
    const factory = this.cfr.resolveComponentFactory(UploadSerialComponent);
    this.createdComponent = this.uploadSerialAndSerialInfoContainer.createComponent(factory);
    this.createdComponent.instance.ref = this.createdComponent;
  }

  /**
   * @param {number} [index=-1]
   * @memberof ValidationComponent
   */
  showSerialInfo(index: number = -1) {
    this.uploadSerialAndSerialInfoContainer.clear();
    const factory = this.cfr.resolveComponentFactory(SerialInfoComponent);
    this.createdComponent = this.uploadSerialAndSerialInfoContainer.createComponent(factory);
    this.createdComponent.instance.ref = this.createdComponent;
    if (index > -1) {
      this.createdComponent.instance.establishedInfo = Object.assign(new Serial(), this.validationService.serialList[index]);
    }
  }
}
