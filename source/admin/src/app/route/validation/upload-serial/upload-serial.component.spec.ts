import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadSerialComponent } from './upload-serial.component';

describe('UploadSerialComponent', () => {
  let component: UploadSerialComponent;
  let fixture: ComponentFixture<UploadSerialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadSerialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadSerialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
