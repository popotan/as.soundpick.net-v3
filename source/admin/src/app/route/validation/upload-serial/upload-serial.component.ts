import { Component, OnInit, EmbeddedViewRef, AfterViewInit, ElementRef, ViewContainerRef, ViewChild, TemplateRef } from '@angular/core';
import { ValidationService } from '../validation.service';
import { FileUploadService } from '../../../core/file-upload.service';
import { apiURL, staticURL } from '../../../../environments/environment.prod';

@Component({
  selector: 'app-upload-serial',
  templateUrl: './upload-serial.component.html',
  styleUrls: ['./upload-serial.component.css'],
  providers: [ValidationService, FileUploadService]
})
export class UploadSerialComponent implements OnInit, AfterViewInit {
  /**
   * @description 컴포넌트 클래스 스스로 컴포넌트를 destroy 할 수 있도록 부모 컴포넌트로부터 상속
   * @type {*}
   * @memberof UploadSerialComponent
   */
  ref: any;

  /**
   * @description 
   * @memberof UploadSerialComponent
   */
  __viewmode = 1;
  
  /**
   *
   * @description comment 입력 폼을 제어하기 위한 view ref
   * @private
   * @type {EmbeddedViewRef<any>}
   * @memberof UploadSerialComponent
   */
  private createdView: EmbeddedViewRef<any> = null;
  
  /**
   * @description 시리얼파일(.xlst) 업로드용 file input element
   * @type {ElementRef}
   * @memberof UploadSerialComponent
   */
  @ViewChild('serialFileInput') serialFileInput: ElementRef;
  
  /**
   * @description comment 입력 폼
   * @type {TemplateRef<any>}
   * @memberof UploadSerialComponent
   */
  @ViewChild('askComment') askComment: TemplateRef<any>;
  
  /**
   * @description comment textarea element
   * @type {ElementRef}
   * @memberof UploadSerialComponent
   */
  @ViewChild('submitComment') submitComment: ElementRef;
  
  /**
   * @description 입력조건
   * @memberof UploadSerialComponent
   */
  selectedCondition = {
    filename: '',
    sheet: -1,
    column: -1,
    brand: -1,
    product: -1,
    is_expired: false
  };

  /**
   * @description 업로드한 시리얼 파일(.xlst) 정보
   * @memberof UploadSerialComponent
   */
  serialFileInfo = {
    filename: '',
    sheet: [],
    column: []
  };

  /**
   * @description 수행할 작업
   * @memberof UploadSerialComponent
   */
  reservedWorks = [];

  /**
   *Creates an instance of UploadSerialComponent.
   * @param {ValidationService} validationService
   * @param {FileUploadService} fileUploadService
   * @param {ElementRef} el
   * @param {ViewContainerRef} vcr
   * @memberof UploadSerialComponent
   */
  constructor(
    private validationService: ValidationService,
    private fileUploadService: FileUploadService,
    private el: ElementRef,
    private vcr: ViewContainerRef
  ) { }

  ngOnInit() {
    this.validationService.getValidationTargetBrand();
  }

  /**
   * @description 시리얼 파일(.xlst) 업로드
   * @param {*} $event
   * @memberof UploadSerialComponent
   */
  uploadSerialSheetFile($event) {
    this.fileUploadService.uploadFile(apiURL + '/validation/serial/file', $event.target.files)
      .then(result => {
        if (result['message'] === 'SUCCESS') {
          this.serialFileInfo.filename = result['response']['meta']['filename'];
          this.serialFileInfo.sheet = result['response']['document']['sheet'];
          this.serialFileInfo.column = [];
          this.serialFileInfo.sheet.forEach(s => {
            this.serialFileInfo.column.push([]);
          });
        }
      });
  }

  /**
   * @description serialFileInfo에서 컬럼명 획득
   * @param {*} $event
   * @returns
   * @memberof UploadSerialComponent
   */
  getColumnsOfSelectedSheet($event) {
    const sheetIndex = $event.target.value;
    if (this.serialFileInfo.column[sheetIndex].length > 0) {
      return;
    } else {
      const sheetName = this.serialFileInfo.sheet[sheetIndex];
      this.validationService.getColumnsOfSelectedSheet(this.serialFileInfo.filename, sheetName)
        .subscribe(result => {
          if (result['message'] === 'SUCCESS') {
            this.serialFileInfo.column[sheetIndex] = result['response']['document'];
          }
        });
    }
  }

  /**
   * @description 작업 추가
   * @returns
   * @memberof UploadSerialComponent
   */
  addWork() {
    const tObject = {};
    tObject['filename'] = this.serialFileInfo.filename;
    tObject['sheet'] = this.serialFileInfo.sheet[this.selectedCondition.sheet];
    tObject['column'] = this.serialFileInfo.column[this.selectedCondition.sheet][this.selectedCondition.column];
    tObject['columnIndex'] = this.selectedCondition.column;
    tObject['brand'] = this.validationService.brandList[this.selectedCondition.brand];
    tObject['product'] = this.validationService.productList[this.selectedCondition.product];
    tObject['is_expired'] = this.selectedCondition.is_expired;
    for (let i = 0; i < this.reservedWorks.length; i++) {
      if (this.reservedWorks[i].sheet === tObject['sheet'] &&
        this.reservedWorks[i].column === tObject['column']) {
        if (confirm('동일한 시리얼 입력정보가 존재합니다. 그래도 계속하시겠습니까?')) {
          continue;
        } else {
          return false;
        }
      }
    }
    this.reservedWorks.push(tObject);
  }

  /**
   * @description comment 입력창 생성
   * @returns
   * @memberof UploadSerialComponent
   */
  drawCommentInputForm() {
    // tslint:disable-next-line:curly
    if (this.createdView) return;
    this.createdView = this.vcr.createEmbeddedView(this.askComment);
  }

  /**
   * @description comment 입력창 제거
   * @memberof UploadSerialComponent
   */
  removeCommentInputForm() {
    this.createdView.destroy();
    this.createdView = null;
  }

  /**
   * @description 작업 수행
   * @memberof UploadSerialComponent
   */
  submit() {
    this.validationService.addSerialInfo(this.reservedWorks, this.submitComment.nativeElement.value)
      .subscribe(result => {
        if (result['message'] === 'SUCCESS') {
          alert(result['response']['meta']['total_count'] + '개의 제품번호에 대하여 작업을 완료하였습니다.');
          this.closeInfoView();
        }
      });
  }

  /**
   * @description 작업창으로 스크롤
   * @memberof UploadSerialComponent
   */
  ngAfterViewInit() {
    window.scrollTo(0, this.el.nativeElement.offsetTop);
  }

  /**
   * @description 컴포넌트 destroy
   * @memberof UploadSerialComponent
   */
  closeInfoView() {
    this.ref.destroy();
  };
}
