import { Injectable } from '@angular/core';

import { Http, URLSearchParams, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { apiURL, httpOptions } from '../../../environments/environment.prod';
import { FormAssistantService } from '../../global/service/form-assistant.service';

@Injectable()
export class ValidationService {
  /**
   * @description 브랜드 목록
   * @memberof ValidationService
   */
  brandList = [];

  /**
   * @description 시리얼제도 운영중인 제품 목록
   * @memberof ValidationService
   */
  productList = [];

  /**
   * @description 검색결과
   * @memberof ValidationService
   */
  serialList = [];
  constructor(
    private $http: Http,
    private formAssistantService: FormAssistantService
  ) { }
  getValidationTargetBrand() {
    this.$http.get(apiURL + '/validation/target/brand', httpOptions)
      .map((res: Response) => res.json()).subscribe(result => {
        if (result['message'] === 'SUCCESS') {
          this.brandList = result['response']['document'];
        }
      });
  }
  getValidationTargetProduct(brandId: number) {
    this.$http.get(apiURL + '/validation/target/brand/' + brandId + '/product', httpOptions)
      .map((res: Response) => res.json()).subscribe(result => {
        if (result['message'] === 'SUCCESS') {
          this.productList = result['response']['document'];
        }
      });
  }
  getSerialList(params: object) {
    const cParams = Object.assign({}, params);
    cParams['from_date'] = this.formAssistantService.convertToStringTypeOfDate(params['from_date']);
    cParams['to_date'] = this.formAssistantService.convertToStringTypeOfDate(params['to_date']);
    httpOptions['params'] = cParams;
    return this.$http.get(apiURL + '/validation/serial', httpOptions)
      .map((res: Response) => res.json());
  }
  getColumnsOfSelectedSheet(filename: string, sheetName: string) {
    return this.$http.get(apiURL + '/validation/serial/file/' + filename + '/sheet/' + sheetName + '/columns')
      .map((res: Response) => res.json());
  }
  addSerialInfo(reservedWorks: object, comment: string) {
    return this.$http.post(apiURL + '/validation/serial', { works: reservedWorks, comment: comment })
      .map((res: Response) => res.json());
  }
}
