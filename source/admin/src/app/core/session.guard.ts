import { Injectable } from '@angular/core';
import { Router, Route, CanActivate, CanActivateChild, CanLoad, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { SessionService } from './session.service';

@Injectable()
export class SessionGuard implements CanActivate, CanActivateChild, CanLoad {

  constructor(
    private session: SessionService,
    private router: Router
  ) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.getUSerSession();
  }

  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.getUSerSession();
  }

  canLoad(
    route: Route
  ): Observable<boolean> | Promise<boolean> | boolean {
    return this.getUSerSession();
  }

  getUSerSession() {
    return this.session.getSession()
      .then(result => {
        this.session._info = result;
        if (result['is_logged_in']) {
          return true;
        } else {
          this.router.navigate(['/login']);
          return false;
        }
      }, error => {
        if (confirm('세션 정보를 획득하는데 실패하였습니다.\r\n로그인페이지로 이동하시겠습니까?')) {
          this.router.navigate(['/login']);
        }
        return false;
      });
  }
}
