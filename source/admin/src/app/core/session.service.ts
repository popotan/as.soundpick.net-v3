import { Injectable } from '@angular/core';

import { Http, URLSearchParams, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { apiURL, httpOptions } from '../../environments/environment.prod';

@Injectable()
export class SessionService {
  _info;
  constructor(
    private $http: Http
  ) { }

  getSession() {
    return new Promise((resolve, reject) => {
      this.$http.get(apiURL + '/user/session', httpOptions)
        .map((res: Response) => res.json())
        .subscribe(result => {
          this._info = result;
          resolve(result);
        }, error => {
          reject(error);
        }, () => {
          // for-test
          // this._info['is_logged_in'] = true;
          // this._info['state'] = ['SUPER', 'CUSTOM', 'SPCARE', 'ATNDN'];
        }
        );
    });
  }
}
