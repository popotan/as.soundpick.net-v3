export class SplitedViewTable {
    /**
     * @description splited view table 데이터열
     * @type {Array<Array<any>>}
     * @memberof SplitedViewTable
     */
    SVTRows: Array<Array<any>>;

    /**
     * @description 전파된 선택된 checkbox 인덱스
     * @type {Array<number>}
     * @memberof SplitedViewTable
     */
    SVTSelectedIndexes: Array<number>;

    /**
     *Creates an instance of SplitedViewTable.
     * @param {Array<Array<string>>} SVTTitles
     * @param {Array<number>} SVTColumnWidths
     * @memberof SplitedViewTable
     */
    constructor(
        protected SVTTitles: Array<Array<string>>,
        protected SVTColumnWidths: Array<number>
    ) {
        this.SVTRows = [[], []];
        if (!this.checkWellFormedness()) {
            console.log('제목열 갯수와 컬럼 넓이 갯수가 일치하지 않습니다.');
        }
    }

    /**
     * @returns {boolean} wellformedness 통과여부
     * @memberof SplitedViewTable
     */
    checkWellFormedness() {
        if (this.SVTTitles[0].length + this.SVTTitles[1].length === this.SVTColumnWidths.length) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @description splited view table에서 전파받은 이벤트를 자식 클래스에서 실행
     * @param {*} $event
     * @memberof SplitedViewTable
     */
    actionFromSVT($event) {
        if (typeof this[$event.methodName] === 'function') {
            this[$event.methodName]($event.index);
        }
    }
}
