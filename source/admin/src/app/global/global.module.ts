import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { HeaderComponent } from './component/header/header.component';
import { AsideComponent } from './component/aside/aside.component';
import { SplitedViewTableComponent } from './component/splited-view-table/splited-view-table.component';
import { CheckboxGroupComponent } from './component/checkbox-group/checkbox-group.component';
import { PaginationComponent } from './component/pagination/pagination.component';
import { SelectCustomerComponent } from './component/select-customer/select-customer.component';
import { SelectPossessionComponent } from './component/select-possession/select-possession.component';
import { SelectProductComponent } from './component/select-product/select-product.component';
import { InputDateComponent } from './component/input-date/input-date.component';
import { SelectBrandComponent } from './component/select-brand/select-brand.component';
import { LogViewerComponent } from './component/log-viewer/log-viewer.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule
  ],
  declarations: [HeaderComponent, AsideComponent, SplitedViewTableComponent, CheckboxGroupComponent, PaginationComponent, SelectCustomerComponent, SelectPossessionComponent, SelectProductComponent, InputDateComponent, SelectBrandComponent, LogViewerComponent],
  exports : [HeaderComponent, AsideComponent, SplitedViewTableComponent, CheckboxGroupComponent, PaginationComponent, SelectCustomerComponent, SelectPossessionComponent, SelectProductComponent, InputDateComponent, SelectBrandComponent, LogViewerComponent]
})
export class GlobalModule { }
