import { Injectable } from '@angular/core';

import { Http, URLSearchParams, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { apiURL, httpOptions } from '../../../environments/environment.prod';

@Injectable()
export class CustomerService {

  constructor(
    private $http: Http
  ) { }

  getCustomerInfoOfKeyword(keyword) {
    httpOptions['params'] = { keyword: keyword };
    return this.$http.get(apiURL + '/customer/search', httpOptions)
      .map((res: Response) => res.json());
  }
  getExistedPhone(phone) {
    return this.$http.get(apiURL + '/customer/phone', { params: { phone: phone } })
      .map((res: Response) => res.json());
  }
}
