import { Injectable } from '@angular/core';

import { Http, URLSearchParams, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { apiURL, httpOptions } from '../../../environments/environment.prod';

@Injectable()
export class LogService {

  constructor(
    private $http: Http
  ) { }
  getLog(param) {
    return this.$http.get(apiURL + '/log?target=' + param)
      .map((res: Response) => res.json());
  }
}
