import { Injectable } from '@angular/core';

import { Http, URLSearchParams, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { apiURL, httpOptions } from '../../../environments/environment.prod';

@Injectable()
export class PossessionService {

  constructor(
    private $http: Http
  ) { }

  getPossessionInfoByCustomer(customer_id) {
    return this.$http.get(apiURL + '/possession/customer/' + customer_id)
      .map((res: Response) => res.json());
  }
}
