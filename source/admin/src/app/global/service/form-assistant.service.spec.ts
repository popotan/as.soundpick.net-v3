import { TestBed, inject } from '@angular/core/testing';

import { FormAssistantService } from './form-assistant.service';

describe('FormAssistantService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FormAssistantService]
    });
  });

  it('should ...', inject([FormAssistantService], (service: FormAssistantService) => {
    expect(service).toBeTruthy();
  }));
});
