import { TestBed, inject } from '@angular/core/testing';

import { PossessionService } from './possession.service';

describe('PossessionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PossessionService]
    });
  });

  it('should ...', inject([PossessionService], (service: PossessionService) => {
    expect(service).toBeTruthy();
  }));
});
