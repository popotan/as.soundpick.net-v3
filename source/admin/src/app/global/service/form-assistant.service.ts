import { Injectable } from '@angular/core';

@Injectable()
export class FormAssistantService {
  constructor() { }

  /**
   * @description string type으로 선언된 날짜포멧을 javascript Date 객체로 변환.
   * @param {string} str 변환하고자 하는 시간표현 문자열
   * @param {string} [format='yyyymmdd'] 문자열의 형식, y는 년도, m은 월, d는 일
   * @returns {(Date|null)}
   * @memberof FormAssistantService
   */
  convertToDateTypeOfString(str: string, format = 'yyyymmdd') {
    const regex = new RegExp('(y*)(\S*)(m*)(\S*)(d*)');
    const execstr = format.match(regex);
    if (execstr) {
      try {
        let darr = [];
        for (let i = 1; i < execstr.length; i = i + 2) {
          let index = { si: 0, ei: 0 };
          for (let j = 1; j < i; j++) {
            index.si += execstr[j].length;
          }
          for (let k = 1; k < i + 1; k++) {
            index.ei += execstr[k].length;
          }
          darr.push(index);
        }
        const date = new Date(
          parseInt(str.substring(darr[0].si, darr[0].ei)),
          parseInt(str.substring(darr[1].si, darr[1].ei)) - 1,
          parseInt(str.substring(darr[2].si, darr[2].ei)));
        return date;
      } catch (error) {
        return null;
      }
    }
    return null;
  }

  /**
   * @description Date type으로 선언된 날짜를 string 객체로 변환.
   * @param {Date} date 변환하고자 하는 Date객체
   * @param {string} [format='yyyymmdd'] 문자열의 형식, y는 년도, m은 월, d는 일
   * @returns {string}
   * @memberof FormAssistantService
   */
  convertToStringTypeOfDate(date: Date, format: string = 'yyyymmdd') {
    function addZero(targetNumber: number, stringLength: number): string {
      let result = '';
      const stringTypeOfTargetNumber = targetNumber.toString();
      if (stringTypeOfTargetNumber.length > stringLength) {
        for (let i = stringTypeOfTargetNumber.length - 1;
          i > stringTypeOfTargetNumber.length - stringLength;
          i--) {
          result = stringTypeOfTargetNumber[i] + result;
        }
      } else {
        for (let i = 0; i < stringLength - stringTypeOfTargetNumber.length; i++) {
          result += '0';
        }
        result += stringTypeOfTargetNumber;
      }
      return result;
    }

    if (date) {
      let result = '';
      const regex = new RegExp('(y*)(\S*)(m*)(\S*)(d*)');
      const execstr = format.match(regex);
      if (execstr) {
        if (execstr[1]) {
          result += addZero(date.getFullYear(), execstr[1].length);
        }
        if (execstr[2]) {
          result += execstr[2];
        }
        if (execstr[3]) {
          result += addZero(date.getMonth() + 1, execstr[3].length);
        }
        if (execstr[4]) {
          result += execstr[4];
        }
        if (execstr[5]) {
          result += addZero(date.getDate(), execstr[5].length);
        }
        return result;
      } else {
        return '00000000';
      }
    } else {
      return '';
    }
  }

  /**
   * @description 날짜 차이 계산
   * @param {number} [dayLength=0]
   * @param {Date} [from=new Date()]
   * @returns
   * @memberof FormAssistantService
   */
  calculateDateTerm(dayLength: number = 0, from: Date = new Date()) {
    const targetDate = new Date(from.getFullYear(), from.getMonth(), from.getDate() + dayLength);
    return targetDate;
  }

  /**
   * @description python에서 json 포멧으로 출력하는 날짜포멧을 Date 또는 string 타입으로 변환
   * @param {string} pydatetime python의 timestamp
   * @param {string} [type='date'] 'date'는 Date객체, 'string'은 문자열
   * @returns {(Date|string|null)}
   * @memberof FormAssistantService
   */
  convertPyDatetime(pydatetime: string, type = 'date') {
    if (pydatetime) {
      const datetime = new Date(pydatetime);
      if (type === 'date') {
        return datetime;
      } else if (type === 'string') {
        return this.convertToStringTypeOfDate(datetime);
      }
    } else {
      return null;
    }
  }

  /**
   * @description 자체 정의한 클래스를 JSON Format으로 변환
   * @param {*} classifiedParams
   * @returns {object} JSON Format
   * @memberof FormAssistantService
   */
  formalize(classifiedParams) {
    function replaceKey(params, modParams) {
      Object.keys(params).forEach(key => {
        const modKey = (key.startsWith('__')) ? key.replace('__', '') : key;

        if (params[key] && params[key] instanceof Array) {
          modParams[modKey] = [];
          params[key].forEach(element => {
            modParams[modKey].push(replaceKey(element, {}));
          });
        } else if (params[key] &&
          typeof params[key] !== 'string' &&
          Object.keys(params[key]).length > 0) {
          modParams[modKey] = replaceKey(params[key], {});
        } else {
          modParams[modKey] = params[key];
        }
      });
      return modParams;
    }
    const v = replaceKey(classifiedParams, {});
    return v;
  }
}
