import { Injectable } from '@angular/core';

import { Http, URLSearchParams, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { apiURL, httpOptions } from '../../../environments/environment.prod';

@Injectable()
export class ProductService {

  constructor(
    private $http: Http
  ) { }
  getProductInfoOfKeyword(keyword) {
    return this.$http.get(apiURL + '/product/search', { params: { keyword: keyword } })
      .map((res: Response) => res.json());
  }
  getProductInfoOfSerial(serial) {
    return this.$http.get(apiURL + '/product/serial/' + serial)
      .map((res: Response) => res.json());
  }
  getBrandInfoOfKeyword(keyword) {
    return this.$http.get(apiURL + '/brand/search', { params: { keyword: keyword } })
      .map((res: Response) => res.json());
  }
}
