import { Component, OnInit } from '@angular/core';
import { SessionService } from '../../../core/session.service';

@Component({
  selector: 'app-aside',
  templateUrl: './aside.component.html',
  styleUrls: ['./aside.component.css']
})
export class AsideComponent implements OnInit {

  constructor(
    private sessionService: SessionService
  ) { }

  ngOnInit() {
  }

}
