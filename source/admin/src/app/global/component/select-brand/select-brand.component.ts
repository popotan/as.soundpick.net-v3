import { Component, OnInit, EventEmitter, Output, ElementRef, ViewChild, Input } from '@angular/core';
import { Brand } from '../../../class/brand';
import { ProductService } from '../../service/product.service';

@Component({
  selector: 'app-select-brand',
  templateUrl: './select-brand.component.html',
  styleUrls: ['./select-brand.component.css'],
  providers: [ProductService]
})
export class SelectBrandComponent implements OnInit {
  /**
   * @description ...
   * @memberof SelectBrandComponent
   */
  __output = new Brand();

  /**
   * @description ...
   * @memberof SelectBrandComponent
   */
  __viewmode = {
    info: false,
    search: true
  };

  /**
   * @description 검색결과
   * @memberof SelectBrandComponent
   */
  searchedList = [];

  /**
   * @description 변경사항을 전달할 EventEmitter
   * @type {EventEmitter<any>}
   * @memberof SelectBrandComponent
   */
  @Output('onBrandSelected') onBrandSelected: EventEmitter<any> = new EventEmitter();

  /**
   * @deprecated
   * @description DOM Destroyed 시 이벤트 전달용 EventEmitter
   * @type {EventEmitter<any>}
   * @memberof SelectBrandComponent
   */
  @Output('onClose') onClose: EventEmitter<any> = new EventEmitter();

  /**
   * @type {ElementRef}
   * @memberof SelectBrandComponent
   */
  @ViewChild('searchedBrandInput') searchedBrandInput: ElementRef;

  /**
   * @type {ElementRef}
   * @memberof SelectBrandComponent
   */
  @ViewChild('selectedBrand') selectedBrand: ElementRef;

  /**
   * @memberof SelectBrandComponent
   */
  @Input('brand') set brand(v) {
    if (v !== undefined) {
      this.__output = Object.assign(new Brand(), v);
      if (v.isValid()) {
        this.__viewmode.info = true;
        this.__viewmode.search = false;
      }
    }
  }

  /**
   *Creates an instance of SelectBrandComponent.
   * @param {ProductService} productService
   * @memberof SelectBrandComponent
   */
  constructor(
    private productService: ProductService
  ) { }

  ngOnInit() {
  }

  /**
   * @description 키워드로 브랜드 검색
   * @param {*} $event
   * @memberof SelectBrandComponent
   */
  getBrandInfoOfKeyword($event) {
    if ($event.target.value) {
      this.productService.getBrandInfoOfKeyword($event.target.value)
        .debounceTime(3000)
        .subscribe(result => {
          if (result['message'] === 'SUCCESS') {
            this.searchedList = result['response']['document'];
          }
        });
    }
  }

  /**
   *@description 검색결과 중 선택한 브랜드 정보 업데이트
   * @memberof SelectBrandComponent
   */
  emitSearchedBrandInfo() {
    if (this.selectedBrand.nativeElement.value !== 'none') {
      this.__output = Object.assign(new Brand(), this.searchedList[this.selectedBrand.nativeElement.value]);
      this.onBrandSelected.emit(this.__output);
      this.onClose.emit();
      this.__viewmode.info = true;
      this.__viewmode.search = false;
    } else {
      alert('올바른 값을 선택해주세요!');
    }
  }
}
