import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-checkbox-group',
  templateUrl: './checkbox-group.component.html',
  styleUrls: ['./checkbox-group.component.css']
})
export class CheckboxGroupComponent implements OnInit, OnDestroy {
  /**
   * @description group property 중, value로 사용할 property name 지정
   * @type {string}
   * @memberof CheckboxGroupComponent
   */
  @Input('valueProp') valueProp: string;

  /**
   * @description group property 중, label로 사용할 property name 지정
   * @type {string}
   * @memberof CheckboxGroupComponent
   */
  @Input('labelProp') labelProp: string;

  /**
   * @type {any[]}
   * @memberof CheckboxGroupComponent
   */
  __group: any[];

  /**
   * @description group property에서 추출한 values
   * @type {any[]}
   * @memberof CheckboxGroupComponent
   */
  __values: any[];

  /**
   * @description group property에서 추출한 labels
   * @type {string[]}
   * @memberof CheckboxGroupComponent
   */
  __labels: string[];

  /**
   * @description 선택된 값 저장을 위한 property
   * @type {any[]}
   * @memberof CheckboxGroupComponent
   */
  __checked: any[];

  /**
   * @description 체크박스 값을 담은 객체배열, 각 valueProp, labelProp를 통해 값과 라벨로 사용할 key를 지정
   * @memberof CheckboxGroupComponent
   */
  @Input('group') set group(arr) {
    this.__values = [];
    this.__labels = [];
    this.__checked = [];
    if (arr.length > 0) {
      arr.forEach(obj => {
        this.__values.push(obj[this.valueProp]);
        this.__labels.push(obj[this.labelProp]);
      });
    }
    this.bindToFormControl.emit([]);
  }

  /**
   * @description 선택된 값을 output하기 위한 EventEmitter
   * @type {EventEmitter<any>}
   * @memberof CheckboxGroupComponent
   */
  @Output('bindToFormControl') bindToFormControl: EventEmitter<any> = new EventEmitter();
  constructor() { }
  ngOnInit() {
  }

  /**
   * @description DOM destroy시, 값 초기화
   * @memberof CheckboxGroupComponent
   */
  ngOnDestroy() {
    this.bindToFormControl.emit([]);
  }

  /**
   * @description 모든 값 선택/해제
   * @param {*} $event
   * @memberof CheckboxGroupComponent
   */
  selectAll($event) {
    if (this.__checked.length > 0) {
      this.__checked = [];
    } else {
      this.__checked = this.__values;
    }
    this.bindToFormControl.emit(this.__checked);
  }

  /**
   * @description bindToFormControl EventEmitter를 통해 값 전파
   * @param {*} $event
   * @memberof CheckboxGroupComponent
   */
  setValue($event) {
    if (this.__checked.indexOf($event.target.value) > -1) {
      this.__checked.splice(this.__checked.indexOf($event.target.value), 1);
    } else {
      this.__checked.push($event.target.value);
    }
    this.bindToFormControl.emit(this.__checked);
  }
}