import { Component, OnInit, AfterViewInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { ProductService } from '../../service/product.service';
import { Product } from '../../../class/product';
import { Serial } from '../../../class/serial';
import { Possession } from '../../../class/possession';
import { clone } from '../../../../environments/environment.prod';

@Component({
  selector: 'app-select-product',
  templateUrl: './select-product.component.html',
  styleUrls: ['./select-product.component.css'],
  providers: [ProductService]
})
export class SelectProductComponent implements OnInit {
  /**
   * @description 선택상태 property
   * @memberof SelectProductComponent
   */
  isSelected = true;

  /**
   * @description 전파받은 시리얼 및 제품정보 원형
   * @memberof SelectProductComponent
   */
  __o = {
    serial: new Serial(),
    product: new Product()
  };

  /**
   * @description 시리얼 및 제품정보 변경사항
   * @memberof SelectProductComponent
   */
  __u = {
    serial: new Serial(),
    product: new Product()
  };

  /**
   * @description 검색결과
   * @memberof SelectProductComponent
   */
  searched_list = [];

  /**
   * @description 변경사항(__u 안의 객체)을 전달할 EventEmitter
   * @type {EventEmitter<any>}
   * @memberof SelectProductComponent
   */
  @Output('onProductSelected') onProductSelected: EventEmitter<any> = new EventEmitter();

  /**
   * @deprecated
   * @description DOM Destroyed 시 이벤트 전달용 EventEmitter
   * @type {EventEmitter<any>}
   * @memberof SelectProductComponent
   */
  @Output('onClose') onClose: EventEmitter<any> = new EventEmitter();

  /**
   * @description 검색결과 element
   * @type {ElementRef}
   * @memberof SelectProductComponent
   */
  @ViewChild('search') search: ElementRef;

  /**
   * @description 전파받은 시리얼 정보를 각각 __o, __u 객체에 나누어 저장
   * @memberof SelectProductComponent
   */
  @Input('serial') set serial(v: Serial) {
    if (v) {
      this.__o.serial = Object.assign(new Serial(), v);
      this.__u.serial = clone(this.__o.serial);
    }
  };

  /**
   * @description 전파받은 제품정보를 각각 __o, __u 객체에 나누어 저장
   * @memberof SelectProductComponent
   */
  @Input('product') set product(v: Product) {
    if (v) {
      this.__o.product = Object.assign(new Product(), v);
      this.__u.product = clone(this.__o.product);
    }
  };

  /**
   * @description 제품 또는 시리얼 정보 수정 여부
   * @type {boolean}
   * @memberof SelectProductComponent
   */
  @Input('isEditing') isEditing: boolean;

  /**
   *Creates an instance of SelectProductComponent.
   * @param {ProductService} productService
   * @memberof SelectProductComponent
   */
  constructor(
    private productService: ProductService
  ) { }

  ngOnInit() { }

  /**
   * @description 입력받은($event) 시리얼 문자열을 바탕으로 검색
   * @param {*} $event
   * @memberof SelectProductComponent
   */
  getProductInfoOfSerial($event) {
    this.productService.getProductInfoOfSerial($event.target.value)
      .subscribe(result => {
        if (result['message'] === 'SUCCESS') {
          this.__u.serial = Object.assign(new Serial(), result['response']['document']);
          this.__u.product = Object.assign(new Product(), result['response']['document']['product']);
          this.onProductSelected.emit(this.__u);
          this.isSelected = true;
          this.search.nativeElement.style.display = 'none';
        } else if (result['message'] === 'SERIAL_IS_NOT_EXIST') {
          alert('존재하지 않는 제품번호입니다.');
        }
      });
  }

  /**
   * @description 입력받은($event) 문자열을 바탕으로 제품검색
   * @param {*} $event
   * @memberof SelectProductComponent
   */
  getProductInfoOfKeyword($event) {
    this.productService.getProductInfoOfKeyword($event.target.value)
      .subscribe(result => {
        if (result['message'] === 'SUCCESS') {
          this.searched_list = result['response']['document'];
          this.search.nativeElement.style.display = 'block';
        }
      });
  }

  /**
   * @description 제품 검색결과 중 선택 시 제품정보 업데이트 및 변경사항 전파 이벤트 실행
   * @param {number} index 검색결과 중 선택된 인덱스
   * @memberof SelectProductComponent
   */
  setProduct(index: number) {
    this.__u.serial = null;
    this.__u.product = Object.assign(new Product(), this.searched_list[index]);
    this.isSelected = true;
    this.search.nativeElement.style.display = 'none';
    this.selectProduct();
  }

  /**
   * @description 제품정보 수정 완료 시 변경사항 전파 이벤트 실행
   * @memberof SelectProductComponent
   */
  selectProduct() {
    this.__o = this.__u;
    this.onProductSelected.emit(this.__o);
    this.onClose.emit();
  }
}
