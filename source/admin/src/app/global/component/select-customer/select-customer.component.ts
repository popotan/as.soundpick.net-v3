import { Component, OnInit, Input, Output, EventEmitter, ViewChild, TemplateRef, ElementRef, AfterViewInit } from '@angular/core';
import { CustomerService } from '../../service/customer.service';
import { Customer } from '../../../class/customer';
import { clone } from '../../../../environments/environment.prod';

@Component({
  selector: 'app-select-customer',
  templateUrl: './select-customer.component.html',
  styleUrls: ['./select-customer.component.css'],
  providers: [CustomerService]
})
export class SelectCustomerComponent implements OnInit {
  /**
   * @description 선택상태 property
   * @memberof SelectCustomerComponent
   */
  isSelected = true;

  /**
   * @description 검색결과
   * @memberof SelectCustomerComponent
   */
  searched_list = [];

  /**
   * @description 변경사항을 전달할 EventEmitter
   * @type {EventEmitter<any>}
   * @memberof SelectCustomerComponent
   */
  @Output('onCustomerSelected') onCustomerSelected: EventEmitter<any> = new EventEmitter();

  /**
   * @description DOM Destroyed 시 이벤트 전달용 EventEmitter
   * @type {EventEmitter<any>}
   * @memberof SelectCustomerComponent
   */
  @Output('onClose') onClose: EventEmitter<any> = new EventEmitter();

  /**
   * @description 검색결과 element
   * @type {ElementRef}
   * @memberof SelectCustomerComponent
   */
  @ViewChild('search') search: ElementRef;

  /**
   * @description 전파받은 고객정보 원형
   * @type {Customer}
   * @memberof SelectCustomerComponent
   */
  __oCustomer: Customer;

  /**
   * @description 고객정보 변경사항
   * @type {Customer}
   * @memberof SelectCustomerComponent
   */
  __uCustomer: Customer;

  /**
   * @description 전파받은 고객정보를 저장하고 복사
   * @memberof SelectCustomerComponent
   */
  @Input('customer') set customer(v: Customer) {
    this.__oCustomer = Object.assign(new Customer(), v);
    this.__uCustomer = clone(this.__oCustomer);
  };

  /**
   * @description 구매내역정보 수정여부
   * @memberof SelectCustomerComponent
   */
  @Input('isEditing') isEditing;

  /**
   *Creates an instance of SelectCustomerComponent.
   * @param {CustomerService} customerService
   * @memberof SelectCustomerComponent
   */
  constructor(
    private customerService: CustomerService
  ) { }

  ngOnInit() {
  }

  /**
   * @description 키워드로 고객정보 검색
   * @param {*} $event
   * @memberof SelectCustomerComponent
   */
  getCustomerInfoOfKeyword($event) {
    this.customerService.getCustomerInfoOfKeyword($event.target.value)
      .subscribe(result => {
        if (result['message'] === 'SUCCESS') {
          this.searched_list = result['response']['document'];
          this.search.nativeElement.style.display = 'block';
        } else {
          if (confirm('기등록된 사용자정보가 없습니다. 새로운 사용자로 등록하시겠습니까?')) {
            this.onCustomerSelected.emit(this.__uCustomer);
            this.onClose.emit();
            this.isSelected = true;
          }
        }
      });
  }

  /**
   * @description 고객정보 검색결과 중 선택 시 고객정보 업데이트 및 변경사항 전파 이벤트 실행
   * @param {*} index
   * @memberof SelectCustomerComponent
   */
  emitSearchedCustomerInfo(index) {
    this.__uCustomer = Object.assign(new Customer(), this.searched_list[index]);
    this.onCustomerSelected.emit(this.__uCustomer);
    this.onClose.emit();
    this.isSelected = true;
    this.search.nativeElement.style.display = 'none';
  }

  /**
   * @description 신규 고객정보 입력 시 고객정보 업데이트 및 변경사항 전파 이벤트 실행
   * @memberof SelectCustomerComponent
   */
  emitAddedCustomerInfo() {
    if (this.__uCustomer.phone1) {
      this.customerService.getExistedPhone(this.__uCustomer.phone1)
        .subscribe(result => {
          if (result['message'] === 'SUCCESS') {
            if (result['response']['document']['is_exist']) {
              alert('이미 존재하는 연락처입니다. 연락처검색을 통해 고객을 등록해주세요!');
            } else {
              this.onCustomerSelected.emit(this.__uCustomer);
              this.onClose.emit();
              this.isSelected = true;
            }
          }
        });
    }
  }
}
