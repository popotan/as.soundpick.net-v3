import { Component, OnInit } from '@angular/core';
import { SessionService } from '../../../core/session.service';
import { apiURL } from '../../../../environments/environment.prod';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  apiURL = apiURL;
  constructor(
    private sessionService: SessionService
  ) { }

  ngOnInit() {
  }

}
