import { Component, OnInit, AfterViewInit, ViewChild, ElementRef, Input, Output, EventEmitter, DoCheck, OnChanges } from '@angular/core';
import { PossessionService } from '../../service/possession.service';
import { FormAssistantService } from '../../service/form-assistant.service';
import { FileUploadService } from '../../../core/file-upload.service';
import { Customer } from '../../../class/customer';
import { Possession } from '../../../class/possession';
import { ExchangeLog } from '../../../class/exchange-log';
import { BehaviorSubject } from 'rxjs';
import { clone, apiURL, staticURL } from '../../../../environments/environment.prod';

@Component({
  selector: 'app-select-possession',
  templateUrl: './select-possession.component.html',
  styleUrls: ['./select-possession.component.css'],
  providers: [PossessionService, FileUploadService]
})
export class SelectPossessionComponent implements OnInit, AfterViewInit {
  /**
   * @description 구매내역정보 수정여부
   * @memberof SelectPossessionComponent
   */
  isEditing = false;

  /**
   * @description 검색결과
   * @memberof SelectPossessionComponent
   */
  searched_list = [];

  /**
   * @description 구매증빙사진파일 업로드용 element
   * @type {ElementRef}
   * @memberof SelectPossessionComponent
   */
  @ViewChild('receiptFileInput') receiptFileInput: ElementRef;

  /**
   * @description 전파받은 구매내역정보 원형
   * @type {Possession}
   * @memberof SelectPossessionComponent
   */
  __oPossession: Possession;

  /**
   * @description 구매내역정보 변경사항
   * @type {Possession}
   * @memberof SelectPossessionComponent
   */
  __uPossession: Possession;

  /**
   * @description 전파받은 구매내역정보를 저장하고 복사
   * @memberof SelectPossessionComponent
   */
  @Input('possession') set possession(v: Possession) {
    this.__oPossession = Object.assign(new Possession(), v);
    this.__uPossession = clone(this.__oPossession);
  };

  /**
   * @deprecated
   * @type {ElementRef}
   * @memberof SelectPossessionComponent
   */
  @ViewChild('possessionSelectView') possessionSelectView: ElementRef;

  /**
   * @description 구매내역정보 변경사항을 전달할 EventEmitter
   * @type {EventEmitter<any>}
   * @memberof SelectPossessionComponent
   */
  @Output('onPossessionSelected') onPossessionSelected: EventEmitter<any> = new EventEmitter();

  /**
   *Creates an instance of SelectPossessionComponent.
   * @param {PossessionService} possessionService
   * @param {FormAssistantService} formAssistantService
   * @param {FileUploadService} fileUploadService
   * @memberof SelectPossessionComponent
   */
  constructor(
    private possessionService: PossessionService,
    private formAssistantService: FormAssistantService,
    private fileUploadService: FileUploadService
  ) { }
  ngOnInit() {
  }
  ngAfterViewInit() {
  }

  /**
   * @description ?
   * @memberof SelectPossessionComponent
   */
  getPossessionInfoByCustomer() {
    if (
      this.__oPossession.exchange_log &&
      this.__oPossession.exchange_log[0].customer &&
      this.__oPossession.exchange_log[0].customer.id
    ) {
      this.possessionService.getPossessionInfoByCustomer(this.__oPossession.exchange_log[0].customer.id)
        .debounceTime(500)
        .subscribe(result => {
          if (result['message'] === 'SUCCESS') {
            this.searched_list = result['response']['document'];
          }
        });
    }
  }

  /**
   * @description 보증종료일 자동 계산
   * @memberof SelectPossessionComponent
   */
  calculateWarrantyEndDate() {
    if (
      this.__uPossession.exchange_log &&
      this.__uPossession.exchange_log[0].product &&
      this.__uPossession.exchange_log[0].product.warranty_day_length &&
      this.__uPossession.purchase_date
    ) {
      const expiredDate = this.formAssistantService.calculateDateTerm(
        this.__uPossession.exchange_log[0].product.warranty_day_length,
        this.__uPossession.purchase_date
      );
      this.__uPossession.warranty_expired_date = expiredDate;
    } else {
      alert('제품명과 구매일을 올바르게 입력해주세요!');
    }
  }

  /**
   * @description 구매증빙사진파일 업로드
   * @param {*} $event
   * @memberof SelectPossessionComponent
   */
  proofReceiptImageFileUpload($event) {
    this.fileUploadService.uploadFile(apiURL + '/possession/receipt/upload', $event.target.files)
      .then(result => {
        this.__uPossession.proof_image_filename = result['filename'];
        alert('파일이 업로드되었습니다.');
      }, error => {
        alert('오류가 발생하였습니다. 새로고침 후 다시 시도해 주세요!');
      });
  }

  /**
   * @description 구매증빙사진 보기
   * @memberof SelectPossessionComponent
   */
  showProofImage() {
    window.open(staticURL + '/receipt/' + this.__uPossession.proof_image_filename, '_blank', 'width=400, height=300, scrollbars=1');
  }

  /**
   * @description 제품정보 수정 시, 수정된 제품정보 입력
   * @param {*} $event
   * @memberof SelectPossessionComponent
   */
  onProductSelected($event) {
    this.__uPossession.exchange_log[0].product = $event.product;
    this.__uPossession.exchange_log[0].serial = $event.serial;
  }

  /**
   * @deprecated
   * @memberof SelectPossessionComponent
   */
  emitSearchedPossessionInfo() {
    if (this.possessionSelectView.nativeElement.value !== 'none') {
      if (confirm('해당 구매정보로 기존 정보를 대체하시겠습니까?')) {
        console.log(this.searched_list[this.possessionSelectView.nativeElement.value]);
        this.onPossessionSelected.emit(
          Object.assign(new Possession(), this.searched_list[this.possessionSelectView.nativeElement.value]));
          this.isEditing = false;
      }
    } else {
      alert('올바른 값을 선택해주세요');
    }
  }

  /**
   * @description 구매내역정보 수정사항 전파
   * @memberof SelectPossessionComponent
   */
  emitAddedPossessionInfo() {
    this.onPossessionSelected.emit(this.__uPossession);
    this.isEditing = false;
  }
}
