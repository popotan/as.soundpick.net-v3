import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectPossessionComponent } from './select-possession.component';

describe('SelectPossessionComponent', () => {
  let component: SelectPossessionComponent;
  let fixture: ComponentFixture<SelectPossessionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectPossessionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectPossessionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
