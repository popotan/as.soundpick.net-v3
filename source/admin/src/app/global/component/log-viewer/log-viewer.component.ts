import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import { LogService } from '../../service/log.service';
import { FormAssistantService } from '../../service/form-assistant.service';
import { apiURL, staticURL } from '../../../../environments/environment.prod';
/**
 * @deprecated
 * @export
 * @class LogViewerComponent
 * @implements {OnInit}
 * @implements {AfterViewInit}
 */
@Component({
  selector: 'app-log-viewer',
  templateUrl: './log-viewer.component.html',
  styleUrls: ['./log-viewer.component.css'],
  providers: [LogService]
})

export class LogViewerComponent implements OnInit, AfterViewInit {
  staticURL = staticURL;

  /**
   *
   *
   * @memberof LogViewerComponent
   */
  __parsed = [];

  /**
   *
   *
   * @memberof LogViewerComponent
   */
  __aligned = [];

  /**
   *
   *
   * @memberof LogViewerComponent
   */
  __columnList = {
    'possession': '구매정보',
    'warranty_expired_date' : '보증만료일자',
    'purchase_date' : '구매일자',
    'customer': '구매자정보',
    'possession_exchange_log': '교환정보',
    'possession_exchange_log_option': '교환정보',
    'as_case': 'AS정보',
    'as_process': 'AS정보',
    'process.test_device' : '연결장비',
    'process.postcode' : '우편번호',
    'process.address' : '주소',
    'process.invoice_price' : '운임가',
    'process.invoice_fee_type' : '처리타입',
    'process.invoice' : '송장번호',
    'serial': '제품번호정보',
    'brand': '브랜드정보',
    'product': '제품정보'
  };


  /**
   *
   *
   * @memberof LogViewerComponent
   */
  __target = {};


  /**
   *
   *
   * @memberof LogViewerComponent
   */
  @Input('target') set target(v) {
    let temp = {};
    let parentObjName = v.constructor.name.toLowerCase().replace('__', '');
    function getId(obj) {
      for (const key in obj) {
        if (key === 'id') {
          if (!temp.hasOwnProperty(parentObjName)) {
            temp[parentObjName] = [];
          }
          if (temp[parentObjName].indexOf(obj[key]) === -1) {
            temp[parentObjName].push(obj[key]);
          }
        } else if (
          obj[key] &&
          obj[key] instanceof Array
        ) {
          parentObjName = key.toLowerCase().replace('__', '');
          obj[key].forEach(innerObj => {
            getId(innerObj);
          });
        } else if (
          obj[key] &&
          typeof obj[key] !== 'string' &&
          Object.keys(obj[key]).length > 0
        ) {
          parentObjName = key.toLowerCase().replace('__', '');
          getId(obj[key]);
        }
      }
    }
    getId(v);
    this.__target = temp;
  }


  /**
   *Creates an instance of LogViewerComponent.
   * @param {LogService} logService
   * @param {FormAssistantService} formAssistantService
   * @memberof LogViewerComponent
   */
  constructor(
    private logService: LogService,
    private formAssistantService: FormAssistantService
  ) { }

  ngOnInit() {
  }


  /**
   * @description DOM 로드 종료 후, 로그 불러오기
   * @memberof LogViewerComponent
   */
  ngAfterViewInit() {
    let param = [];
    for (const col in this.__target) {
      if (this.__target.hasOwnProperty(col)) {
        param.push(col + ':' + this.__target[col].join(','));
      }
    }
    this.logService.getLog(
      param.join('|')
    ).subscribe(result => {
      if (result['message'] === 'SUCCESS') {
        this.__parsed = result['response']['document'];
        this.distinctByRegDate();
      } else {
        alert('로그정보를 불러오는데 실패하였습니다.');
      }
    });
  }


  /**
   * @description 날짜별로 로그 정렬
   * @memberof LogViewerComponent
   */
  distinctByRegDate() {
    const _temp = [];
    const _regDateTemp = [];
    for (let i = 0; i < this.__parsed.length; i++) {
      let k = _regDateTemp.indexOf(this.__parsed[i]['reg_date']);
      if (k === -1) {
        _regDateTemp.push(this.__parsed[i]['reg_date']);
        _temp.push([]);
        k = _regDateTemp.indexOf(this.__parsed[i]['reg_date']);
      }
      _temp[k].push(this.__parsed[i]);
    }
    this.__aligned = _temp;
    console.log(this.__aligned);
  }


  /**
   *
   *
   * @param {string} columnName
   * @returns
   * @memberof LogViewerComponent
   */
  translate(columnName: string) {
    if (this.__columnList.hasOwnProperty(columnName)) {
      return this.__columnList[columnName];
    } else {
      return columnName;
    }
  }

}
