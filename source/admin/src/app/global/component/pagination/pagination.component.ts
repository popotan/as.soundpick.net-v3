import { Component, OnInit, Input, Output, EventEmitter, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnInit, AfterViewInit {
  __contentLength: number;
  @Input('contentLength') set contentLength(value: number) {
    this.__contentLength = value;
    this.set_pagination_arr();
  }

  __atOnce: number;
  @Input('atOnce') set atOnce(value: number) {
    this.__atOnce = value;
  }

  @Output('whenPageClicked') targetPage: EventEmitter<any> = new EventEmitter();

  pagination_arr = [];
  active_arr = [];
  __active_page: number;
  set active_page(value: number) {
    this.__active_page = value;
    this.set_pagination_range_to_show();
  }

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit() {
  }

  set_pagination_arr() {
    this.pagination_arr = [];
    let pageLength = Math.ceil(this.__contentLength / this.__atOnce);
    for (let i = 0; i < pageLength; i++) {
      this.pagination_arr.push(i + 1);
    }
    this.active_page = 1;
  }

  set_pagination_range_to_show() {
    this.active_arr = [];

    let start_with = this.__active_page - 2;
    let end_with = this.__active_page + 5;
    for (let i = this.__active_page; i > 0 && i >= start_with; i--) {
      this.active_arr.unshift(i);
      end_with--;
    }
    for (let i = this.__active_page + 1; i <= end_with && i <= this.pagination_arr.length; i++) {
      this.active_arr.push(i);
    }
  }

  goto(page) {
    this.targetPage.emit(page);
    this.active_page = page;
  }

  prev() {
    if (this.__active_page > 1) {
      this.active_page = this.__active_page - 1;
      this.goto(this.__active_page);
    }
  }
  next() {
    if (this.__active_page < this.pagination_arr.length) {
      this.active_page = this.__active_page + 1;
      this.goto(this.__active_page);
    }
  }
}