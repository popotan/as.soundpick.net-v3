import { Component, OnInit, forwardRef, ElementRef, AfterViewInit } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { FormAssistantService } from '../../service/form-assistant.service';

const noop = () => { };

export const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => InputDateComponent),
  multi: true
};

@Component({
  selector: 'input-date',
  templateUrl: './input-date.component.html',
  styleUrls: ['./input-date.component.css'],
  providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR, FormAssistantService]
})
export class InputDateComponent implements ControlValueAccessor {
  /**
   * @private
   * @type {Date}
   * @memberof InputDateComponent
   */
  private dateTypeValue: Date = null;

  /**
   * @description FormAssistantService의 convertToDateTypeOfString() 메소드를 통해 변환하여 사용
   * @private
   * @type {string}
   * @memberof InputDateComponent
   */
  private strTypeValue: string;

  /**
   * @memberof InputDateComponent
   */
  placeholder = this.el.nativeElement.attributes.placeholder.value;

  /**
   * @memberof InputDateComponent
   */
  disabled = this.el.nativeElement.attributes.disabled;

  /**
   * @memberof InputDateComponent
   */
  name = this.el.nativeElement.attributes.name;

  private onToutchedCallback: () => void = noop;
  private onChangeCallback: (_: any) => void = noop;

  get value(): any {
    return this.strTypeValue;
  }

  set value(v: any) {
    if (v) {
      if (v !== this.strTypeValue) {
        this.strTypeValue = v;
        this.dateTypeValue = this.formAssistantService.convertToDateTypeOfString(this.strTypeValue);
        if (this.dateTypeValue) {
          this.onChangeCallback(this.dateTypeValue);
        }
      }
    } else {
      this.onChangeCallback(null);
    }
  }

  onBlur() {
    this.onToutchedCallback();
  }

  writeValue(value: any) {
    if (value) {
      if (value !== this.dateTypeValue) {
        this.dateTypeValue = value;
        this.strTypeValue = this.formAssistantService.convertToStringTypeOfDate(this.dateTypeValue);
      }
    }
  }

  registerOnChange(fn: any) {
    this.onChangeCallback = fn;
  }

  registerOnTouched(fn: any) {
    this.onToutchedCallback = fn;
  }

  /**
   *Creates an instance of InputDateComponent.
   * @param {FormAssistantService} formAssistantService
   * @param {ElementRef} el
   * @memberof InputDateComponent
   */
  constructor(
    private formAssistantService: FormAssistantService,
    private el: ElementRef
  ) { }
}
