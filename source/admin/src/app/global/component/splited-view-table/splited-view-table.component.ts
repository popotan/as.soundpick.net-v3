import { Component, OnInit, AfterViewInit, AfterContentInit, Renderer2, Input, Output, EventEmitter, ViewChild, HostListener, ElementRef } from '@angular/core';
import { DomSanitizer, SafeValue } from '@angular/platform-browser';

@Component({
  selector: 'app-splited-view-table',
  templateUrl: './splited-view-table.component.html',
  styleUrls: ['./splited-view-table.component.css']
})
export class SplitedViewTableComponent implements OnInit, AfterContentInit {

  /**
   * @description 타이틀 표기
   * @type {any[]}
   * @memberof SplitedViewTableComponent
   */
  @Input('title') title: any[];

  /**
   * @description 데이터 배열. {'text', 'actionType', 'methodName'} 으로 구성된 배열.
   * static area와 scrollable area로 나누어 값 전달. ex) [[static],[scrollable]]
   * data property에 할당. 할당 시, 선택된 체크박스 초기화하고 스크롤 0으로 초기화
   * @memberof SplitedViewTableComponent
   */
  @Input('data') set _data(v) {
    this.selectedIndex = [];
    this.onCheckboxSwitchedActionOutput.emit(this.selectedIndex);
    this.data = v;
    this.el.nativeElement.scrollTop = 0;
  };

  /**
   * @description 데이터배열.
   * @type {Array<Array<any>>}
   * @memberof SplitedViewTableComponent
   */
  data: Array<Array<any>>  = [[], []];

  /**
   * @description 컬럼 넓이. data 배열과 다르게, 1차원 배열이며, SplitedViewTableComponent 초기화 시, data 배열과 길이 비교를 한다.
   * @type {number[]}
   * @memberof SplitedViewTableComponent
   */
  @Input('columnWidth') columnWidth: number[];

  /**
   * @description 데이터 배열 내, actionType 및 methodName이 정의된 오브젝트가 해당 EventEmitter를 통해 이벤트를 전파한다.
   * @type {EventEmitter<any>}
   * @memberof SplitedViewTableComponent
   */
  @Output('actionOutput') actionOutput: EventEmitter<any> = new EventEmitter<any>();

  /**
   * @description 체크박스 체크 시, 전체 체크된 인덱스를 전파.
   * @type {EventEmitter<any>}
   * @memberof SplitedViewTableComponent
   */
  @Output('onCheckboxSwitchedActionOutput') onCheckboxSwitchedActionOutput: EventEmitter<any> = new EventEmitter<any>();

  /**
   * @type {ElementRef}
   * @memberof SplitedViewTableComponent
   */
  @ViewChild('staticHeader') staticHeader: ElementRef;

  /**
   * @type {ElementRef}
   * @memberof SplitedViewTableComponent
   */
  @ViewChild('scrollableHeader') scrollableHeader: ElementRef;

  /**
   * @type {ElementRef}
   * @memberof SplitedViewTableComponent
   */
  @ViewChild('staticBody') staticBody: ElementRef;

  /**
   * @type {ElementRef}
   * @memberof SplitedViewTableComponent
   */
  @ViewChild('scrollableBody') scrollableBody: ElementRef;

  /**
   * @description static 영역의 넓이. px단위
   * @memberof SplitedViewTableComponent
   */
  staticAreaWidth = 45;

  /**
   * scrollable 영역의 넓이. px단위
   * @memberof SplitedViewTableComponent
   */
  scrollAreaWidth = 0;

  /**
   * @description 스크롤 상태 추적을 위한 property
   * @memberof SplitedViewTableComponent
   */
  isScrolling = false;

  /**
   * @description 체크된 인덱스를 저장하기 위한 property. onCheckboxSwitchedActionOutput가 이벤트 전파 시, 이 property를 사용한다.
   * @memberof SplitedViewTableComponent
   */
  selectedIndex = [];

  /**
   *Creates an instance of SplitedViewTableComponent.
   * @param {ElementRef} el
   * @param {Renderer2} renderer2
   * @param {DomSanitizer} sanitizer
   * @memberof SplitedViewTableComponent
   */
  constructor(
    public el: ElementRef,
    public renderer2: Renderer2,
    public sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
  }

  /**
   * @description splited-view-table 넓이 설정 및 scroll 이벤트 리스너 설정.
   * @memberof SplitedViewTableComponent
   */
  ngAfterContentInit() {
    for (let i = 0; i < this.title[0].length; i++) {
      this.staticAreaWidth += this.columnWidth[i];
    }
    this.staticBody.nativeElement.addEventListener('scroll', this.onScroll.bind(this), false);
    this.scrollableBody.nativeElement.addEventListener('scroll', this.onScroll.bind(this), false);

    for (let i = 0; i < this.title[1].length; i++) {
      this.scrollAreaWidth += this.columnWidth[this.title[0].length + i];
    }
  }

  /**
   * @param {*} element 이벤트 발생여부를 검사할 element
   * @returns {boolean} 검사결과
   * @memberof SplitedViewTableComponent
   */
  isMyElement(element) {
    let isFound = false;
    do {
      if (element === this.el.nativeElement) {
        isFound = true;
      } else {
        element = element['parentNode'];
      }
    } while (element && !isFound);
    return isFound;
  }

  /**
   * @param {*} value
   * @returns
   * @memberof SplitedViewTableComponent
   */
  styleSnt(value) {
    const v = this.sanitizer.bypassSecurityTrustStyle(value);
    return v;
  }

  /**
   * @description mouseenter 이벤트리스너
   * @param {*} index mouseenter 이벤트가 발생한 엘리먼트의 인덱스
   * @memberof SplitedViewTableComponent
   */
  mouseenterStyle(index) {
    const elements = this.el.nativeElement.querySelectorAll('.body .row:nth-child(' + (index + 1) + ')');
    for (let i = 0; i < elements.length; i++) {
      this.renderer2.setStyle(elements.item(i), 'background-color', 'yellow');
    }
  }

  /**
   * @description mouseleave 이벤트리스너
   * @param {*} index mouseleave 이벤트가 발생한 엘리먼트의 인덱스
   * @memberof SplitedViewTableComponent
   */
  mouseleaveStyle(index) {
    const elements = this.el.nativeElement.querySelectorAll('.body .row:nth-child(' + (index + 1) + ')');
    for (let i = 0; i < elements.length; i++) {
      this.renderer2.setStyle(elements.item(i), 'background-color', '#fff');
    }
  }

  /**
   * @description 스크롤 이벤트리스너
   * @param {*} $event
   * @memberof SplitedViewTableComponent
   */
  onScroll($event) {
    if (!this.isScrolling) {
      this.isScrolling = true;
      setTimeout(() => {
        if (this.isMyElement($event.target)) {
          if ($event.target.className.indexOf('scrollable-x') > -1) {
            this.scrollableHeader.nativeElement.scrollLeft = $event.target.scrollLeft;
          }
          if ($event.target.className.indexOf('scrollable-y') > -1) {
            if (this.scrollableBody.nativeElement !== $event.target) {
              this.scrollableBody.nativeElement.scrollTop = $event.target.scrollTop;
            }
            if (this.staticBody.nativeElement !== $event.target) {
              this.staticBody.nativeElement.scrollTop = $event.target.scrollTop;
            }
          }
        }
        this.isScrolling = false;
      }, 100, this);
    }
  }

  /**
   * @description 전체선택/해제. selectedIndex에 결과가 정의되며, 해당 값을 onCheckboxSwitchedActionOutput을 통해 전파함. 
   * @param {*} $event
   * @memberof SplitedViewTableComponent
   */
  onTopCheckboxSwitched($event) {
    if (this.selectedIndex.length === this.data[0].length) {
      this.selectedIndex = [];
    } else {
      this.selectedIndex = Array.apply(null, {length: this.data[0].length}).map(Number.call, Number);
    }
    this.onCheckboxSwitchedActionOutput.emit(this.selectedIndex);
  }

  /**
   * @description 개별 선택/해제. selectedIndex에 결과가 정의되며, 해당값을 onCheckboxSwitchedActionOutput을 통해 전파함.
   * @param {*} $event
   * @memberof SplitedViewTableComponent
   */
  onCheckboxSwitched($event) {
    if (this.selectedIndex.indexOf(Number($event.target.value)) > -1) {
      this.selectedIndex.splice(this.selectedIndex.indexOf(Number($event.target.value)), 1);
    } else {
      this.selectedIndex.push(Number($event.target.value));
    }
    this.selectedIndex.sort((a, b) => a - b);
    this.onCheckboxSwitchedActionOutput.emit(this.selectedIndex);
  }

  /**
   * @description data 배열에 담긴 오브젝트에 정의된 methodName의 value값을 이벤트 발생 인덱스와 함께 전파하여, 상위 컴포넌트에서 해당 메소드 수행.
   * @param {*} methodName
   * @param {*} index
   * @memberof SplitedViewTableComponent
   */
  methodEmit(methodName, index) {
    this.actionOutput.emit({ methodName: methodName, index: index });
  }
}
