import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SplitedViewTableComponent } from './splited-view-table.component';

describe('SplitedViewTableComponent', () => {
  let component: SplitedViewTableComponent;
  let fixture: ComponentFixture<SplitedViewTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SplitedViewTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SplitedViewTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
