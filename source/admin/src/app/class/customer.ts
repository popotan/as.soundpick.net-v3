export class Customer {
    id = null as number;
    name = null as string;
    phone1 = null as string;
    phone2 = null as string;
    __reg_date = null as Date;
    set reg_date(v) {
        if (typeof v === 'string') {
            this.__reg_date = new Date(v);
        } else if (v instanceof Date) {
            this.__reg_date = v;
        }
    }
    get reg_date() {
        return this.__reg_date;
    }
}
