import { BrandOption } from "./brand-option";

export class Brand {
    id = null as number;
    name = null as string;
    image = null as object[];
    __reg_date = null as Date;
    __option = null as BrandOption;
    set option(v) {
        if (v instanceof BrandOption) {
            this.__option = v;
        } else {
            this.__option = Object.assign(new BrandOption(), v);
        }
    }
    get option() {
        if (!this.__option) {
            this.option = new BrandOption();
        }
        return this.__option;
    }
    set reg_date(v) {
        if (typeof v === 'string') {
            this.__reg_date = new Date(v);
        } else if (v instanceof Date) {
            this.__reg_date = v;
        }
    }
    get reg_date() {
        return this.__reg_date;
    }
}
