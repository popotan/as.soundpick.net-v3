import { Customer } from './customer';
import { Possession } from './possession';
import { ASProcess } from './asprocess';

export class ASCase {
    id = null as number;
    division = null as string;
    description = null as string;
    is_expired = null as string;
    __expired_date = null as Date;
    __reg_date = null as Date;
    __customer = null as Customer;
    __possession = null as Possession;
    __process = null as ASProcess;
    set process(v) {
        if (v instanceof ASProcess) {
            this.__process = v;
        } else {
            this.__process = Object.assign(new ASProcess(), v);
        }
    }
    get process() {
        if (!this.__process) {
            this.process = new ASProcess();
        }
        return this.__process;
    }
    set possession(v) {
        if (v instanceof Possession) {
            this.__possession = v;
        } else {
            this.__possession = Object.assign(new Possession(), v);
        }
    }
    get possession() {
        if (!this.__possession) {
            this.possession = new Possession();
        }
        return this.__possession;
    }
    set customer(v) {
        if (v instanceof Customer) {
            this.__customer = v;
        } else {
            this.__customer = Object.assign(new Customer(), v);
        }
    }
    get customer() {
        if (!this.__customer) {
            this.customer = new Customer();
        }
        return this.__customer;
    }
    set reg_date(v) {
        if (typeof v === 'string') {
            this.__reg_date = new Date(v);
        } else if (v instanceof Date) {
            this.__reg_date = v;
        }
    }
    get reg_date() {
        return this.__reg_date;
    }
    set expired_date(v) {
        if (typeof v === 'string') {
            this.__expired_date = new Date(v);
        } else if (v instanceof Date) {
            this.__expired_date = v;
        }
    }
    get expired_date() {
        return this.__expired_date;
    }
}
