import { ASCase } from './ascase';
import { ExchangeLog } from './exchange-log';
import { Customer } from './customer';

export class Possession {
    id = null as number;
    __purchase_date = null as Date;
    proof_image_filename = null as string;
    __warranty_expired_date = null as Date;
    __reg_date = null as Date;
    __exchange_log = null as Array<ExchangeLog>;
    __as_case = null as Array<ASCase>;
    set exchange_log(v) {
        if (this.__exchange_log === null) {
            this.__exchange_log = Array<ExchangeLog>();
        }
        for (const c of v) {
            if (c instanceof ExchangeLog) {
                this.__exchange_log.push(c);
            } else {
                this.__exchange_log.push(Object.assign(new ExchangeLog(), c));
            }
        }
    }
    get exchange_log() {
        return this.__exchange_log;
    }
    set as_case(v) {
        if (this.__as_case === null) {
            this.__as_case = [];
        }
        for (const c of v) {
            if (c instanceof ASCase) {
                this.__as_case.push(c);
            } else {
                this.__as_case.push(Object.assign(new ASCase(), c));
            }
        }
    }
    get as_case() {
        return this.__as_case;
    }
    set purchase_date(v) {
        if (typeof v === 'string') {
            this.__purchase_date = new Date(v);
        } else if (v instanceof Date) {
            this.__purchase_date = v;
        }
    }
    get purchase_date() {
        return this.__purchase_date;
    }
    set reg_date(v) {
        if (typeof v === 'string') {
            this.__reg_date = new Date(v);
        } else if (v instanceof Date) {
            this.__reg_date = v;
        }
    }
    get reg_date() {
        return this.__reg_date;
    }
    set warranty_expired_date(v) {
        if (typeof v === 'string') {
            this.__warranty_expired_date = new Date(v);
        } else if (v instanceof Date) {
            this.__warranty_expired_date = v;
        }
    }
    get warranty_expired_date() {
        return this.__warranty_expired_date;
    }
}
