import { Product } from './product';
import { SerialProcess } from './serial-process';

export class Serial {
    id = null as number;
    serial = null as string;
    is_expired = null as boolean;
    __expired_date = null as Date;
    __reg_date = null as Date;
    __product = null as Product;
    __process = null as SerialProcess;
    set process(v) {
        if (v instanceof SerialProcess) {
            this.__process = v;
        } else {
            this.__process = Object.assign(new SerialProcess(), v);
        }
    }
    set product(v) {
        if (v instanceof Product) {
            this.__product = v;
        } else {
            this.__product = Object.assign(new Product(), v);
        }
    }
    get product() {
        if (!this.__product) {
            this.product = new Product();
        }
        return this.__product;
    }
    set reg_date(v) {
        if (typeof v === 'string') {
            this.__reg_date = new Date(v);
        } else if (v instanceof Date) {
            this.__reg_date = v;
        }
    }
    get reg_date() {
        return this.__reg_date;
    }
    set expired_date(v) {
        if (typeof v === 'string') {
            this.__expired_date = new Date(v);
        } else if (v instanceof Date) {
            this.__expired_date = v;
        }
    }
    get expired_date() {
        return this.__expired_date;
    }
}
