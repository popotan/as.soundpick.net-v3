export class ExchangeLogOption {
    comment = null as string;
    adjustment_type = null as string;
    adjustment_fee = null as number;
    address = null as string;
    postcode = null as string;
    delivery_method = null as string;
    invoice = null as string;
    dsc = null as string;
    invoice_price = null as number;
}
