export class ASProcess {
    test_device = null as string;
    address = null as string;
    postcode = null as string;
    invoice = null as string;
    invoice_price = null as number;
    invoice_fee_type = null as string;
    delivery_method = null as string;
    delivery_date = null as Date;
}
