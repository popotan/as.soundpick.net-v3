import { Customer } from './customer';
import { Product } from './product';
import { Serial } from './serial';
import { ExchangeLogOption } from './exchange-log-option';

export class ExchangeLog {
    id = null as number;
    __reg_date = null as Date;
    __customer = null as Customer;
    __product = null as Product;
    __serial = null as Serial;
    __option = null as ExchangeLogOption;
    as_case_id = null as number;
    set option(v) {
        if (v instanceof ExchangeLogOption) {
            this.__option = v;
        } else {
            this.__option = Object.assign(new ExchangeLogOption(), v);
        }
    }
    get option() {
        if (!this.__option) {
            this.option = new ExchangeLogOption();
        }
        return this.__option;
    }
    set customer(v) {
        if (v instanceof Customer) {
            this.__customer = v;
        } else {
            this.__customer = Object.assign(new Customer(), v);
        }
    }
    get customer() {
        if (!this.__customer) {
            this.customer = new Customer();
        }
        return this.__customer;
    }
    set product(v) {
        if (v instanceof Product) {
            this.__product = v;
        } else {
            this.__product = Object.assign(new Product(), v);
        }
    }
    get product() {
        if (!this.__product) {
            this.product = new Product();
        }
        return this.__product;
    }
    set serial(v) {
        if (v instanceof Serial) {
            this.__serial = v;
        } else {
            this.__serial = Object.assign(new Serial(), v);
        }
    }
    get serial() {
        if (!this.__serial) {
            this.serial = new Serial();
        }
        return this.__serial;
    }
    set reg_date(v) {
        if (typeof v === 'string') {
            this.__reg_date = new Date(v);
        } else if (v instanceof Date) {
            this.__reg_date = v;
        }
    }
    get reg_date() {
        return this.__reg_date;
    }
}
