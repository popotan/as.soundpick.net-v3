import { Brand } from './brand';
import { ProductOption } from './product-option';

export class Product {
    id = null as number;
    name = null as string;
    validation = null as boolean;
    lineup = null as string;
    image = null as object[];
    __reg_date = null as Date;
    warranty_day_length = null as number;
    __brand = null as Brand;
    __option = null as ProductOption;
    set option(v) {
        if (v instanceof ProductOption) {
            this.__option = v;
        } else {
            this.__option = Object.assign(new ProductOption(), v);
        }
    }
    get option() {
        if (!this.__option) {
            this.option = new ProductOption();
        }
        return this.__option;
    }
    set brand(v) {
        if (v instanceof Brand) {
            this.__brand = v;
        } else {
            this.__brand = Object.assign(new Brand(), v);
        }
    }
    get brand() {
        if (!this.__brand) {
            this.brand = new Brand();
        }
        return this.__brand;
    }
    set reg_date(v) {
        if (typeof v === 'string') {
            this.__reg_date = new Date(v);
        } else if (v instanceof Date) {
            this.__reg_date = v;
        }
    }
    get reg_date() {
        return this.__reg_date;
    }
}
