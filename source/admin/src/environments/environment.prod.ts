export const environment = {
  production: true
};

export const apiURL = 'http://admin-as.soundpick.net/api/admin';
export const staticURL = 'http://admin-as.soundpick.net/static';
export const httpOptions = { withCredentials: true };

export function clone<T>(obj: T): T {
  let copy;

  // Handle the 3 simple types, and null or undefined
  if (null == obj || "object" != typeof obj) return obj;

  // Handle Date
  if (obj instanceof Date) {
    copy = new Date();
    copy.setTime(obj.getTime());
    return copy;
  }

  // Handle Array
  if (obj instanceof Array) {
    copy = [];
    for (let i = 0, len = obj.length; i < len; i++) {
      copy[i] = clone(obj[i]);
    }
    return copy;
  }

  // Handle Object
  if (obj instanceof Object) {
    copy = new (obj.constructor as { new(): T })();
    for (let attr in obj) {
      if (obj.hasOwnProperty(attr) && typeof obj[attr] !== 'function') {
        copy[attr] = clone(obj[attr]);
      }
    }

    return copy;
    // return Object.assign(copy, obj);
  }

  throw new Error("Unable to copy obj! Its type isn't supported.");
}